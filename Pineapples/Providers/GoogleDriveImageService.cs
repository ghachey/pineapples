﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using ImageProcessor.Web.Services;
using ImageProcessor.Web.Helpers;
using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace Pineapples.Providers
{
    public class GoogleDriveImageService: IImageService
    {
        public bool IsFileLocalService => false;
        public Dictionary<string, string> Settings { get; set; }

        /// <summary>
        /// Gets or sets the white list of <see cref="System.Uri"/>.
        /// </summary>
        public Uri[] WhiteList { get; set; }

        public string Prefix { get; set; }
        public bool IsValidRequest(string path)
        {
			return true;
        }

        public async Task<byte[]> GetImage(object id)
        {
            byte[] buffer;
			string[] parts = ((string)id).Split('/', '\\');
			id = parts[0];
			var service = GoogleDriveManager.getDriveService();
            
			if (id.ToString().ToLower().StartsWith("n/"))
			{
				// its a name
				FilesResource.ListRequest listRequest = service.Files.List();
				listRequest.Q = string.Format("name = '{0}'", id.ToString().Substring(2)) ;
				listRequest.PageSize = 20;
				listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

				// List files.
				var fileList = listRequest.Execute();
				IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


				if (files != null && files.Count > 0)
				{
					id = files[0].Id;
				}

			}
			MemoryStream stream = await GoogleDriveManager.getFileContent(id.ToString());
			buffer = new byte[stream.Length];
			await stream.ReadAsync(buffer, 0, (int)stream.Length);
			return buffer;
		}

    }
}