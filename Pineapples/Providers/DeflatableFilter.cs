﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;

namespace Pineapples.Providers
{
	public class Deflatable : ActionFilterAttribute
	{
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
		}

		//https://stackoverflow.com/questions/16688248/modify-httpcontent-actionexecutedcontext-response-content-in-onactionexecuted
		public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
		{
			// if Accept-Encoding header includes the customised option "JSONDeflate"
			// and we have a dataset or data table, then deflate
			if (actionExecutedContext.Request.Headers.Contains("X-Json-Deflate"))
			{
				System.Data.DataTable dt = null;
				if (actionExecutedContext.Response.TryGetContentValue(out dt))
				{
					actionExecutedContext.Response = Deflate(dt);
				}
				System.Data.DataSet ds = null;
				if (actionExecutedContext.Response.TryGetContentValue(out ds))
				{
					actionExecutedContext.Response = Deflate(ds);
				}

			}
		}


		/// <summary>
		/// In this controller, use the 'deflating' json serializer since these arae likely to be big
		/// </summary>
		/// <param name="dt">the datatable to serialize</param>
		/// <returns></returns>
		private HttpResponseMessage Deflate(System.Data.DataTable dt)
		{
			var formatter = new System.Net.Http.Formatting.JsonMediaTypeFormatter();

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

			var stream = Softwords.Web.Json.DatasetSerializer.toStream(dt);
			response.Content = new StreamContent(stream);
			response.Content.Headers.ContentType =
				new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
			return response;
		}
		private HttpResponseMessage Deflate(System.Data.DataSet ds)
		{
			var formatter = new System.Net.Http.Formatting.JsonMediaTypeFormatter();

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

			var stream = Softwords.Web.Json.DatasetSerializer.toStream(ds);
			response.Content = new StreamContent(stream);
			response.Content.Headers.ContentType =
				new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
			return response;
		}

	}
}