﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
    public class ExamController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        public ActionResult Upload()
        {
            return View();
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult UploadConfirm()
        {
            return View();
        }

        // GET: Exam
        [Authorize]
        public ActionResult Searcher(string version)
        {
            // pass through the data needed for the model
            ExamSearcherModel model = new ExamSearcherModel();
            string viewName = "ExamSearcher";

            switch (CurrentUser.MenuKey)
            {
                default:
                    viewName = viewName + version;
                    break;
            }
            return View(viewName, model);
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
            return View("ExamItem");
        }

        public ActionResult SearcherComponent()
        {
            return View();
        }
    }
}