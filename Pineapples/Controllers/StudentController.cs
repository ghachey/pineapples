﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
    public class StudentController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: Student
        [Authorize]
        public ActionResult Searcher(string version)
        {
            // pass through the data needed for the model
            StudentSearcherModel model = new StudentSearcherModel();
            string viewName = "StudentSearcher";

            switch (CurrentUser.MenuKey)
            {
                default:
                    viewName = viewName + version;
                    break;
            }
            return View(viewName, model);
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
            return View("StudentItem");
        }

        public ActionResult SearcherComponent()
        {
            return View();
        }

        #region Student-centric components
        // Included below as ideas for future student profile tabs
        public ActionResult EnrollmentList()
        {
            return View();
        }


        public ActionResult ScoreCardList()
        {
            return View();
        }

        public ActionResult DocumentList()
        {
            return View();
        }        
        #endregion
    }
}