﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Pineapples.Hubs
{
    /// <summary>
    /// this interface may be called by the client
    /// normally you would use this 
    /// </summary>
    public interface IPineapplesCodeClient : IHub
    {
        [HubMethodName("codeSetChange")]
        void CodeSetChange(string name);
    }
    /// <summary>
    /// this interface defines the callbcks that the server can make to the client
    /// the hub is a generic based on this interface
    /// Clients.All exposes this interface
    /// </summary>
    public interface IPineapplesCodeCallbacks 
    {
        /// <summary>
        /// when a code set is changed, notify all listeners
        /// the listener can then read the code set
        /// </summary>
        /// <param name="name">name of the code set that got changed</param>
        void BroadcastCodeSetChange(string name);
    }
    [HubName("codehub")]
    public class PineapplesCodeHub : Hub<IPineapplesCodeCallbacks>, IPineapplesCodeClient
    {
        public override Task OnConnected()
        {
            return base.OnConnected();
        }
        public void CodeSetChange(string name)
        {
            Clients.All.BroadcastCodeSetChange(name);
        }
    }
}