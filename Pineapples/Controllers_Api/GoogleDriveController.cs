﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
//using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Google.Apis.Drive.v3.Data;
using System.Xml.Linq;

namespace Pineapples.Controllers
{
	/// <summary>
	/// THIS CLASS IS FOR TESTING
	/// These endpoints just demonstrate the major functions in the Api
	/// Whether this class survives into production is moot.
	/// Controllers such as the SchoolAccreditation controller will work with GoogDriveManager
	/// class to do most of these operations
	/// To get a document, you can use the ImageService google drive implementation; e.g.
	/// like this:
	/// https://localhost:44301/google/1CUdq8bGBq4DoGw63XafDRW0kqUg-iAQn
	/// </summary>
	[RoutePrefix("api/googledrive")]
	public class GoogleDriveController : PineapplesApiController
	{
		public GoogleDriveController(DataLayer.IDSFactory factory) : base(factory) { }


		[HttpGet]
		[Route("metadata/{fileID}")]
		public async Task<File> getFileMetaData(string fileID)
		{
			return await GoogleDriveManager.getFileMetaData(fileID);
		}

		[HttpGet]
		[Route("files")]
		public async Task<FileList> getFiles()
		{
			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();


			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			listRequest.PageSize = 20;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";
			listRequest.Q = "mimeType = 'image/png'";
			listRequest.Q = "(properties has { key='Uploaded' and value = 'POSITIVE'})";

			// List files.
			//var fileList = listRequest.Execute();
			var searchProps = new Dictionary<string, string>();
			searchProps.Add("Uploaded", "POSITIVE");
			var fileList = await GoogleDriveManager.FindFiles(searchProps);

			IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


			if (files != null && files.Count > 0)
			{
				foreach (var file in files)
				{
				//	Console.WriteLine("{0} ({1})", file.Name, file.Id);
				}
			}
			else
			{
				//Console.WriteLine("No files found.");
			}

			var jsonObject = new
			{
				files
			};
			// get the next page
			//
			return fileList;
		}

		[HttpGet]
		[Route("upload")]
		public object Upload()
		{
			var fileMetadata = new File()
			{
				Name = "vermdata_fedemis.xml"
			};

			// Create Drive API service.
			var service = getDriveService();

			FilesResource.CreateMediaUpload request;
			using (var stream = new System.IO.FileStream(
				System.Web.Hosting.HostingEnvironment.MapPath(@"~/assets_local/data/vermdata_fedemis.xml"),
									System.IO.FileMode.Open))
			{
				request = service.Files.Create(
					fileMetadata, stream, "text/xml");
				request.Fields = "id";
				request.Upload();
			}
			var file = request.ResponseBody;
			return file;
		}

		[HttpGet]
		[Route("download")]
		public object Download()
		{

			// Create Drive API service.
			var service = getDriveService();

			var fileId = "1CUdq8bGBq4DoGw63XafDRW0kqUg-iAQn";
			var request = service.Files.Get(fileId);
			var stream = new System.IO.MemoryStream();

			// Add a handler which will be notified on progress changes.
			// It will notify on each chunk download and when the
			// download is completed or failed.
			request.MediaDownloader.ProgressChanged +=
				(Google.Apis.Download.IDownloadProgress progress) =>
				{
					switch (progress.Status)
					{
						case Google.Apis.Download.DownloadStatus.Downloading:
							{
								//Console.WriteLine(progress.BytesDownloaded);
								break;
							}
						case Google.Apis.Download.DownloadStatus.Completed:
							{
								//Console.WriteLine("Download complete.");
								break;
							}
						case Google.Apis.Download.DownloadStatus.Failed:
							{
								//Console.WriteLine("Download failed.");
								break;
							}
					}
				};
			request.Download(stream);
			return stream;
		}

		[HttpGet]
		[Route("complete")]
		public object Complete()
		{
			var fileId = "1CUdq8bGBq4DoGw63XafDRW0kqUg-iAQn";
			IDictionary<string, string> propertiesToSet = new Dictionary<string, string>();
			propertiesToSet.Add("Uploaded", "POSITIVE");
			propertiesToSet.Add("InspectionId", "98765");
			return GoogleDriveManager.SetProperties(fileId, propertiesToSet);

		}



		[HttpGet]
		[Route("searchforfolders")]
		public object searchForFolders()
		{

			// Create Drive API service.
			var service = getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			listRequest.Q = "mimeType = 'application/vnd.google-apps.folder'";
			listRequest.PageSize = 20;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

			// List files.
			var fileList = listRequest.Execute();
			IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


			if (files != null && files.Count > 0)
			{
				foreach (File file in files)
				{
					//	Console.WriteLine("{0} ({1})", file.Name, file.Id);
				}
			}
			else
			{
				//Console.WriteLine("No files found.");
			}
		
			return fileList;
		}

		[HttpGet]
		[Route("teamdrive")]
		public object TeamDrive()
		{
			var service = GoogleDriveManager.getDriveService();
			var teamDriveMetadata = new TeamDrive()
			{
					Name = "MIEMIS"
			};
			var requestId = System.Guid.NewGuid().ToString();
			var request = service.Teamdrives.Create(teamDriveMetadata, requestId);
			
			request.Fields = "id, name";
			var teamDrive = request.Execute();
			//Console.WriteLine("Team Drive ID: " + teamDrive.Id);
			var newOrganizerPermission = new Permission()
			{
				Type = "user",
				Role = "organizer",
				EmailAddress = "bdlsoftwords@gmail.com"
			};
			var permissionRequest =service.Permissions.Create(
		  newOrganizerPermission,
		  teamDrive.Id
		);
			permissionRequest.UseDomainAdminAccess = true;
			permissionRequest.SupportsTeamDrives = true;
			permissionRequest.Fields = "id";
			var permissionResult = permissionRequest.Execute();

			return teamDrive;
		}

		[HttpGet]
		[Route("prepfolder")]
		public object PrepFolders()
		{

			// Create Drive API service.
			var service = getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			listRequest.Q = string.Format("mimeType = 'application/vnd.google-apps.folder' and name='{0}'", Context);
			listRequest.PageSize = 1;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

			// List files.
			var fileList = listRequest.Execute();
			IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


			if (files != null && files.Count > 0)
			{
				return files[0].Id;
			}
			else
			{
				File fileMetadata = new File()
				{
					Name = Context,
					MimeType = "application/vnd.google-apps.folder"

				};

				FilesResource.CreateRequest request;
				request = service.Files.Create(fileMetadata);
				request.Fields = "id";
				var file = request.Execute();
				return file.Id;
			}

			return fileList;
		}

		[HttpGet]
		[Route("downloadxml")]
		public object Downloadxml()
		{

			// Create Drive API service.
			var service = getDriveService();

			var fileId = "11vh_1c7lIqOdJmT8DaH82ukqbScieoF5";
			var request = service.Files.Get(fileId);
			var stream = new System.IO.MemoryStream();

			// Add a handler which will be notified on progress changes.
			// It will notify on each chunk download and when the
			// download is completed or failed.
			request.MediaDownloader.ProgressChanged +=
				(Google.Apis.Download.IDownloadProgress progress) =>
				{
					switch (progress.Status)
					{
						case Google.Apis.Download.DownloadStatus.Downloading:
							{
								//Console.WriteLine(progress.BytesDownloaded);
								break;
							}
						case Google.Apis.Download.DownloadStatus.Completed:
							{
								//Console.WriteLine("Download complete.");
								break;
							}
						case Google.Apis.Download.DownloadStatus.Failed:
							{
								//Console.WriteLine("Download failed.");
								break;
							}
					}
				};
			request.Download(stream);
			stream.Position = 0;
			XDocument vermdata = XDocument.Load(stream);
			return vermdata;
		}
		//[HttpGet]
		//[Route("deletesubfolder")]
		//public object deleteSubFolder()
		//{
		//	File fileMetadata = new File()
		//	{
		//		Name = "fedemis",
		//		MimeType = "application/vnd.google-apps.folder"

		//	};

		//	// Create Drive API service.
		//	var service = getDriveService();
		//	FilesResource.ListRequest listRequest = service.Files.List();
		//	listRequest.Q = string.Format("mimeType = 'application/vnd.google-apps.folder' and name='{0}'", Context);
		//	listRequest.PageSize = 1;
		//	listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

		//	FilesResource.CreateRequest request;
		//	request = service.Files.Delete(file.Id);
		//	return request.Execute();
		//}

		[HttpGet]
		[Route("subfolder")]
		public object createSubFolder()
		{
			{
				File fileMetadata = new File()
				{
					Name = "fedemis",
					MimeType = "application/vnd.google-apps.folder"

				};

				// Create Drive API service.
				var service = getDriveService();

				FilesResource.CreateRequest request;
				request = service.Files.Create(fileMetadata);
				request.Fields = "id";
				var file = request.Execute();
				return file;
			}
		}


		private DriveService getDriveService()
		{
			var fsSource = new System.IO.FileStream
			  (System.Web.Hosting.HostingEnvironment.MapPath(@"~/google_api.json"),
				System.IO.FileMode.Open, System.IO.FileAccess.Read);

			string[] Scopes = { DriveService.Scope.Drive };
			string ApplicationName = "Pacific EMIS";

			GoogleCredential credential = GoogleCredential.FromStream(fsSource)
				.CreateScoped(Scopes);

			// Create Drive API service.
			var service = new DriveService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = ApplicationName,
			});

			return service;
		}
	}

}
