﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;

// TODO: Obsolete this file. Routes in here are declared and used from
// within SchoolController.cs

namespace Pineapples.Controllers
{
    [RoutePrefix("api/survey")]
    public class SurveyEntryController : PineapplesApiController
    {
        public SurveyEntryController(DataLayer.IDSFactory factory) : base(factory) { }

        #region Pupil Table
        [HttpGet]
        [Route(@"pupiltable/{schoolNo}/{year:int}/{tableName}")]
        public object PupilTable(string schoolNo, int year, string tableName)
        {
            return Factory.SurveyEntry().PupilTable(schoolNo, year, tableName); //.ResultSet;
        }

        [HttpPost]
        [Route(@"pupiltable")]
        public object SavePupilTable(PupilTable pt)
        {
            return Factory.SurveyEntry().SavePupilTable(pt, this.User.Identity.Name); //.ResultSet;
        }
        #endregion

        #region ResourceList
        [HttpGet]
        [Route(@"resourcelist/{schoolNo}/{year:int}/{category}")]
        public object ResourceList(string schoolNo, int year, string category)
        {
            return Factory.SurveyEntry().ResourceList(schoolNo, year, category); //.ResultSet;
        }

        [HttpPost]
        [Route(@"resourcelist")]
        public object SaveResourceList(ResourceList rl)
        {
            return Factory.SurveyEntry().SaveResourceList(rl, this.User.Identity.Name); //.ResultSet;
        }
        #endregion
    }
}
