﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentSearcher.aspx.cs" Inherits="Pineapples.Views.StudentSearcher" %>
<div class="container searcher" >
        <div class="col-md-3">
            <div class="form-group">
                <label for="srchSurname">Student Card ID</label>
                <input id="srchSurname" type="text" class="form-control" ng-model="vm.params.StudentCardID" />
            </div>

        </div>
    <div class="col-md-3">
            <div class="form-group">
                <label for="srchSurname">Given Name</label>
                <input id="srchSurname" type="text" class="form-control" ng-model="vm.params.StudentGiven" />
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="srchPayroll">Family Name</label>
                <input id="srchPayroll" type="text" class="form-control" ng-model="vm.params.StudentFamilyName" />
            </div>
        </div>
    <div class="col-md-2">
            <div class="form-group">
                <label for="srchInYear">Gender</label>
                <input id="srchInYear" type="text" class="form-control" ng-model="vm.params.StudentGender" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="srchAtSchool">Ethnicity</label>
                <select class="form-control" id="srchAtSchool"
                      ng-model="vm.params.StudentEthnicity"
                      ng-options="r.C as r.N for r in vm.lookups.cache.ethnicities">
                  <option value=""></option>
              </select>
            </div>
        </div>        

        <div class="col-xs-1">

            <div class="form-group">
                <button class="btn btn-default findNow" id="findNow" ng-click="vm.FindNow()" >
                    Find</button>
            </div>
            <div class="form-group">
                <button id="Reset" class="btn clearSearch" ng-click="vm.theFilter.Reset()" >
                    Clear</button>

            </div>
        </div>


</div>

