﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkforceSearcher.aspx.cs" Inherits="Pineapples.Views.mvc.Workforce.WorkforceSearcher" %>

<!-- controller as vm -->
<div>
     <div class="col-md-3">
            <div class="form-group">
                <label for="asAtDate">Payroll</label>
                <input id="asAtDate" type="date" class="form-control" ng-model="vm.theFilter.params.asAtDate" />
            </div>
        </div>
     <button ng-click="vm.theFilter.FindNow()">Get data</button>
</div>
