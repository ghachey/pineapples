' TableOps
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit
Dim gRows As Variant
Dim iLoops As Integer


Sub NewPupil()
On Error GoTo localErr
gRows = InputBox("How many Rows to add?", , 1)
If gRows = "" Then Exit Sub
'For iLoops = 1 To gRows
    Dim lo As ListObject
    Set lo = shtStudents.ListObjects(1)
    Application.ScreenUpdating = False
    For iLoops = 1 To gRows
        NewRow lo
    Next iLoops

ExitDoor:
    Application.ScreenUpdating = True
    Exit Sub


localErr:
    MsgBox Err.Description
    Resume ExitDoor
    
End Sub

Sub NewStaff()
On Error GoTo localErr
gRows = InputBox("How many Rows to add?", , 1)
If gRows = "" Then Exit Sub
'For iLoops = 1 To gRows
    Dim lo As ListObject
    Set lo = shtStaff.ListObjects(1)
    Application.ScreenUpdating = False
    For iLoops = 1 To gRows
        NewRow lo
    Next iLoops
    
ExitDoor:
    Application.ScreenUpdating = True
    Exit Sub


localErr:
    MsgBox Err.Description
    Resume ExitDoor
End Sub

Sub NewWASH()
gRows = InputBox("How many Rows to add?", , 1)
If gRows = "" Then Exit Sub
'For iLoops = 1 To gRows
    Dim lo As ListObject
    Set lo = shtWash.ListObjects(1)
    For iLoops = 1 To gRows
        NewRow lo
    Next iLoops
End Sub

Sub NewSchool()
gRows = InputBox("How many Rows to add?", , 1)
If gRows = "" Then Exit Sub
'For iLoops = 1 To gRows
    Dim lo As ListObject
    Set lo = shtSchools.ListObjects(1)
    For iLoops = 1 To gRows
        NewRow lo
    Next iLoops
End Sub

Sub DeleteRow()
Dim lo As ListObject
Set lo = ActiveSheet.ListObjects(1)
Dim r As Range

Set r = Selection

DeleteListObjectRow lo, r
End Sub

Sub DeleteListObjectRow(lo As ListObject, r As Range, Optional silent As Boolean = False)



Set r = Application.Intersect(r, lo.DataBodyRange)

If r Is Nothing Then
    MsgBox "Select a row in the table"
    Exit Sub
End If

If r.Rows.Count > 1 And (r.Columns.Count < lo.ListColumns.Count) Then
    MsgBox "To delete multiple rows, select the entire rows."
    Exit Sub
End If

Dim result As Variant

If silent = False Then
    If r.Rows.Count = 1 Then
        result = MsgBox("Delete row " & r.row & "?", vbDefaultButton1 + vbOKCancel, "Delete Row")
    ElseIf r.Rows.Count >= lo.ListRows.Count Then
        result = MsgBox("Delete all rows (" & r.row & "-" & r.row + r.Rows.Count - 1 & ") ?", vbDefaultButton1 + vbOKCancel, "Delete Rows")
    Else
        result = MsgBox("Delete rows " & r.row & "-" & r.row + r.Rows.Count - 1 & "?", vbDefaultButton1 + vbOKCancel, "Delete Rows")
    End If
Else
    result = vbOK
End If

If result = vbOK Then
    Dim sht As Worksheet
    Set sht = r.Parent
    sht.Unprotect
    'eugene - if the first data row (row 2) gets deleted, we lost all the column formats - so dont allow it to be deleted.
    ' brain - clear it inseatd of deleting it if we are trying to delete every row
    If r.Rows.Count >= lo.ListRows.Count Then
        clearRow lo, 1
        If r.Rows.Count = 1 Then
            Exit Sub
        End If
        ' resize the range to really delete the remaining rows selected
        Set r = r.Offset(1, 0).Resize(r.Rows.Count - 1)
    End If


    r.EntireRow.Delete
    doProtect sht

End If
End Sub


Sub DeleteAllRows()
Dim lo As ListObject
Set lo = ActiveSheet.ListObjects(1)

lo.DataBodyRange.EntireRow.Select
DeleteRow
End Sub

' clear the 4 data entry lists
Sub ClearAllData()
Dim lo As ListObject
Set lo = ThisWorkbook.SchoolList
DeleteListObjectRow lo, lo.DataBodyRange.EntireRow, True
Set lo = ThisWorkbook.StudentList
DeleteListObjectRow lo, lo.DataBodyRange.EntireRow, True
Set lo = ThisWorkbook.StaffList
DeleteListObjectRow lo, lo.DataBodyRange.EntireRow, True
Set lo = ThisWorkbook.WashList
DeleteListObjectRow lo, lo.DataBodyRange.EntireRow, True

End Sub

Sub clearRow(lst As ListObject, idx As Integer)
Dim col As ListColumn
Dim r As Range
For Each col In lst.ListColumns
    Set r = col.DataBodyRange(idx, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    '' its calculated
    Else
        r.ClearContents
    End If
Next
End Sub
Sub NewRow(lo As ListObject)
Dim lr As ListRow
lo.Parent.Unprotect
Set lr = lo.ListRows.Add(, True)
lr.Range.Cells(1, 3).Select
doProtect lo.Parent
End Sub


Sub doProtect(sht As Worksheet)
 sht.Protect DrawingObjects:=True, Contents:=True, Scenarios:=True _
        , AllowInsertingRows:=False, AllowDeletingRows:=False, AllowSorting:=True, AllowFiltering:=True
End Sub

Sub RefreshTables()
Dim sht As Worksheet
Dim pv As PivotTable
Dim pc As PivotCache


For Each pc In ThisWorkbook.PivotCaches
    pc.Refresh
Next


End Sub


Sub hideStudent(ByVal hide As Boolean)
Dim rStudent As Range

Set rStudent = Range("colsStudent").EntireColumn

rStudent.Hidden = hide
End Sub

Sub hideEnrolment(ByVal hide As Boolean)
Dim rStudent As Range

Set rStudent = Range("colsEnrolment").EntireColumn

rStudent.Hidden = hide
End Sub

Sub hideOutcome(ByVal hide As Boolean)
Dim rStudent As Range

Set rStudent = Range("colsOutcome").EntireColumn

rStudent.Hidden = hide
End Sub

Sub enableRowOps(enable As Boolean)
Dim shpN As Shape
Dim shpD As Shape

Dim color As Variant
Set shpN = shtStudents.Shapes("btnNew")
Set shpD = shtStudents.Shapes("btnDelete")

If enable Then
    shpN.OnAction = "NewPupil"
    shpD.OnAction = "DeleteRow"
    shpN.Fill.ForeColor.RGB = RGB(64, 192, 64)
    shpD.Fill.ForeColor.RGB = RGB(192, 64, 64)
Else
    shpN.OnAction = ""
    shpD.OnAction = ""
    shpN.Fill.ForeColor.RGB = RGB(192, 192, 192)
    shpD.Fill.ForeColor.RGB = RGB(192, 192, 192)
    
End If
End Sub


Sub AllView()
shtStudents.Unprotect
hideStudent False
hideEnrolment False
hideOutcome False
enableRowOps True
doProtect shtStudents
End Sub

Sub EnrolmentView()
shtStudents.Unprotect
hideStudent False
hideEnrolment False
hideOutcome True
enableRowOps True
doProtect shtStudents
End Sub

Sub OutcomeView()
shtStudents.Unprotect
hideStudent True
hideEnrolment True
hideOutcome False
enableRowOps False
doProtect shtStudents
End Sub

