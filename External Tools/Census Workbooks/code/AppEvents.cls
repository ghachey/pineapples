' AppEvents
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit

Public WithEvents app As Application


Private Sub app_WorkbookBeforeClose(ByVal Wb As Workbook, Cancel As Boolean)
Application.CalculateFull
End Sub

Private Sub app_WorkbookBeforeSave(ByVal Wb As Workbook, ByVal SaveAsUI As Boolean, Cancel As Boolean)
Application.CalculateFull
End Sub

Private Sub app_WorkbookOpen(ByVal Wb As Workbook)
Application.CalculateFull
End Sub


