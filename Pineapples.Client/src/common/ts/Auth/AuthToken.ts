﻿module Sw.Auth {


  // This interface matches the ASpNet Identity
  export interface IPasswordValidator {
    RequiredLength: number;
    RequireNonLetterOrDigit: boolean;
    RequireLowercase: boolean;
    RequireUppercase: boolean;
    RequireDigit: boolean;
    // regex representing these rules
    RegEx: string;
  }

  export class PasswordValidator implements IPasswordValidator {
    public RequiredLength: number;
    public RequireNonLetterOrDigit: boolean;
    public RequireLowercase: boolean;
    public RequireUppercase: boolean;
    public RequireDigit: boolean;
    // regex representing these rules
    public RegEx: string;

    constructor(data: IPasswordValidator) {
      this.RequireDigit = data.RequireDigit;
      this.RequiredLength = data.RequiredLength;
      this.RequireLowercase = data.RequireLowercase;
      this.RequireNonLetterOrDigit = data.RequireNonLetterOrDigit;
      this.RequireUppercase = data.RequireUppercase;
      this.RegEx = this.buildRegEx();
    }
    private buildRegEx() {
      // /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8}.*$/
      let rx = "^";
      if (this.RequireUppercase) {
        rx = rx + "(?=.*[A-Z])";
      }
      if (this.RequireLowercase) {
        rx = rx + "(?=.*[a-z])";
      }
      if (this.RequireDigit) {
        rx = rx + "(?=.*[0-9])";
      }
      if (this.RequireNonLetterOrDigit) {
        rx = rx + "(?=.*[!@#$&*])";
      }
      rx = rx + ".{" + this.RequiredLength + "}.*$";
      return rx;
    }
  }

  /**
   * Identity represents the current system user
   */
  export interface IIdentity {
    isAuthenticated: boolean;
    // this just wraps isAuthenticated in a resolved promise.
    // this is useful for ui-router and permissions that expect a promise
    isAuthenticatedPromise: ng.IPromise<boolean>;
    isDomainUser: boolean;

    userName: string;
    token: string;
    tokenhash(seed: string): string;      //mangled token


    roles: string[];
    inRole(role: string): boolean;
    inAnyRole(roles: string[]): boolean;

    // filter
    filters: any;
    hasFilter(filter: string): boolean;
    getFilter(filter: string): string;

    // permissionHash encodes the read/write permissions across system areas

    permissionHash: string;
  }

  /**
   * Identity represents the current system user
   */
  export class Identity implements IIdentity {

    static $inject = ["$rootScope", "$q", "storage"];
    constructor(rootscope: ng.IRootScopeService, private _q: ng.IQService, private _storage) {
      // this._getCurrentAuthentication();
      // rootscope.$on("auth:login-success", this._getCurrentAuthentication);
      // rootscope.$on("auth:login-error", this._getCurrentAuthentication);
      // rootscope.$on("auth:logout-success", this._getCurrentAuthentication);
      // rootscope.$on("auth:logout-error", this._getCurrentAuthentication);
    }

    public get isAuthenticated(): boolean {
      var authData = this._storage.get('authorizationData');
      return (authData ? true : false);
    }

    public inRole = (role: string) => {
      if (this.roles && this.roles.length) {
        return (this.roles.indexOf(role) >= 0 ? true : false);
      }
      return false;
    };

    public inAnyRole = (roles: string[]) => {
      for (var i = 0; i < roles.length; i++) {
        if (this.inRole(roles[i])) {
          return true;
        }
      }
      return false;
    };

    public hasFilter: (string) => boolean = (filter: string) => {
      let fltr = this.getFilter(filter);
      if (fltr === undefined || fltr === null) {
        return false;
      }
      return true;
    };

    public getFilter = (filter: string) => {
      if (this.filters) {
        return this.filters[filter];
      }
      return undefined;
    };

    public get isAuthenticatedPromise() {
      var d = this._q.defer<boolean>();
      var authData = this._storage.get('authorizationData');
      if (authData) {
        d.resolve(true);
      } else {
        d.reject(false);
      }
      return d.promise;
    }

    public get userName(): string {
      return this._getAuthenticationItem("userName");
    }

    public get token(): string {
      return this._getAuthenticationItem("token");
    }
    public tokenhash(seed: string) {
      // get the first character of the seed, convert to lower case
      let i = (seed.charCodeAt(0) % 8) + 1;
      let t = this.token;
      let c1 = t.substr(0, 1);
      let c2 = t.substr(i, 1);
      if (c1 === c2)
        return t;
      //t = t.replace(c1, " ");
      //t = t.replace(c2, c1);
      //t = t.replace(" ", c2);
      t = t.split(c1).join(" ");
      t = t.split(c2).join(c1);
      t = t.split(" ").join(c2);
      t = t + i.toString();
      return t;

    }

    public get home(): string {
      return this._getAuthenticationItem("home");
    }

    public get menu(): any[] {
      return this._getAuthenticationItem("menu");
    }

    public get roles(): string[] {
      return this._getAuthenticationItem("roles");
    }

    public get filters(): string[] {
      return this._getAuthenticationItem("filters");
    }

    public get permissionHash(): string {
      return this._getAuthenticationItem("p");
    }


    public get isDomainUser(): boolean {
      return this._getAuthenticationItem("isDomainUser");
    }

    private _getAuthenticationItem(item) {
      var authData = this._storage.get('authorizationData');
      return (authData ? authData[item] : "");
    }
  }
  export interface IAuthenticationService {
    logOut: () => void;
    login: (userName: string, password: string, persist: boolean) => ng.IPromise<any>;
    changePassword: (oldPassword: string, newPassword: string, confirmPassword: string) => ng.IPromise<any>;
    register: (userName: string, email: string, password: string, confirmPassword: string, menuKey: string, country: string, permission: string) => ng.IPromise<any>;
    getPasswordInfo: () => ng.IPromise<IPasswordValidator>;

  }
  // class represeenting the authentication management
  class AuthenticationService implements IAuthenticationService {

    static $inject = ["$http", "$q", "$rootScope", "identity", "storage", "httpBuffer"];
    constructor(private _http: ng.IHttpService, private _q: ng.IQService, private _rootscope: ng.IRootScopeService,
      public identity: IIdentity, private _storage: Storage, private _httpBuffer: IHttpBuffer) {
    }

    public login(userName: string, password: string, persist: boolean) {
      let data = "grant_type=password&username=" + userName + "&password=" + password;
      let deferred = this._q.defer();

      this._http.post('api/token', data,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then(response => {
          // save everything sent back from the server
          // here we need to process this returned data
          // -- menu is a string, not a json array
          // -- rolesis a common separated string, not a json array
          // -- ng-token-auth has a configuration method for this transformation
          // -- likewise an interceptor from http...
          var data = this._transformResponse(response.data);
          this._storage.set('authorizationData', data, persist);
          this._rootscope.$broadcast("auth:login-success", data);
          this._httpBuffer.retryAll();
          deferred.resolve(data);
        },
        (err) => {
          this.logOut();
          this._rootscope.$broadcast("auth:login-error");
          deferred.reject(err);
        });
      return deferred.promise;
    }

    public logOut = () => {
      this._storage.remove('authorizationData');
      this._rootscope.$broadcast("auth:logout-success");
    }

    private _transformResponse(response) {
      // code here is just adapated from the earlier ng-token-auth config
      // support filters set up as claims

      let result = {
        token: response.access_token,
        userName: response.userName,
        expiry: response[".expires"],
        roles: response.roles.split(","),
        menu: JSON.parse(response.menu),
        home: response.home,
        isDomainUser: (response.isDomainUser === "yes" ? true : false),
        filters: null,
        p: response.p    // permissions
      };

      if (response.filters) {
        let fltr = {};
        let arr = response.filters.split(",");
        arr.forEach(str => {
          let stra = str.split(": ");
          fltr[stra[0]] = stra[1];
        });
        result.filters = fltr;
      }
      return result;
    }

    public changePassword(oldPassword: string, newPassword: string, confirmPassword: string) {
      let data = {
        NewPassword: newPassword,
        ConfirmPassword: confirmPassword,
        OldPassword: oldPassword
      };

      let deferred = this._q.defer();

      this._http.post('_api/Account/ChangePassword', data,
        { headers: { 'Content-Type': 'application/json' } }).then(response => {
          // change password returns 200 with no body on success
          deferred.resolve();
        },
        (err) => {
          this._rootscope.$broadcast("auth:changePassword-error");
          deferred.reject(err);
        });
      return deferred.promise;
    }

    public register(userName: string, email: string, password: string, confirmPassword: string, menuKey: string, country: string, permission: string) {
      let data = {
        UserName: userName,
        Email: email,
        Password: password,
        ConfirmPassword: confirmPassword,
        MenuKey: menuKey,
        Country: country,
        Permission: permission
      };

      let deferred = this._q.defer();

      this._http.post('_api/Account/register', data,
        { headers: { 'Content-Type': 'application/json' } }).then(response => {
          // change password returns 200 with no body on success
          deferred.resolve();
        },
        (err) => {
          this._rootscope.$broadcast("auth:register-error");
          deferred.reject(err);
        });
      return deferred.promise;
    }

    // return the password strength configuration
    // this retrieves the settings from the Asp.Net Identity PasswordValidator on the server
    // (which is itself configured in ApplicationUserManager)
    // the end point is in Softwords.Web

    public getPasswordInfo(): ng.IPromise<IPasswordValidator> {
      return this._http.get("_api/account/PasswordInfo").then(
        (response) => new PasswordValidator(<IPasswordValidator>(response.data))
      );
    }
  }

  class AuthenticationInterceptor implements ng.IHttpInterceptor {
    constructor(private _q: ng.IQService, private _rootscope: ng.IRootScopeService, private _identity: IIdentity
      , private _httpBuffer: IHttpBuffer) { }
    /**
     * Request
     * add the bearer token always if we have it
     */
    public request = config => {
      config.headers = config.headers || {};

      if (this._identity.isAuthenticated) {
        config.headers.Authorization = 'Bearer ' + this._identity.token;
      }
      return config;
    };

    public responseError = (rejection) => {
      var config = rejection.config || {};
      if (!config.ignoreAuthModule) {
        switch (rejection.status) {
          case 401:
            // http calls are asynchronous, and a previous 401 may have triggered a fresh login
            // so, it's possible that this call was made with an obselete token.
            // if so, we should try again with the current token before trying to login again

            if (this._identity.isAuthenticated) {
              if (config.headers === undefined || config.headers.Authorization !== 'Bearer ' + this._identity.token) {
                // we can submit the original request again - it will get the new token this time
                let deferred = this._q.defer();
                this._httpBuffer.retry(config, deferred);
                return deferred.promise;
              }
            }
            // the request that failed used the bearer token we have, so the token is the problem
            // get a fresh one
            let deferred = this._q.defer();
            this._httpBuffer.append(config, deferred);
            this._rootscope.$broadcast('auth:login-required', rejection);
            return deferred.promise;
          case -1:
          case 502:
              // allow a chance to retry if we have lost the network connection
            deferred = this._q.defer();
            this._httpBuffer.append(config, deferred);
            this._rootscope.$broadcast('auth:no-connection', rejection);
            return deferred.promise;
              
          case 403:
            this._rootscope.$broadcast('event:auth-forbidden', rejection);
            break;
        }
      }
      // otherwise, default behaviour
      return this._q.reject(rejection);
    };

    // the same strategy to construct a "factory method" that is used for typescript directives
    public static factory() {
      let interceptor = ($q, $rootScope, identity, httpBuffer) => {
        return new AuthenticationInterceptor($q, $rootScope, identity, httpBuffer);
      };
      interceptor["$inject"] = ["$q", "$rootScope", "identity", "httpBuffer"];
      return interceptor;
    }
  }

  angular
    .module("sw.common")
    .service("identity", Identity)
    .service("authenticationService", AuthenticationService)

    .config(["$httpProvider", function (provider) {
      provider.interceptors.push(AuthenticationInterceptor.factory());
    }])

    .run(["$rootScope", "identity", (rootscope, identity) => rootscope.identity = identity]);

}
