﻿namespace Sw.Api {
	export interface IDeflatedTable {
		columns: string[];
		rows: any[][];
	}

	export interface IDeflatedTableSet {
		tables: { [tableName: string]: IDeflatedTable}
	}


	export interface IReflator {
		get(url: string, config?: ng.IRequestShortcutConfig): ng.IHttpPromise<any>;
		reflate(table: IDeflatedTable): any;
	}

	class Reflator {

		static $inject = ['$http'];
		constructor(public http: ng.IHttpService) { }

		/**
		 * Reflator get wraps the usual http get, but inserts the custom header
		 * X-Json-Deflate = ON
		 * At the server, any method marked with the attribute [Deflatable]
		 * can respond to this flag by "deflating" a DataTable or DataSet
		 * 
		 * When the data is received, it is converted back to a Json collection.
		 * Note that if the method is not marked [Deflatable] this flag has no effect
		 * 
		 * @param url : url to navigate to
		 * @param config : optional configuration - passed to $http.get
		 */
		public get(url: string, config?: ng.IRequestShortcutConfig) {
			config = config || {};
			config.headers = config.headers || {};
			config.headers["X-Json-Deflate"] = "ON"; 
			return this.http.get(url, config).then(response => {
				if (_.has(response.data, "columns")) {
					response.data = this.reflate(<IDeflatedTable>response.data);
				} else if (_.has(response.data, "tables")) {
					response.data = this.reflateSet(<IDeflatedTableSet>response.data);
				}
				return response;
			});
		}

		/**
		 * decode a compressed datatable
		 * This object has two properties
			* columns: an array of the column names
			* rows: an array of arrays. Each array is the values of the data
			* this will turn this structure into a normal Json collection; ie array of objects
			* Objects in this form are created by Softwords.Web DatasetSerializer
			* @param table: the 'deflated table'
		 */
		public reflate(table: IDeflatedTable) {
			let reflateRow = (row) => {
				let out = {};
				row.forEach((cell, index) => {
					out[table.columns[index]] = cell;
				});
				return out;
			}
			return table.rows.map(reflateRow);
		}

		/**
			* decode a compressed dataset
		  * This is an object with a single property 'tables'
		  * The value is an object where each property name is the table name, 
		  * and the property value is an IDeflatedTable
			* This object has two properties
			* columns: an array of the column names
			* rows: an array of arrays. Each array is the values of the data
			* this will turn this structure into a normal Json collection; ie array of objects
			* Objects in this form are created by Softwords.Web DatasetSerializer
			* @param table
			*/
		public reflateSet(tableset: IDeflatedTableSet) {
			// iterate over all the properties of the object, converting each value to a collection
			return _.mapValues(tableset.tables, (d) => this.reflate(d));
		}

	}
	angular
		.module("sw.common")
		.service("reflator", Reflator)
}