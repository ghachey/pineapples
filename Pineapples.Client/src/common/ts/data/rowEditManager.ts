﻿/**
 * @ngdoc directive
 * @name rowEditManager
 *
 * @description
 * Directive to provide visual feedback in ui-grid for the state of a row edit
 * rowEditManager reflects these states:
 * ** clean - the row is unedited
 * ** dirty - the row has been edited
 * ** invalid - the row is dirty, and has failed client-side validations
 * ** saving - the row is being saved; ie a promise has been raised to save the row, that promise is unresolved
 * ** error - an error occurred saving the row; ie the save promise was rejected
 * ** saved the row has been succssfully saved - ie the save promise was resolved
 *
 * Note that saved is maintained only for a second, then is reset to 'clean'. This provides brief UI to confirm the successful save
 * The row edit trategy is built on this directive, and the PagedListEditableController.
 * It requires the following plug-ins to ui-grid:
 * ** edit
 * ** cellNav
 * ** rowEdit
 * ** validate
 *
 * The default saving in rowEdit ( ie save after expiry of timeout) is turend off. The saving model is modelled on grids such as SSMS / MS Access
 * Specifically a dirty row is saved when -
 * navigating to a new row - if the old row is dirty, it is committed
 * clicking on the rowEditManager.
 * Note that unlike SSMS MS Access, saving is non-blocking - becuase it is implemented as a promise. So, it is possible for a number of rows
 * to be saving simultaneously. The status of each saving row will resolve to saved => clean as the promise resolves.
 * Row editing ultimately relies on the rowEntity - the object attached to the row - to have a post() method to invoke its conventional post REST url.
 * ie it expects restangular to put that post method there.
 *
 */
module Sw.Api {
  const SAVED_DISPLAY_INTERVAL = 2000;

  interface IBindings {
    row: any;
  }
  class Controller implements IBindings {
    public row: any;

    public state: string = "";

    // becuase this is a callback, it is defined with lambda function to bind to 'this'
    private setState = () => {
      if (this.row.isSaving) {
        this.state = "saving";
        return;
      }
      if (this.row.isDeleting) {
        this.state = "deleting";
        return;
      }
      if (this.row.isDeleted) {
        this.state = "deleted";
        return;
      }
      // this is a save error
      if (this.row.isError||this.row.isDeleteError) {
        this.state = this.apiUi.getErrorType(this.row.entity.errorData);
        return;
      }

      if (this.row.isDirty) {
        this.state = "dirty";
        return;
      }
      if (this.state === "saving") {
        this.state = "saved";
        this.$timeout(this.setState, SAVED_DISPLAY_INTERVAL);
        return;
      }
      this.state = "";
    };

    static $inject = ["$scope", "$timeout", "ApiUi"];
    constructor(scope: ng.IScope, public $timeout: ng.ITimeoutService, private apiUi: Sw.Api.IApiUi) {

      scope.$watchGroup(["vm.row.isDirty", "vm.row.isSaving", "vm.row.isDeleting", "vm.row.isDeleted","vm.row.entity.errorData"], this.setState);
    }

    public $onChanges(changes) {
      if (this.row) {
        this.setState();
      }
    }
    public stateMessage() {
      switch (this.state) {
        case "saving":
          return "Saving changes...";
        case "error":
          return "Error - click for details...";
        case "notfound":
          return "Record not found - click for details...";
        case "invalid":
          return "Invalid data - click for details...";
        case "conflict":
          return "Data change conflict - click for details...";
        case "dirty":
          return "Editing - click to save...";
        default:
          return null;
      }
    }
    public clickHandler() {
      if (this.row.isError) {
        this.handleErrorReponse();
        return;
      }
      // the timeout allows the state change to register when
      // we navigate away from the row by clicking the row header

      this.$timeout(() => {
        switch (this.state) {
          case "dirty":
            this.save();
            break;
        }
      },10); 
    }
    public save() {
      console.log("flush");
      // expects the PagedListEditablecontroller as listvm in the parent scope
      this.row.grid.appScope.vm.saveRow(this.row);
    }

    private handleErrorReponse() {
      this.apiUi.showErrorResponse(this.row.entity, this.row.entity.errorData)
        .then(result => {
          switch (result) {
            case "retry":
              this.save();
              break;

            case "overwrite":
              // update the timestamp
              this.row.entity.pRowversion = this.row.entity.errorData.data.pRowversion;
              this.save();
              break;
            case "discard":
              // to do we already have all the data in nv, but
              // can;t use it becuase it is not restangularized
              // however merging nv back into this.pa seems need to be handled case-by-case
              angular.extend(this.row.entity, this.row.entity.errorData.data);
              // housekeeping - restangularize

              // remove the error, the error flag, and the dirty flag
              delete this.row.entity.errorData;
              this.gridApi.rowEdit.setRowsClean([this.row.entity]);   // these apis take the entity, not the grid row
              break;
            case "goto":
              // scroll to the offending field.
              let field: string = this.row.entity.errorData.data.field;
              let coldef = this.grid.getColDef(field);
              this.gridApi.cellNav.scrollToFocus(this.row.entity, coldef);
          };
        });
    }
    private get grid() {
      return this.row.grid;
    }

    private get gridApi() {
      return this.row.grid.api;
    }
  }

  class RowEditManager implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        row: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "common/RowEditManager";
    }
  }

  angular
    .module("sw.common")
    .component("rowEditManager", new RowEditManager());
}
