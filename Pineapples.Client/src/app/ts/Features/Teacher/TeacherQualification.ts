﻿namespace Pineapples.Teachers {
  
  export class TeacherQualification extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(teacherData) {
      super();
      this._transform(teacherData);
      angular.extend(this, teacherData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let tqual = new TeacherQualification(resultSet);
      return tqual;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).trYear + ' ' + (<any>this).trQual;
    }
    public _type() {
      return "teacherqualification";
    }
    public _id() {
      return (<any>this).trID
    }

    public _transform(newData) {
      // convert these incoming data values
      return super._transform(newData);
    }

    private trYear;
    private trQual;

  }
}