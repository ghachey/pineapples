﻿namespace Pineapples.Students {

  interface IBindings {
    scorecards: any;
  }

  class Controller implements IBindings {
    public scorecards: any;
 
    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.scorecards) {
//        console.log(this.scorecards);
      }
    }

    public showScoreCard(scorecard) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "student/scorecarddialog",
        controller: DialogController,
        controllerAs: "dvm",
        bindToController: true,
        locals: {
          scorecard: scorecard
        }

      }
      this.mdDialog.show(options);
    }

 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        scorecards: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "student/scorecardlist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "scorecard"];
    constructor(mdDialog: ng.material.IDialogService) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentStudentScoreCardList", new Component());
}
