﻿namespace Pineapples.Exams {
  let modes = [
    {
      key: "Results",
      gridOptions: {
        columnDefs: [
          {
            field: 'exCandidates',
            name: 'Candidates',
            displayName: 'Candidates',
            cellClass: 'gdAlignRight',
          },
          {
            field: 'exMarks',
            name: 'Marks',
            displayName: 'Marks',
            cellClass: 'gdAlignRight',
          },          
        ]
      }
    },    
  ];

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['ExamFilter', pushModes]);
}
