﻿namespace Pineapples.Dashboards {

	export class SchoolDashboard extends Dashboard {

		public xFilter: Sw.xFilter.XFilter;

		// bindings
		public rawEnrolment: any;           // bound to name 'enrolment'
		public districtEnrolment: any;
		public school: Pineapples.Schools.School;                 //
		public schoolTeacherCount: any;
		public examsSchoolResults: any;
		public schoolFlow: any;

		public enrolment: any;

		private colors: Array<string> = ['#FF6666', '#FFB266', '#FFFF66', '#B2FF66', '#66FF66', '#66FFB2', '#66FFFF', '#66B2FF', '#6666FF', '#B266FF', '#FF66FF', '#FF66B2', 'C00000', '#C06600', 'CCCC00', '66DD00'];

		public schoolNo() {
			return this.school._id();
		}


		public dataForFlowBarChart;

		public dataForBarChart;

		public dataForLineChart;

		public lastSurveyYear = () => this.school.LatestSurvey.svyYear;
		 
		public surveyYearArray = (numYears: number) => _.range(this.lastSurveyYear() - numYears + 1, this.lastSurveyYear() + 1);
		// flow data is always one year behind the current survey

		public lastFlowYear = () => this.lastSurveyYear() - 1;
		public flowYearArray = (numYears: number) => _.range(this.lastFlowYear() - numYears + 1, this.lastFlowYear() + 1);
		public lastExamYear = _.memoize( () => _(this.school.Exams)
			.map(({ ExamYear }) => ExamYear)
			.max()
			);


		public gradesForSchool;
		public dataForSchoolFlowLineChart;



		private national = (year) => x => x.filter(_.matchesProperty('surveyYear', year))
		private district = (district, year) => x => x.filter(_.matchesProperty('surveyYear', year))
			.filter(_.matchesProperty('District', district));

		private classLevels;

		public dataForFemalePercentBarChart; // to be initialised in $onChanges

		public latestEnrolPieChartData = _.memoize(
			() => [{ enrol: _(this.school.Surveys).last()['ssEnrolF'] }, { enrol: _(this.school.Surveys).last()['ssEnrolM'] }]
		);

		public latestEnrolPieChartMetadata = _.memoize(() => ['Female', 'Male']);
		public latestEnrolPieChartColors = _.memoize(() => ["#00b161", "#0964a5"]);

		public dataExamResultsByBenchmarkForYear = _.memoize((year, exam) => {

			const tPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - T',
				percent: Math.round((x[`${level}F`] + x[`${level}M`]) * sign * 1000 / (x['CandidatesF'] + x['CandidatesM']) / 10)
			})

			const mPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})

			const fPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})

			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			const data = this.examsSchoolResults
				.filter(x => x.ExamYear === year)
				.filter(x => x.SchoolNo === this.school._id())
				.filter(x => x.Exam === exam);

			const groupedData = _.groupBy(data, ({ ExamStandard }) => ExamStandard)

			return _.mapValues(groupedData, data =>
				[
					{
						data: _.flatten(data.map(genderPercent('ApproachingCompetence', -1))),
						color: '#FFC000',
						meta: 'Approaching competence'
					},
					{
						data: _.flatten(data.map(genderPercent('WellBelowCompetent', -1))),
						color: '#FF0000',
						meta: 'Well below competent'
					},
					{
						data: _.flatten(data.map(genderPercent('MinimallyCompetent', 1))),
						color: '#92D050',
						meta: 'Minimally competent'
					},
					{
						data: _.flatten(data.map(genderPercent('Competent', 1))),
						color: '#00B050',
						meta: 'Competent'
					}
				]
			)
		},
			(year, exam) => [year, exam].toString());



		public dataForStackedGenderHistory;

		private _classSorter = x => this.lookups.byCode("levels", x.ClassLevel, "YoEd");
		
		public enrol: any; // = _.memoize((year: number) => _.sortBy(this.enrolment[year]), this._classSorter);

		public enrolmentYears;

		public exams;

		public standardsForExam;

		// retrieve the grade from the year of ed
		// this may be dependent upon school type
		public grade = yearOfEd => {
			let types = (<any>this.lookups.cache["schoolTypeLevels"])
				.filter(({ YoEd, ST }) => (YoEd === yearOfEd && ST === this.school["schType"]));
			if (types.length) {
				return types[0]["L"];					// return the level code
			}
			// this school has a year of ed not compatible with its type
			// TO DO - full treatment should check default EducationPath to get this grade
			console.log("Using default grade for YoEd = ", yearOfEd);
			return (<any>this.lookups.cache["levels"])
				.filter(({ YoEd }) => (YoEd === yearOfEd))[0]["C"];
		
		};

		public schoolFlowData;

		public dataForFemalePercentHistoryLineChart;

		public schoolFlowDataValue = (schNo, year, grade, indicator) => {
			const data = this.schoolFlowData(schNo)[`${year}:${grade}`];
			return data && data[indicator] && `${(Math.round(data[indicator] * 10) / 10.0)}`;
		}

		public $onChanges(changes) {
			// novalues are pushed until all values are pushed
			if (this.school) {
				// enrolment has been set or changed
				this._collectData(this.schoolNo());
			}
		}

		private _collectData(schNo) {
			const xFilter = new Sw.xFilter.XFilter();
			// this code is migrated from resolves in dashboard.routes, (line 23 ff) which was removed in commmit
			// fdd4739 (fdd4739fe58e2ac14f3a2e99a7c1b4f390d723d1) 22-03-2019

			// api/schools is not needed now - passed in from School Component
			// although enrolments are available in the school object, 
			// leave this if we want to get district or national averages

			let pEnrol = this.reflator.get(`api/warehouse/enrolbyschool/${schNo}`);
			let pDistrict = this.reflator.get(`api/warehouse/districtEnrol`);
			let pTeacher = this.reflator.get(`api/warehouse/schoolteachercount/${schNo}`);
			let pExam = this.reflator.get(`api/warehouse/examsschoolresults/${schNo}`);
			let pFlow = this.reflator.get(`api/warehouse/schoolflowrates/${schNo}`);

			// use $q.all so we only proceeed to construct the various data shapes 
			// when all the data is available
			this.$q.all({ pEnrol, pDistrict, pTeacher, pExam, pFlow })
				.then(response => {
					this.rawEnrolment = response.pEnrol.data;
					this.districtEnrolment = response.pDistrict.data;
					this.schoolTeacherCount = response.pTeacher.data;
					this.examsSchoolResults = response.pExam.data;
					this.schoolFlow = response.pFlow.data;
					this.rebuild();
				});
		}
		// sequencing issues mean that functions bound to child components are called before the enrolments are populated
		// so these initialisations have to be moved so that they are called when enrolment changes
		private rebuild() {
			this.enrolment = _(this.rawEnrolment).groupBy((x: any) => [x.surveyYear, x.ClassLevel])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					surveyYear: vv.surveyYear,
					ClassLevel: vv.ClassLevel,
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM
				})))
				.groupBy(x => x.surveyYear).value()
			this.enrol = _.memoize((year: number) => _.sortBy(this.enrolment[year], this._classSorter)); // fixes issue 556
			this.enrolmentYears = () => _.keys(this.enrolment);

			const schoolDistrict = this.lookups.byCode("islands", this.school["iCode"], "D"); // extract the district value from the lookup table

			this.dataForBarChart = _.memoize((year) => [         // Will need to clear memoize cache for all memoized funcs in $onChange if .enrolment is updated
				this.enrol(year).map(({ ClassLevel, EnrolF: Enrol }) => ({ ClassLevel, Enrol })),
				this.enrol(year).map(({ ClassLevel, EnrolM: Enrol }) => ({ ClassLevel, Enrol }))
			]);


			this.dataForLineChart = _.memoize(() => {
				const colors = ["#34b24c", "#ffa500", "#551a8b"];
				const meta = ['Female', 'Male', 'Total'];

				// _.assign is equivalent to angular.extend
				// _.merge is a deep copy , like angular.merge
				const addObjs = (a, b) => (<any>_.assign)(

					// spread operator here is typescript shorthand to use the array as the arguments to the function
					// the resulting javascript is _.assign.apply(_, Object.keys(b).map(etc ect))
					...Object.keys(b).map(
						// get an array of all keys in b, 
						// then map this array of strings to an array of objects.
						// For each key in b, the resulting object has that same key, and its value is the sum of the [key] value in b and a
						// we end up with an array of object each with one key, and and keys are distinct:
						// [{a:1}, {b:3}, {c:5}]
						// _.assign pulls them back to to a single object
						// {a:1, b:3, c:5}
						key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
					)
				);

				// lodash 'implicit object wrapper' is used to create a 'fluent' syntax
				// allowing data transformation using a pipeline of lodash operations
				const totals = _(this.enrolment)
					//https://lodash.com/docs/4.17.11#mapValues
					// this.enrolment is an object with a key for each year. The value of each key is a collection of the rows for that year
					// the elements of this colection are passed to mapValues
					.mapValues(y => _(y)
						.map(({ EnrolF, EnrolM }) => ({ Female: EnrolF, Male: EnrolM, Total: EnrolF + EnrolM }))
						.reduce(addObjs, {}))
					.value()

				return _(meta)
					.map(g => _(totals).toPairs()
						.map(x => ({ year: parseInt(x[0]), enrol: x[1][g] }))
						.value())
					.map((dataset, i) => ({ dataset, meta: meta[i], color: colors[i] }))
					.value();
			}, () => this.enrolment);

			this.dataForFemalePercentBarChart = _.memoize((year) => [
				// School Dataset
				_(this.enrol(year))
					.map(({ ClassLevel, EnrolF, EnrolM }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / (EnrolF + EnrolM), 1) }))
					// the sort by order of class levels can be retrieved from the yearofeducation
					.sortBy(({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel, "YoEd"))
					.value(),

				// District Dataset
				_(this.district(schoolDistrict, year)(this.districtEnrolment))
					.filter(({ ClassLevel }) => (this.classLevels(year).indexOf(ClassLevel) !== -1))
					.map(({ ClassLevel, EnrolF, EnrolM }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / (EnrolF + EnrolM), 1) }))
					.value(),

				// National Dataset
				_(this.national(year)(this.districtEnrolment))
					.filter(({ ClassLevel }) => (this.classLevels(year).indexOf(ClassLevel) !== -1))
					.groupBy((x: any) => x.ClassLevel)
					.mapValues((x: any) =>
						x.reduce((acc, val) =>
							_.assign({}, { EnrolF: val.EnrolF + acc.EnrolF, EnrolM: val.EnrolM + acc.EnrolM }), { EnrolF: 0, EnrolM: 0 }))
					.mapValues((x: any) => _.round(100 * x.EnrolF / (x.EnrolF + x.EnrolM), 1))
					.toPairs()
					.map(x => ({ ClassLevel: x[0], Ratio: x[1] }))
					.value()
			]);

			this.schoolFlowData = _.memoize(
				(schNo) => _(this.schoolFlow)
					.filter(_.matchesProperty('schNo', schNo))
					.map(x => _.assign({}, x, { grade: this.grade(x.YearOfEd) }))
					.groupBy(({ surveyYear, grade }) => `${surveyYear}:${grade}`)
					.mapValues(_.first)
					.value()
			);

			this.exams = _.memoize(
				(year) => _.uniq(
					this.examsSchoolResults
						.filter(({ ExamYear }) => ExamYear === year)
						.filter(({ SchoolNo }) => SchoolNo === this.schoolNo())
						.map(({ Exam }) => Exam)
				));

			this.standardsForExam = _.memoize(
				(exam, year) => _.uniq(
					this.examsSchoolResults
						.filter(({ ExamYear }) => ExamYear === year)
						.filter(({ Exam }) => Exam === exam)
						.filter(({ SchoolNo }) => SchoolNo === this.schoolNo())
						.map(({ ExamStandard }) => ExamStandard)
				),
				// memoize cache logic changed in 4.x - array will not "deep" match - convert to string instead
				(exam, year) => [exam, year].toString());

			// TO DO: get this from metaSchoolTypesLevelMap? for now, adapt the existing logic which filters
			// all possible levels based on what is found in schoolflowdata
			this.gradesForSchool = _.memoize(
				(schNo) => _.map(this.lookups.cache["levels"],"C")
					.filter(x => _(this.schoolFlowData(schNo)).values().map(_.property('grade')).uniq().sort().value().indexOf(x) != -1)
			);

			this.dataForSchoolFlowLineChart = _.memoize(
				(indicator) =>
					_(this.gradesForSchool(this.school._id()))
						.map((grade, i) => (
							{
								meta: grade,
								color: this.colors[i],
								dataset: this.flowYearArray(5).map(year => (
									{
										year,
										value: Number(this.schoolFlowDataValue(this.school._id(), year, grade, indicator)) || 0
									}
								))
							}
						)).value()
			);

			this.classLevels = (year) => _(this.enrol(year)).map('ClassLevel').value();

			this.dataForFemalePercentHistoryLineChart = _.memoize(() => {
				const schoolDataset = _(this.enrolment)
					.map(x => _(x).reduce((acc, { surveyYear, EnrolF, EnrolM }) => ({ year: surveyYear, F: acc.F + EnrolF, M: acc.M + EnrolM, }), { year: 0, F: 0, M: 0 }))
					.map(({ year, F, M }) => ({ year: year, Ratio: _.round(100 * F / (F + M), 1) }))
					.value();

				const years = schoolDataset.map(x => x.year);
				const keepYear = ({ surveyYear }) => _.includes(years, surveyYear);
				return [
					{
						meta: 'School',
						color: "#34b24c",
						dataset: schoolDataset
					},
					{
						meta: 'District',
						color: "#ffa500",
						dataset:
							_(this.districtEnrolment)
								.filter(_.matchesProperty('District', schoolDistrict))
								.filter(keepYear)
								.groupBy(_.property('surveyYear'))
								.map(x => _(x).reduce((acc, { surveyYear, EnrolF, EnrolM }) => ({ year: surveyYear, F: acc.F + EnrolF, M: acc.M + EnrolM, }), { year: 0, F: 0, M: 0 }))
								.map(({ year, F, M }) => ({ year: year, Ratio: _.round(100 * F / (F + M), 1) }))
								.value()
					},
					{
						meta: 'National',
						color: "#551a8b",
						dataset:
							_(this.districtEnrolment)
								.filter(keepYear)
								.groupBy(_.property('surveyYear'))
								.map(x => _(x).reduce((acc, { surveyYear, EnrolF, EnrolM }) => ({ year: surveyYear, F: acc.F + EnrolF, M: acc.M + EnrolM, }), { year: 0, F: 0, M: 0 }))
								.map(({ year, F, M }) => ({ year: year, Ratio: _.round(100 * F / (F + M), 1) }))
								.value()
					}
				];
			});

			this.dataForStackedGenderHistory = _.memoize(() => {
				const colors = ["#34b24c", "#ffa500"];
				const meta = ['Female', 'Male'];

				const addObjs = (a, b) => (<any>_.assign)(
					...Object.keys(b).map(
						key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
					)
				);

				const totals = _(this.enrolment)
					.mapValues(y => _(y)
						.map(({ EnrolF, EnrolM }) => ({ Female: EnrolF, Male: EnrolM }))
						.reduce(addObjs, {}))
					.value()

				return _(meta)
					.map(g => _(totals).toPairs()
						.map(x => ({ year: parseInt(x[0]), enrol: x[1][g] }))
						.value())
					.map((dataset, i) => ({ dataset, meta: meta[i], color: colors[i] }))
					.value();
			}, () => this.enrolment);

			this.dataForFlowBarChart = _.memoize(
				(schNo, year, indicator) => [
					_.map(
						this.gradesForSchool(schNo),
						(grade) => ({
							ClassLevel: grade,
							Value: this.schoolFlowDataValue(schNo, year, grade, indicator)
						}
						)),
				],
				(schNo, year, indicator) => `${schNo} - ${year} - ${indicator}`
			);
		}
	}


	class Component implements ng.IComponentOptions {
		public bindings: any = {
			school: "<",
			////schoolInfo: "<",
			////rawEnrolment: "<enrolment",
			////districtEnrolment: "<",
			////schoolTeacherCount: "<",
			////examsSchoolResults: "<",
			////schoolFlow: "<",
		};
		public controller: any = SchoolDashboard;
		public controllerAs: string = "vm";
		public templateUrl: string = `dashboard/dashboard/SchoolDashboard`;
	}

	angular
		.module("pineapples")
		.component("schoolDashboard", new Component());


}
