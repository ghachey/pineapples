﻿namespace Pineappples.Dashboards {
    
  class Controller {
    static $inject = ['Lookups'];
    constructor(public lookups) { }

    rowLabels = () => this.lookups.cache[this.rowLabelsLookup].map(x => x.N);
    rowCodes = () => this.lookups.cache[this.rowLabelsLookup].map(x => x.C);

    // Bindings
    rowLabelsLookup: string;
    group: any;
    selectedKey: any;
    onClick: any;
    colHeadings: any;

    //$onChanges(changes) {
    //  console.log('SimpleTableBase.$onChanges', changes);
    //}

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        rowLabelsLookup: "<",
        group: "<",
        selectedKey: "<",
        onClick: "<",
        colHeadings: "<",
      };

      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/SimpleTableBase`;
    }
  }

  angular
    .module("pineapples")
    .component("simpleTableBase", new Component());
}