﻿/* <PlottableBarChart />
 * Bar Chart Component
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * headingOptions: js object, keys of which are displayed in dropdown in component header.  Values corrosponding to selected option are available to the component to customise the view.
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasets: data for chart - js Array of Arrays of Objects, one Array per dataset, one object per datapoint
        i.e. [[ {
                  "ClassLevel": "G5",
                  "Value": "110"
                },
                {
                  "ClassLevel": "G6",
                  "Value": "440"
                },
                {
                  "ClassLevel": "G7",
                  "Value": "1200"
                } ]]

 * datasetsX: key for objects in dataset attribute for x axis.  i.e. "ClassLevel"
 * datasetsY: key for objects in dataset attribute for y axis.  i.e. "Value"
 */

namespace Pineappples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;
    public headingOptions: any;
    public chartTitle: string;
    public datasets: any;
    public datasetsX: any;
    public datasetsY: any;

    constructor() {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    public componentId: string;
    public isSelected = () => this.componentId == this.selectedChild;
    public anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      headingOptions: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      chartTitle: '@',
      datasets: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
        <dashboard-child class="dashboard-wrapper"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
          <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption">
              <md-option ng-value="opt" ng-repeat="opt in vm.viewOptions()">{{ opt }}</md-option>
            </md-select>
          </heading-options>  

          <child-body>
            <plottable-bar-chart-base
                chart-title="{{vm.chartTitle}}"
                is-selected="vm.isSelected()"
                datasets="vm.datasets"
                datasets-x="{{vm.datasetsX}}"
                datasets-y="{{vm.datasetsY}}" />

          </child-body>
        </dashboard-child>
        `;
  }

  angular
    .module("pineapples")
    .component("plottableBarChart", new Component());
}


