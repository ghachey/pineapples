﻿namespace Pineapples.Dashboard {


  class Controller {
    public options: any

    static $inject = ["Lookups"]
    constructor(public lookups: Sw.Lookups.LookupService) { }

    public resetAll = () => this.options.resetAll();

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor(templateUrl) {
      this.bindings = {
        options: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = templateUrl;
    }
  }

  angular
    .module("pineapples")
    .component("coreOptionsEditor", new Component("dashboard/options/core"))
    .component("indicatorOptionsEditor", new Component("dashboard/options/indicator"));
}