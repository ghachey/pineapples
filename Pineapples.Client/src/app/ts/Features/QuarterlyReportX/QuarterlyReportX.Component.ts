﻿namespace Pineapples.QuarterlyReportsX {

  interface IBindings {
    model: QuarterlyReportX;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: QuarterlyReportX;

    static $inject = ["ApiUi", "quarterlyReportsXAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentQuarterlyReportX", new Sw.Component.ItemComponentOptions("quarterlyreportx", Controller));
}