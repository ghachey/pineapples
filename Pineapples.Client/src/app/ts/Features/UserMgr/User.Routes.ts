﻿// User Management Routes
namespace Sw.Auth {

  let routes = function ($stateProvider) {
    let state: ng.ui.IState = {
      abstract: true
    }
    let statename = "site.users";
    $stateProvider.state(statename, state);

    state = {
      url: "^/users/list",
      views: {
        "@": "userList"
      },
      resolve: {
        users: ['usersAPI',  (api) => {
          return api.list();
        }]
      }
    };
    statename = "site.users.list";
    $stateProvider.state(statename, state);

    state = {
      url: "^/users/{id}",
      params: { id: null },
      views: {
        "actionpane@site.users.list": "componentUser"
      },
      resolve: {
        model: ['usersAPI', "$stateParams", (api, stateParams) => {
          return api.read(stateParams.id);
        }],
        roles: ['umgrAPI', (api) => {
          return api.roles();
        }],
        menuKeys: ['umgrAPI', (api) => {
          return api.menuKeys();
        }]
      }
    };
    statename = "site.users.list.item";
    $stateProvider.state(statename, state);
  }

  angular
    .module("sw.common")
    .config(['$stateProvider', routes])
}
