namespace Pineapples.Books {
  let modes = [
    {
      key: "ERU",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'qty',
            name: 'eruqoh',
            displayName: 'ERU',
            cellClass: 'gdAlignLeft'
          }
        ]
      }
    },
    {
      key: "Total Holding",
      columnSet: 1,
      gridOptions: {
        columnDefs: [
          {
            field: 'QOH',
            name: 'qoh',
            displayName: 'QOH',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'QOHReplace',
            name: 'qohreplace',
            displayName: 'Replace',
            cellClass: 'gdAlignRight'
          }
        ]
      } // gridoptions
    }
  ]; // modes

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['BookFilter', pushModes]);
}
