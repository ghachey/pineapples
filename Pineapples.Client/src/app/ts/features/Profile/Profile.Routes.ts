﻿// User Routes
namespace Pineappples.Profile {

  let routes = ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider
      .state("site.profile", {
        url: "^/profile",
        views: {
          "@": {
            templateUrl: "profile/permissions",
            controller: function () {
              this.title = 'Profile Permissions'
            },
            controllerAs: 'vm'
          }
        },
      });
    // Following could be uncomment to enable a more agile reload workflow
    //$stateProvider
    //  .state("site.profile.reload", {
    //    url: '^/profile/reload',
    //    onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
    //      $templateCache.remove("profile/permissions");
    //      $state.go("site.surveys.item");
    //    }]
    //  });
  };

  angular
    .module("pineapples")
    .config(["$stateProvider", routes]);
}