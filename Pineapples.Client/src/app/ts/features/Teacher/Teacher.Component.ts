﻿namespace Pineapples.Teachers {

  interface IBindings {
    model: Teacher;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Teacher;

    static $inject = ["ApiUi", "teachersAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentTeacher", new Sw.Component.ItemComponentOptions("teacher", Controller));
}