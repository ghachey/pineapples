namespace Pineappples.QuarterlyReports {

  let routes = function ($stateProvider) {
    var featurename = 'QuarterlyReports';
    var filtername = 'QuarterlyReportFilter';
    var templatepath = "quarterlyreport";
    var tableOptions = "quarterlyreportFieldOptions";
    var url = "quarterlyreports";
    var usersettings = null;
    //var mapview = 'SchoolMapView';

    // root state for 'quarterlyreport' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath);

    // default 'api' in this feature is quarterlyReportsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "quarterlyReportsAPI";

    let statename = "site.quarterlyreports";
    $stateProvider.state("site.quarterlyreports", state);

    // takes an editable list state
    state = Sw.Utils.RouteHelper.editableListState("QuarterlyReport");
    statename = "site.quarterlyreports.list";
    state.url = "^/quarterlyreports/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/quarterlyreports/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("quarterlyreport/item");
        $templateCache.remove("quarterlyreport/searcher");
        $state.go("site.quarterlyreports.list");
      }]
    };
    statename = "site.quarterlyreports.reload";
    $stateProvider.state(statename, state);

    state = {
      url: "^/quarterlyreports/dashboard",
      data: {
        permissions: {
          only: 'SchoolReadX'
        }
      },
      views: {
        "@": {
          component: "quarterlyReportsDashboardComponent"
        }

      },
    };
    $stateProvider.state("site.quarterlyreports.dashboard", state);

    // chart table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    // Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

    // new - state with a custom url route
    state = {
      url: "^/quarterlyreports/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.quarterlyreports.list": {
          component: "componentQuarterlyReport"
        }

      },
      resolve: {
        model: ['quarterlyReportsAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.quarterlyreports.list.new", state);

    state = {
      url: "^/quarterlyreports/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Quarterly_Reports",  // actually Quarterly Reports folder in Jasper but URI is underscore
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.quarterlyreports.reports", state);

    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/quarterlyreports/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.quarterlyreports.list": {
          component: "componentQuarterlyReport"
        }
      },
      resolve: {
        model: ['quarterlyReportsAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.quarterlyreports.list.item", state);
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}
