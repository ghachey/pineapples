﻿module Pineapples {
  // shows histogram of exam results for the wchool
  // its authority, district and nation

  // define the IsolateScope of the directive
  // this will include those members bound to the directive attributes
  // and any other values we may want to place on scope
  // we can use the generic name IIsolateScope becuase it is not exported outside this closure
  interface IIsolateScope extends ng.IScope {
    height: number;
    width: number;
    data: any[][];
    // keyField - the property in the data that represents the key
    // hiliteValue - key value for the highlighted point
    // yField - property for the y value
    // yLabel - label for the y values
    // hiliteClass - class for the hilite bar
  }

  // declaration of the class that implements the directive
  // does not need to be exported
  class SchoolExamChart implements ng.IDirective {
    // properties of the directive are public properties of this class

    // scope defines the bindings from the directive attributes to the isolate scope
    // it is not the scope itself
    public scope = {
      height: '@',
      width: '@',
      data: "=dataset"
    };
    public restrict = "EA";
    public template = "<svg class=\"schoolExamChart\" height=\"{{height}}\" width=\"{{width}}\"></svg>";

    public link: ng.IDirectiveLinkFn;
    private _chart: Plottable.Components.Table;

    // the arguments to the constructor are the injected dependencies
    constructor() {
      // define the directive link function in the constructor
      // put it on the prototype
      SchoolExamChart.prototype.link = (scope: IIsolateScope, element, attrs, ctrlr, transclude) => {

        // link function sets up any required watches on the scope
        // this allows the chart to respond to changes in the controller
        scope.$watch("data", (newValue, oldValue) => {
          if (newValue) {
            this._drawChart(scope, element);
          }
        });
      };
    }

    // function to establish the chart objects and render
    private _makeChart = (scope: IIsolateScope, element) => {
      // use plottable to make the chart
      var ticks = function (scale) {
        return [41, 141, 241, 341];
      };

      let yScale = new Plottable.Scales.Linear()
        .domain([1, 391])
        .tickGenerator(ticks);

      var yAxis = new Plottable.Axes.Numeric(yScale, "left")
        .formatter(function (b) { return b.toString() + "-" + (b + 9).toString(); });

      let colors = new Plottable.Scales.Color();

      colors
        .domain(["P10", "P50", "P90"])
        .range(["dimgray", "darkblue", "gray"]);

      let legend = new Plottable.Components.Legend(colors)
        .formatter(function (t) {
          switch (t) {
            case "P50":
              return "median";
            case "P10":
              return "10th percentile";
            case "P90":
              return "90th percentile";
            default:
              return "";
          };
        });

      let titles = [];

      titles.push(null);
      titles.push(new Plottable.Components.TitleLabel("school"));
      titles.push(new Plottable.Components.TitleLabel("authority"));
      titles.push(new Plottable.Components.TitleLabel("district"));
      titles.push(new Plottable.Components.TitleLabel("all"));
      titles.push(null);

      // the chart
      let charts = [];
      charts.push(yAxis);
      for (var i = 1; i <= 4; i++) {
        let xScale = new Plottable.Scales.Linear();  // xScale is private
        var ds = new Plottable.Dataset(scope.data[i]);
        var cht = new Plottable.Plots.Bar('horizontal')
          .addDataset(ds)
          .y((d, i) => d.bandMin, yScale)
          .x((d) => d.Gender === "M" ? d.C * -1 : d.C, xScale)
          // .classed(scope.hiliteClass, function(d) { return (d[scope.keyField] == scope.hiliteValue);})
          .attr("index", (d, i) => i)
          .attr("fill", function (d, i) {
            if (d.PERCT === "P10") {
              return "dimgray";
            }
            if (d.PERCT === "P50") {
              return "navyblue";
            }
            if (d.PERCT === "P90") {
              return "gray";
            }
            return "skyblue";
          })
          .attr("v", function (d) { return d.band; })
          ;
        // make gridlines
        var gl = new Plottable.Components.Gridlines(null, yScale);

        // group the gridlines and the chart
        var grp = new Plottable.Components.Group([gl, cht]);
        charts.push(grp);
      }///----- make a table component for the chart
      charts.push(legend);
      this._chart = new Plottable.Components.Table([
        charts
        , titles
      ]);

      //// Create
      // this is a bit of translation to turn a jQuery element to a d3 element
      let svg = $(element).find('svg.schoolExamChart');
      let d3svg = d3.selectAll(svg.toArray());
      this._chart.renderTo(d3svg);
    };

    private _drawChart = (scope, element) => {
      if (!this._chart) {
        this._makeChart(scope, element);
      }
    };

    public static factory() {
      let directive = () => {
        return new SchoolExamChart();
      };
      return directive;
    }
  }

  angular
    .module("pineapples")
    .directive("schoolExamChart", SchoolExamChart.factory());
}
