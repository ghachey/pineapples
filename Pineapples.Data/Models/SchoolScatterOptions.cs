﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class SchoolScatterOptions
    {
        public int BaseYear {get;set;}

        public string XSeries { get; set; }
        public int XSeriesOffset { get; set; }
        public string xArg1 { get; set; }
        public string xArg2 { get; set; }

        public string YSeries { get; set; }
        public int YSeriesOffset { get; set; }
        public string yArg1 { get; set; }
        public string yArg2 { get; set; }

        public string kml { get; set; }

        //public IFilterParam SchoolType { get { return getParam("SchoolType", typeof(string)); } }
        //public IFilterParam Authority { get { return getParam("Authority", typeof(string)); } }
        //public IFilterParam District { get { return getParam("District", typeof(string)); } }
        //public IFilterParam SchoolNo { get { return getParam("SchoolNo", typeof(string)); } }
        //public IFilterParam kml { get { return getParam("kml", typeof(string)); } }

    }
}
