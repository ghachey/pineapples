﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
    [Table("TeacherTraining")]
    [Description("Detailed records of teacher qualifications and training.  TeacherIdentity tID is the foreign key.")]
    public class TeacherQualification:ChangeTracked
    {
        [Key]
        [Column("trID", TypeName = "int")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "ID is required")]
        [Display(Name = "ID")]
        public int trID { get; set; }
        [Column("tID", TypeName = "int")]
        [Display(Name = "Teacher ID")]
        public int? tID { get; set; }
        [Column("trYear", TypeName = "int")]
        [Display(Name = "Year")]
        public int? trYear { get; set; }
        [Column("trPre", TypeName = "nvarchar")]
        [MaxLength(4)]
        [StringLength(4)]
        [Display(Name = "Pre")]
        public string trPre { get; set; }
        [Column("trComplete", TypeName = "bit")]
        [Display(Name = "Complete")]
        public bool? trComplete { get; set; }
        [Column("trQual", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Qual")]
        public string trQual { get; set; }
        [Column("trInstitution", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Institution")]
        public string trInstitution { get; set; }
        [Column("trDuration", TypeName = "int")]
        [Display(Name = "Duration")]
        public int? trDuration { get; set; }
        [Column("trDurationUnit", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Duration Unit")]
        public string trDurationUnit { get; set; }
        [Column("trMajor", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Major")]
        public string trMajor { get; set; }
        [Column("trMinor", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Minor")]
        public string trMinor { get; set; }
        [Column("trProgress", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Progress")]
        public string trProgress { get; set; }
        [Column("trProgressPerc", TypeName = "int")]
        [Display(Name = "Progress Perc")]
        public int? trProgressPerc { get; set; }
        [Column("trComment", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Comment")]
        public string trComment { get; set; }
        [Column("trEffectiveDate", TypeName = "datetime")]
        [Display(Name = "Effective Date")]
        public DateTime? trEffectiveDate { get; set; }
        [Column("trExpirationDate", TypeName = "datetime")]
        [Display(Name = "Expiration Date")]
        public DateTime? trExpirationDate { get; set; }
    }

}
