﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;

namespace Pineapples.Data
{
    public interface IDSWarehouse
    {
        IDataResult TableEnrol();
		// flow rates
		/// <summary>
		/// all flow rate data for a specified school
		/// </summary>
		/// <param name="schoolNo"></param>
		/// <returns></returns>
        IDataResult SchoolFlowRates(string schoolNo);
		// flow rates
		/// <summary>
		/// all flow rate data for all schools
		/// </summary>
		/// <param name="schoolNo"></param>
		/// <returns></returns>
		IDataResult AllSchoolFlowRates();
		IDataResult SchoolTeacherCount(string schoolNo);
		IDataResult AllSchoolTeacherCount();
		IDataResult SchoolTeacherPupilRatio();

        IDataResult TeacherCount();
        IDataResult TeacherQual();
        IDataResult TeacherPupilRatio();
        IDataResult ClassLevelER();
        IDataResult EdLevelER();
        IDataResult FlowRates();
        IDataResult EdLevelAge();
        
        IDataResult TableDistrictEnrol();
        IDataResult TableEnrolBySchool(string schoolNo);

        // exam methods
        /// <summary>
        /// return all data from Exam School Results - for a single school
        /// </summary>
        /// <returns></returns>
        IDataResult ExamSchoolResults(string schoolNo);

        /// <summary>
        /// return all data from Exam School Results - for every school
        /// </summary>
        /// <returns></returns>
        IDataResult ExamAllSchoolResults();

        /// <summary>
        /// return all data from Exam School Results - consolidated by district
        /// </summary>
        /// <returns></returns>
        IDataResult ExamDistrictResults();
    }
}
