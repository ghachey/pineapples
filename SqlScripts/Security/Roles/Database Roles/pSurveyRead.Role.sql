CREATE ROLE [pSurveyRead]
GO
EXEC sys.sp_addextendedproperty @name=N'Comment', @value=N'Permission to read a survey' , @level0type=N'USER',@level0name=N'pSurveyRead'
GO
EXEC sys.sp_addextendedproperty @name=N'PineapplesUser', @value=N'CoreRole' , @level0type=N'USER',@level0name=N'pSurveyRead'
GO

