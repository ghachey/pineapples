SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpRoomTypes](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeGroup] [nvarchar](1) NULL,
	[codeSort] [int] NULL,
	[rmresCat] [nvarchar](50) NULL,
	[rfcnCode] [nvarchar](10) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpRoomTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpRoomTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpRoomTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpRoomTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpRoomTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpRoomTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpRoomTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpRoomTypes] ADD  CONSTRAINT [DF__lkpRoomTy__codeS__595B4002]  DEFAULT ((0)) FOR [codeSort]
GO
ALTER TABLE [dbo].[lkpRoomTypes]  WITH CHECK ADD  CONSTRAINT [lkpRoomTypes_FK00] FOREIGN KEY([rfcnCode])
REFERENCES [dbo].[lkpRoomFunction] ([rfcnCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lkpRoomTypes] CHECK CONSTRAINT [lkpRoomTypes_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of rooms. Some are system defined - e.g. CLASS.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpRoomTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpRoomTypes'
GO

