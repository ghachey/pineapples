SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpBanks](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpBanks1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpBanks] TO [pFinanceAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpBanks] TO [pFinanceAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpBanks] TO [pFinanceAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpBanks] TO [pFinanceAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpBanks] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpBanks] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lookup of banks (not branches) in the country. Specific branches may appear in services, but this older data is not linked.
Specific branches also in SchoolBanks, which is linked.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBanks'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBanks'
GO

