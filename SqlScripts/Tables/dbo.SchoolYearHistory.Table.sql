SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolYearHistory](
	[syID] [int] IDENTITY(1,1) NOT NULL,
	[syYear] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[saSent] [datetime] NULL,
	[saCollected] [datetime] NULL,
	[saReceived] [datetime] NULL,
	[saCompletedby] [nvarchar](50) NULL,
	[saEntered] [datetime] NULL,
	[saEnteredBy] [nvarchar](30) NULL,
	[saAudit] [bit] NOT NULL,
	[saAuditDate] [datetime] NULL,
	[saAuditBy] [nvarchar](50) NULL,
	[saAuditResult] [nvarchar](1) NULL,
	[saAuditNote] [ntext] NULL,
	[saComment] [ntext] NULL,
	[saReceivedTarget] [datetime] NULL,
	[saReceivedD] [datetime] NULL,
	[saReceivedDTarget] [datetime] NULL,
	[saFileLocn] [nvarchar](50) NULL,
	[syDormant] [bit] NOT NULL,
	[syStatus] [nvarchar](10) NULL,
	[systCode] [nvarchar](10) NULL,
	[syAuth] [nvarchar](10) NULL,
	[syParent] [nvarchar](50) NULL,
	[syEstablishmentPoint]  AS (isnull([syParent],[schNo])) PERSISTED NOT NULL,
	[syFlag] [nvarchar](10) NULL,
	[syNote] [nvarchar](400) NULL,
	[syLocked] [int] NULL,
	[syBestssID] [int] NULL,
 CONSTRAINT [SchoolYearHistory_PK] PRIMARY KEY NONCLUSTERED 
(
	[syID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[SchoolYearHistory] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolYearHistory] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolYearHistory] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[SchoolYearHistory] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolYearHistory] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolYearHistory] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolYearHistory] TO [pSurveyWrite] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SchoolYearHistory_SchNo_Year] ON [dbo].[SchoolYearHistory]
(
	[schNo] ASC,
	[syYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolYearHistory_Year] ON [dbo].[SchoolYearHistory]
(
	[syYear] ASC
)
INCLUDE ( 	[schNo],
	[syParent]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolYearHistory] ADD  CONSTRAINT [DF_SchoolYearHistory_saAudit]  DEFAULT ((0)) FOR [saAudit]
GO
ALTER TABLE [dbo].[SchoolYearHistory] ADD  CONSTRAINT [DF_SchoolYearHistory_syDormant]  DEFAULT ((0)) FOR [syDormant]
GO
ALTER TABLE [dbo].[SchoolYearHistory]  WITH CHECK ADD  CONSTRAINT [FK_SchoolYearHistory_Authorities] FOREIGN KEY([syAuth])
REFERENCES [dbo].[Authorities] ([authCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolYearHistory] CHECK CONSTRAINT [FK_SchoolYearHistory_Authorities]
GO
ALTER TABLE [dbo].[SchoolYearHistory]  WITH CHECK ADD  CONSTRAINT [FK_SchoolYearHistory_Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolYearHistory] CHECK CONSTRAINT [FK_SchoolYearHistory_Schools]
GO
ALTER TABLE [dbo].[SchoolYearHistory]  WITH CHECK ADD  CONSTRAINT [FK_SchoolYearHistory_SchoolsParent] FOREIGN KEY([syParent])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[SchoolYearHistory] CHECK CONSTRAINT [FK_SchoolYearHistory_SchoolsParent]
GO
ALTER TABLE [dbo].[SchoolYearHistory]  WITH CHECK ADD  CONSTRAINT [FK_SchoolYearHistory_SchoolTypes] FOREIGN KEY([systCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolYearHistory] CHECK CONSTRAINT [FK_SchoolYearHistory_SchoolTypes]
GO

