SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[bestSurvey](
	[SurveyYear] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[surveyDimensionID] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Gets the best survey available for each school in each survey year, identified by its ssID. 
This is joined back to dimensionSchoolSurvey to get any survey parameters that you may want to aggregate on inside the warehouse system.

Note that the most standard aggregations based on school properties - district, school type, authority - are generally explicitlyt handled by custom warehouse tables or views.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'bestSurvey'
GO

