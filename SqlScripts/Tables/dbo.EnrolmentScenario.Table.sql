SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnrolmentScenario](
	[escnID] [int] IDENTITY(1,1) NOT NULL,
	[escnName] [nvarchar](50) NULL,
	[escnDescription] [ntext] NULL,
	[escnParams] [xml] NULL,
	[escnSplit] [nvarchar](1) NULL,
	[escnGenderSplit] [nvarchar](1) NULL,
	[escnLevelSplit] [nvarchar](1) NULL,
	[escnAgeSplit] [nvarchar](1) NULL,
	[escnSource] [nvarchar](50) NULL,
	[escnYearStart] [int] NULL,
	[escnYearEnd] [int] NULL,
 CONSTRAINT [aaaaaEnrolmentScenario1_PK] PRIMARY KEY NONCLUSTERED 
(
	[escnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[EnrolmentScenario] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[EnrolmentScenario] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EnrolmentScenario] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EnrolmentScenario] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EnrolmentScenario] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An Enrolment Scenario is a projection of future enrolments, over any number of years, that share the same set of assumptions. 
Scenarios may project at different levels of detail - S, District or National. 
They may split or may not split by Level Age and/or Gender.
Each Scenario comprises multiple Projection records in EnrolmentProjection. Each Projection is for a single year, at single reporting point (School Ditrisct or National). 

EnrolmentProjectionData hold the actual enrolment numbers, split according to the settings of the Scenario.


Projections are used by the Establishment Planner.
Projection can be linked to a Inspection/Assessment Set.
Scenarios can be generated using the Excel Projection Modeller and saved into the Pineapples database.
Multiplier Tool and Trend Tool from the Projection Wizard generate scenarios using a muliplier factor, or linear regression of exisitng data.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentScenario'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'You can enquire on and edit projections usingXXXX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentScenario'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Scenario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentScenario'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Projections' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentScenario'
GO

