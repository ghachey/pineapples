SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [audit].[xfdfAudit](
	[sxaID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NOT NULL,
	[svyYear] [int] NULL,
	[schNo] [nvarchar](50) NULL,
	[sxaDate] [datetime] NULL,
	[sxaUser] [nvarchar](50) NULL,
	[sxaNum] [int] NULL,
	[sxaDesc] [nvarchar](100) NULL,
	[sxaData] [nvarchar](50) NULL,
	[sxaSection] [nvarchar](100) NULL,
	[sxaRow] [nvarchar](2) NULL,
	[sxaErrorFlag] [int] NULL,
	[sxaErrorText] [nvarchar](1000) NULL,
 CONSTRAINT [PK_audit.EFormAudit] PRIMARY KEY CLUSTERED 
(
	[sxaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [audit].[xfdfAudit] TO [pSurveyRead] AS [pineapples]
GO
GRANT DELETE ON [audit].[xfdfAudit] TO [pSurveyWrite] AS [pineapples]
GO
GRANT INSERT ON [audit].[xfdfAudit] TO [pSurveyWrite] AS [pineapples]
GO
GRANT SELECT ON [audit].[xfdfAudit] TO [pSurveyWrite] AS [pineapples]
GO
GRANT UPDATE ON [audit].[xfdfAudit] TO [pSurveyWrite] AS [pineapples]
GO
ALTER TABLE [audit].[xfdfAudit] ADD  CONSTRAINT [DF__xfdfAudit__sxaEr__2CD37DA5]  DEFAULT ((0)) FOR [sxaErrorFlag]
GO

