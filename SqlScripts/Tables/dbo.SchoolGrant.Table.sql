SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolGrant](
	[sgID] [int] IDENTITY(1,1) NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[sgYear] [int] NOT NULL,
	[sgPaymentNo] [int] NOT NULL,
	[sgEstimate] [int] NULL,
	[sgRecalculated] [int] NULL,
	[sgStatus] [nvarchar](10) NULL,
 CONSTRAINT [SchoolGrant_PK] PRIMARY KEY NONCLUSTERED 
(
	[sgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [SchoolGrant_IDX_Unique] UNIQUE NONCLUSTERED 
(
	[sgYear] ASC,
	[sgPaymentNo] ASC,
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolGrant] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolGrant] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolGrant] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolGrant] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolGrant] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [schNo] ON [dbo].[SchoolGrant]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolGrant] ADD  CONSTRAINT [DF__SchoolGra__sgEst__62F1F939]  DEFAULT ((0)) FOR [sgEstimate]
GO
ALTER TABLE [dbo].[SchoolGrant] ADD  CONSTRAINT [DF__SchoolGra__sgRec__63E61D72]  DEFAULT ((0)) FOR [sgRecalculated]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A grant paid to a school. Unique for School/Year/Payment No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolGrant'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolGrant'
GO

