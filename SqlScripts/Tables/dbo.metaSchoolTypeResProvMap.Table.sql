SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeResProvMap](
	[mrpID] [int] IDENTITY(1,1) NOT NULL,
	[mrpSurveyName] [nvarchar](50) NULL,
	[stCode] [nvarchar](10) NULL,
	[svyYear] [int] NULL,
	[mrpResource] [nvarchar](20) NULL,
	[mrpSort] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeResProvMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[mrpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeResProvMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeResProvMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeResProvMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeResProvMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeResProvMap] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeResProvMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeResProvMap] ADD  CONSTRAINT [DF__metaSchoo__mrpSo__2BC97F7C]  DEFAULT ((0)) FOR [mrpSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mapping table to specify the resource provision items that appear on a given survey. Sets may be created by survey form, school type , or year. The best match (3,2 or 1 fields) wins - that is the set used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeResProvMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeResProvMap'
GO

