SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolBanks](
	[sbnkID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[sbnkBank] [nvarchar](10) NULL,
	[sbnkBSB] [nvarchar](10) NULL,
	[sbnkAccount] [nvarchar](20) NULL,
 CONSTRAINT [aaaaaSchoolBanks1_PK] PRIMARY KEY NONCLUSTERED 
(
	[sbnkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolBanks] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolBanks] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolBanks] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolBanks] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolBanks] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_SchoolBanks_SSID] ON [dbo].[SchoolBanks]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolBanks]  WITH CHECK ADD  CONSTRAINT [SchoolBanks_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolBanks] CHECK CONSTRAINT [SchoolBanks_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank accounts maintained by a school. Collected on the Vanuatu finance survey. Foreign key is ssID.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolBanks'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolBanks'
GO

