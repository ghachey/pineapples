SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpQualityIssues](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpQualityIssues1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpQualityIssues] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpQualityIssues] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpQualityIssues] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpQualityIssues] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpQualityIssues] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classification of quality issues. Foreign key on SchoolSurveyQuality' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpQualityIssues'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Quality' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpQualityIssues'
GO

