SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [workflow].[ActionTypes](
	[wrkaType] [int] NOT NULL,
	[wrkaName] [nvarchar](50) NULL,
	[wrkaUse] [ntext] NULL,
 CONSTRAINT [metawrkActionTypes_PK] PRIMARY KEY NONCLUSTERED 
(
	[wrkaType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [workflow].[ActionTypes] ADD  CONSTRAINT [DF__ActionTyp__wrkaT__5D01B3B4]  DEFAULT ((0)) FOR [wrkaType]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Attached as workflow_ActionTypes. Categorises workflow actions.' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'ActionTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'ActionTypes'
GO

