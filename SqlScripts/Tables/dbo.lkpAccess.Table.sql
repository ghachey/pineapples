SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpAccess](
	[codeCode] [nvarchar](2) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSeq] [smallint] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpAccess1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpAccess] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpAccess] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpAccess] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpAccess] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpAccess] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpAccess] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpAccess] ADD  CONSTRAINT [DF__lkpAccess__codeS__382F5661]  DEFAULT ((0)) FOR [codeSeq]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lookup for ways of getting to school' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAccess'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAccess'
GO

