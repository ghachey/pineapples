SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 10 2013
-- Description:	data for grounds - create the temp table and passes to the EXEC
-- =============================================
CREATE PROCEDURE [dbo].[PIVRooms_Dorm]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@group nvarchar(40) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT
R.rmType [Code],
lkpRoomTypes.codeDescription AS [Type],
R.rmID,
R.rmYear AS YearBuilt,
R.rmCondition AS Condition,
case when genderCode = '<>' then rmBeds end NumBeds,
case when genderCode = '<>' then rmCupboards end NumCupboards,
case when genderCode = '<>' then rmMatresses end NumMatresses,
case when genderCode = '<>' then 1 end AS Number,
case genderCode when 'M' then R.rmCapM when 'F' then R.rmCapF end Capacity,
case genderCode when 'M' then R.rmToiletsM when 'F' then R.rmToiletsF end NumToilets,
case genderCode when 'M' then R.rmShowersM when 'F' then R.rmShowersF end NumShowers,
genderCode GenderCode,
case when gendercode = '<>' then null else Gender end Gender,
ssID [ssID]
INTO #tmpPivCols
FROM Rooms as R
	INNER JOIN lkpRoomTypes
		ON lkpRoomTypes.codeCode = R.rmType
	CROSS JOIN DimensionGenderU
WHERE
	(R.rmType like 'DORM')


exec dbo.PIVRooms_EXEC
   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear

END
GO

