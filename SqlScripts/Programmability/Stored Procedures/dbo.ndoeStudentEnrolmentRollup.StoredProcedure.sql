SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 4 2019
-- Description:	Generate records in Enrollments and PupilTables
-- from StudentEnrolment_ rows, for a given year
-- splitting this into a separate proc is convenient if maintenance is required
-- to regenerate the totals - and thene the warehouse - with out a reload of the workbook.
-- =============================================
CREATE PROCEDURE [dbo].[ndoeStudentEnrolmentRollup]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin transaction

DELETE FROM Enrollments
WHERE ssID in (Select ssID from StudentEnrolment WHERE stueYear = @Year)

-- enter the enrolments
INSERT INTO Enrollments
(
ssID
, enAge
, enLevel
, enM
, enF
)
Select ssID
, Age
, stueClass
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
-- only load up the ones in this book!
	WHERE stueYEar = @Year
GROUP BY STUE.ssID
, STUE.Age
, stueClass


DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'REP'
and ssID in (Select ssID from StudentEnrolment WHERE stueYear = @Year)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptAge
, ptLevel
, ptM
, ptF
)
Select 'REP'
, ssID
, Age
, stueClass
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
WHERE STUE.stueFrom = 'REP'
-- only load up the ones in this year
	AND stueYear = @Year
GROUP BY STUE.ssID
, STUE.Age
, stueClass

-- pre-school attenders - these are those with ECE in the source column, and enrolment in Year level 1

DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'PSA'
and ssID in (Select ssID from StudentEnrolment WHERE stueYear = @Year)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptAge
, ptM
, ptF
)
Select 'PSA'
, ssID
, Age
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
	LEFT JOIN lkpLevels L
		ON STUE.stueClass = L.codeCode
WHERE STUE.stueFrom = 'ECE'
-- for safety, be sure only to count year 1 enrollees
	AND L.lvlYear = 1
	AND stueYear = @year
GROUP BY STUE.ssID
, STUE.Age
, stueClass

-- Transfers In

DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'TRIN'
and ssID in (Select ssID from StudentEnrolment WHERE stueYear = @Year)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptLevel
, ptPage
, ptAge
, ptM
, ptF
)
Select 'TRIN'
, ssID
, stueClass
, TFRI.iGroup		-- group by the State from which they transferred
, STUE.Age
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
	LEFT JOIN Schools TFRS
		ON STUE.stueFromSchool = TFRS.schNo
	LEFT JOIN Islands TFRI
		ON TFRS.iCode = TFRI.iCode
WHERE STUE.stueFrom = 'TRIN'
-- only load up the ones in this book!
	AND stueYear = @year
GROUP BY STUE.ssID
, TFRI.iGroup
, stueClass
, Age

-- disability


DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'DIS'
and ssID in (Select ssID from StudentEnrolment WHERE stueYear = @Year)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptAge
, ptLevel
, ptRow
, ptM
, ptF
)
Select 'DIS'
, ssID
, Age
, stueClass
, stueSpEdDisability
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
WHERE ( stueSpEdDisability is not null)		-- count on the disability code, rather than the Sp Ed flag
-- only load up the ones in this year
	AND stueYear = @Year
GROUP BY STUE.ssID
, STUE.Age
, stueClass
, stueSpEdDisability

commit
END
GO

