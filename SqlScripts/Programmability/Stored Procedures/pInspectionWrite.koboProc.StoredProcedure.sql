SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 12 2017
-- Description:	first attempt at working with kobo downloaded data
-- =============================================
CREATE PROCEDURE [pInspectionWrite].[koboProc]
	-- Add the parameters for the stored procedure here

	@xml xml
	, @filereference uniqueidentifier
	, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- this is the type of Kobo form implemented by this stored proc
-- it must match a description in the table lkpInspectionTypes

declare @rows TABLE
(
 	rowIndex int
	, [start] datetime
	, [end] datetime
	, inspectionType nvarchar(20)
	, schNo nvarchar(50)
	, rowXml xml
	, filereference uniqueidentifier null
)

-- represent the xml file - column names are 'sanitized'
INSERT INTO @rows
SELECT
v.value('@Index', 'int') [ RowIndex]
, nullif(ltrim(v.value('@start', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@end', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@inspectionType', 'nvarchar(50)')),'')
, nullif(ltrim(v.value('@schoolNo', 'nvarchar(50)')),'')
, v.query('.')

-- file reference is the id of the input file
, @fileReference

from @xml.nodes('ListObject/row') as V(v)


declare @inspectionType nvarchar(20)
declare @inspSetId int
declare @inspSetYear int


-- we create an inspection set representing this upload
-- key it with the file reference

Select @inspSetYear = year(getDate())

INSERT INTO InspectionSet
(
	inspsetName
	, inspsetType
	, inspSetYear
)
Select TOP 1
	intyDesc
	, intyCode
	, @inspSetYear
FROM @rows R
INNER JOIN lkpInspectionTypes I
	ON R.inspectionType = I.intyCode

SELECT @inspSetID = SCOPE_IDENTITY()


-- create the inspections
INSERT INTO SchoolInspection
( schNo,
inspStart,
inspEnd,
inspsetID,
inspXml
)
SELECT schNo
, [start]
, [end]
, @inspSetId
, rowXml
FROM @rows
WHERE schNo is not null

-- return the inspections
Select * from SchoolInspection
WHERE inspSetID = @inspSetId


END
GO

