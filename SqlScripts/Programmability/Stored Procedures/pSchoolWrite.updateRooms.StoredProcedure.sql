SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pSchoolWrite].[updateRooms]
	-- Add the parameters for the stored procedure here
	@SurveyID int = 0,
	@roomXML xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Validate the arguments
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;


	if @SurveyID is null
		begin
			set @err = 999
			set @ErrorMessage = 'Null SurveyID passed to updateRooms'
			set @ErrorSeverity = 16
			set @ErrorState = 0

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			return @err
		end

	declare @Total int
	Select @Total = count(ssID) from SchoolSurvey WHERE ssId = @surveyID

	if (@total = 0)
		begin
			set @err = 999
			set @ErrorMessage = 'Unknown Survey ID'
			set @ErrorSeverity = 16
			set @ErrorState = 0

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			return @err
		end


		declare @roomtbl table(
								RoomType nvarchar(10),
								QualityCode nvarchar(50),
								RoomNumber smallint,
								RoomTitle nvarchar(50),
								BuildingReviewID int,
								ClassLevel nvarchar(10),
								RoomLength float,
								RoomWidth float,
								RoomSize float,
								YearConstruction int,
								Materials nvarchar(1),
								MaterialsRoof nvarchar(1),
								MaterialsFloor nvarchar(1),
								Condition nvarchar(1),
								RoomUse nvarchar(200),
								SecureDoor smallint,
								SecureWindows smallint,
								CapF int,
								CapM int,
								ShowersF int,
								ShowersM int,
								ToiletsF int,
								ToiletsM int,
								Laundry smallint,
								Kitchen smallint,

								Beds int,
								Matresses int,
								Cupboards int,

								WoodStove int,
								GasStove int,
								ElecStove int,
								LockedStorage int,
								ColdStorage int,
								LunchDay int,
								LunchBoarder int,
								Location nvarchar(50),
								Sector nvarchar(10),
								RoomGUID uniqueidentifier
							 )
		declare @furntbl table(
								RoomNumber int,
								RoomType nvarchar(10),
								FurnType nvarchar(50),
								Num int,
								Condition nvarchar(1)
								)

		declare @typestbl table(
								RoomType nvarchar(10)
								)
		declare @deletedRooms table(
								rmID int
								)

		declare @insertedRooms table(
								RoomType nvarchar(10),
								RoomNumber int
								)


	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @roomXML

-- populate the table of grid values
	INSERT @roomtbl
	Select * from OPENXML (@idoc, '/Rooms/Room',2)
		WITH (
				Roomtype nvarchar(10) '@roomType',
				QualityCode nvarchar(50) '../@qualityCode',
				RoomNumber int '@displaySeq',
				RoomTitle nvarchar(50) '@roomName',
				BuildingReviewID int '@buildingReviewID',
				ClassLevel nvarchar(10) '@classLevel',
				RoomLength float '@roomLength',
				RoomWidth float '@roomWidth',
				RoomSize float '@roomArea',
				YearConstruction int '@yearConstruction',
				Materials nvarchar(1) '@materials',
				MaterialsRoof nvarchar(1) '@materialsRoof',
				MaterialsFloor nvarchar(1) '@materialsfloor',
				Condition nvarchar(1) '@condition',
				RoomUse nvarchar(200) '@roomUse',
				SecureDoor smallint '@secureDoor',
				SecureWindows smallint '@secureWindows',
				CapF int '@capF',
				CapM int '@capM',
				ShowersF int '@showersF',
				ShowersM int '@showersM',
				ToiletsF int '@toiletsF',
				ToiletsM int '@toiletsM',

				Laundry smallint '@laundry',
				Kitchen smallint '@kitchen',

				Beds int '@beds',
				Matresses int '@matresses',
				Cupboards int '@cupboards',

				WoodStove int '@woodStove',
				GasStove int '@gasStove',
				ElecStove int '@elecStove',
				LockedStorage int '@lockedStorage',
				ColdStorage int '@coldStorage',
				LunchDay int '@lunchDay',
				LunchBoarder int '@lunchBoarder',
				Location nvarchar(50) '../@location',		-- location is on the room tag
				Sector nvarchar(10) '../@sector',		-- forced school type IS NOW CALLED SECTOR
				RoomGUID uniqueidentifier '@roomGUID'
)
	INSERT @furntbl

	SELECT * from OPENXML (@idoc, '/Rooms/Room/Furniture/Item',2)	-- capitalised elements 9 5 2009
		WITH (
				RoomNumber int '../../@displaySeq',					-- FIXED 20 8 2009
				RoomType nvarchar(10) '../../@roomType',
				FurnType nvarchar(50) '@furnType',
				Num int '@num',
				Condition nvarchar(1) '@condition'

)

	INSERT @typestbl
	SELECT * from OPENXML (@idoc, '/Rooms/RoomTypes/RoomType',2)
		WITH (
				RoomType nvarchar(10) '.'
			)


    -- Insert statements for procedure here
--	SELECT * from @typestbl
--	SELECT * from @roomtbl
--	sELECT * FROM @fURNTBL

declare @Location nvarchar(50)
declare @Sector nvarchar(10)

Select @Location = Location,
		@Sector = Sector
	from OPENXML (@idoc, '/Rooms',2)
		WITH (
				Location nvarchar(50) '@location'
				, Sector nvarchar(10) '@sector'
			  )

-- deal separately with inserts , updates, and deletes

-- deletes first - these room type/room no combination are no longer there
-- select @Location, @SchoolType
-- the rooms that aren;t there any more
-- have to match the Location and SchoolType
INSERT @deletedRooms
Select rmID from Rooms
left join @roomtbl TMP
on Rooms.rmType = TMP.roomType
and Rooms.RmNo = TMP.roomNumber
WHERE Rooms.ssID = @SurveyID
and Rooms.rmType = any (Select roomType from @typestbl)
and (Rooms.rmLocation = @Location or @Location is null)
and (Rooms.rmSchLevel = @Sector or @Sector is null)
and TMP.roomNumber is null


--	Select * from @deletedrooms


INSERT @insertedRooms
Select roomType, roomNumber from @roomtbl TMP
left join (Select rmType, rmNo from Rooms WHERE
			ssID = @SurveyID
			and (Rooms.rmLocation = @Location or @Location is null)
			and (Rooms.rmSchLevel = @Sector or @Sector is null)
			) R
on TMP.roomType = R.rmType
	and TMP.roomNumber = R.rmNo
where R.rmNo is null


/*
Select * from @deletedRooms
Select * from @insertedRooms
*/


--- transaction can start here

begin transaction

begin try

-- now delete ALL the furniture
	DELETE from Furniture
	from Furniture F
	INNER JOIN Rooms  R on F.rmID = R.rmID
	WHERE R.ssID = @SurveyID
	and R.rmType = any (Select roomType from @typestbl)
	and (R.rmLocation = @Location or @Location is null)
	and (R.rmSchLevel = @Sector or @Sector is null)


-- unreference the deleted rooms on Classes
	UPDATE Classes
		SET rmID = null
	from Classes C
		WHERE C.ssID = @surveyID			-- this is kind of a hint really
		and rmID = any (Select rmID from @deletedRooms)


-- delete the DELETED rooms
	DELETE from Rooms
	from Rooms R
	WHERE R.rmID = any (Select rmID from @deletedRooms)


-- update the UPDATED Rooms - we preserve the rmID this way

	UPDATE Rooms
		SET rmQualityCode = TMP.QualityCode
		, rmTitle = TMP.RoomTitle
		, brevID = TMP.BuildingReviewID
		, rmLength = TMP.RoomLength
		, rmWidth = TMP.RoomWidth
		, rmSize = TMP.roomSize
		, rmYear = TMP.YearConstruction
		, rmMaterials = TMP.Materials
		, rmMAterialRoof = TMP.MaterialsRoof
		, rmMaterialFloor = TMP.MaterialsFloor
		, rmCondition =	TMP.Condition

		, rmUse = TMP.RoomUse
		, rmSecureDoor = TMP.SecureDoor
		, rmSecureWindows = TMP.SecureWindows
		, rmCapF =			TMP.CapF
		, rmCapM = TMP.CapM
		, rmShowersF = TMP.ShowersF
		, rmShowersM = TMP.ShowersM
		, rmToiletsF = TMP.ToiletsF
		, rmToiletsM = TMP.ToiletsM
		, rmLaundry = TMP.Laundry
		, rmKitchen = TMP.Kitchen

		, rmBeds = TMP.Beds
		, rmMatresses = TMP.Matresses
		, rmCupboards = TMP.Cupboards

		, rmStoveElec = TMP.ElecStove
		, rmStoveGas = TMP.GasStove
		, rmStoveWood = TMP.WoodStove
		, rmLockedStore = TMP.LockedStorage
		, rmRefrig = TMP.ColdStorage
		, rmLunchDay = TMP.LunchDay
		, rmLunchBoarder = TMP.LunchBoarder
		, rmLocation = TMP.Location
		, rmSchLevel = TMP.Sector
		, rmLevel = TMP.ClassLevel
		, rmGUID = TMP.roomGUID


	From Rooms R INNER JOIN @roomtbl TMP
			on R.rmType = TMP.roomType
			and R.RmNo = TMP.roomNumber
			WHERE R.ssID = @SurveyID
			and R.rmType = any (Select roomType from @typestbl)
			and (R.rmLocation = @Location or @Location is null)
			and (R.rmSchLevel = @Sector or @Sector is null)


-- add the rooms back

	INSERT INTO Rooms(ssID, rmType, rmQualityCode,
			rmNo, rmTitle, brevID, rmSchLevel,
			rmLength, rmWidth,
			rmSize, rmYear,
			rmMaterials, rmMaterialRoof, rmMaterialFloor,
			rmCondition, rmUse,
			rmSecureDoor, rmSecureWindows,
			rmCapF, rmCapM,
			rmShowersF, rmShowersM,
			rmToiletsF, rmToiletsM,
			rmLaundry,
			rmKitchen,
			rmBeds, rmMatresses, rmCupboards,

			rmStoveElec, rmStoveGas, rmStoveWood,
			rmLockedStore, rmRefrig,
			rmLunchDay, rmLunchBoarder
			, rmLocation
			, rmLevel

)
	Select @surveyID, TMP.RoomType, TMP.QualityCode,
			TMP.RoomNumber, TMP.RoomTitle, TMP.BuildingReviewID, TMP.Sector,
			TMP.RoomLength, TMP.RoomWidth,
			TMP.RoomSize, TMP.YearConstruction,
			TMP.Materials,TMP.MaterialsRoof,TMP.MaterialsFloor,
			TMP.Condition, TMP.RoomUse,
			TMP.SecureDoor,TMP.SecureWindows,
			TMP.CapF, TMP.CapM,
			TMP.ShowersF, TMP.ShowersM,
			TMP.ToiletsF, TMP.ToiletsM,
			TMP.Laundry,
			TMP.Kitchen,
			TMP.Beds, TMP.Matresses, TMP.Cupboards,
			TMP.ElecStove, TMP.GasStove, TMP.WoodStove,
			TMP.LockedStorage, TMP.ColdStorage,
			TMP.LunchDay, TMP.LunchBoarder
			, TMP.Location
			, TMP.ClassLevel
	From @roomtbl TMP
	inner join @insertedRooms I
		on TMP.roomType = I.roomType
		and tmp.roomNumber = I.roomNumber

-- add ALL the furniture back

	INSERT INTO Furniture(ssID,fCode,fLevel,fNum,rmID, fCond)
	Select @SurveyID, F.FurnType, null, F.Num,
			R.rmID, F.Condition
	From @furntbl F INNER JOIN Rooms R
		on F.RoomNumber = R.rmNo
		AND F.RoomType = R.rmType
	WHERE R.ssID = @SurveyID
			and (R.rmLocation = @Location or @Location is null)
			and (R.rmSchLevel = @Sector or @Sector is null)


/*
	 select * from Rooms WHERE ssID = @SurveyID
	 select * from furniture WHERE ssID = @SurveyID
*/

end try

begin catch

	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return

END
GO

