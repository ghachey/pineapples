SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Brian Lewis
-- Create date: 13 08 2015
-- Description: Read multiple reordsets about school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolReadEx]
 -- Add the parameters for the stored procedure here
 @SchoolNo nvarchar(50)
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

 SELECT * from Schools WHERE schNo = @schoolNo
 -- survey summary data
 exec pEnrolmentRead.schoolAnnualSummary  @schoolNo
 Select * from pExamRead.SchoolExams WHERE schNo = @schoolNo
 Select * from pInspectionRead.SchoolInspections WHERE schNo = @schoolNo

 	SELECT *
	from pSchoolRead.SchoolLinks
	WHERE schNo = @SchoolNo

 -- quarterly reports is specific to RMI. However, this recordset will always be
 -- exported to simplfy the construction of the School object
 Select * from pInspectionRead.QuarterlyReports WHERE schNo = @schoolNo
 Select * from pInspectionRead.SchoolAccreditations WHERE schNo = @schoolNo
END
GO

