SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 12 2007
-- Description:	Delete a schoolsurvey record given its ssID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteSchoolSurvey]
	-- Add the parameters for the stored procedure here
	@ssID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete
from SchoolSurvey
WHERE ssID = @ssID
END
GO
GRANT EXECUTE ON [dbo].[sp_DeleteSchoolSurvey] TO [pSchoolOps] AS [dbo]
GO

