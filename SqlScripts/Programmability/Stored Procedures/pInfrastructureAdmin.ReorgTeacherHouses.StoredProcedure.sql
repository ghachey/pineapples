SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 3 2010
-- Description:	Recast teacher houses as Buildings
-- =============================================
CREATE PROCEDURE [pInfrastructureAdmin].[ReorgTeacherHouses]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- first recordset is the totals of traditional housing by year

Select *
INTO #tmp
from
(
select schNo
	, [2005]
	, [2006],[2007],[2008],[2009]
from
(
select schNo, svyYear
	, resNumber
FROM Resources
	INNER JOIN SchoolSurvey SS
		ON resources.ssID = SS.ssID
	LEFT JOIN metaResourceDefs RD
		ON REsources.resName = RD.mresCAt
			AND Resources.resSplit = RD.mresName
WHERE resName like 'Staff Housing Permanent%'
) p

pivot
( sum(resNumber)
FOR svyYear IN ([2005],[2006],[2007],[2008],[2009])
) pvt
) SUB


select schNo
	, svyYEar
	, [T1B], [T2B], [T3B]
from
(
select schNo, svyYear
	, resNumber
	, mresBuildingSubType
FROM Resources
	INNER JOIN SchoolSurvey SS
		ON resources.ssID = SS.ssID
	LEFT JOIN metaResourceDefs RD
		ON REsources.resName = RD.mresCAt
			AND Resources.resSplit = RD.mresName
WHERE resName like 'Staff Housing Traditional%'
	AND svyYear >=2005
) p

pivot
( sum(resNumber)
FOR mresBuildingSubType IN ([T1B],[T2B],[T3B])
) pvt

ORDER BY schNo, svyYEar

Select *
from  #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))
	AND ( isnull([2008],isnull([2007],0)) >=isnull([2007],0))
	AND ( isnull([2007],isnull([2006],0)) >=isnull([2006],0))
	AND ( isnull([2006],isnull([2005],0)) >=isnull([2005],0))

DELETE
from  #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))
	AND ( isnull([2008],isnull([2007],0)) >=isnull([2007],0))
	AND ( isnull([2007],isnull([2006],0)) >=isnull([2006],0))
	AND ( isnull([2006],isnull([2005],0)) >=isnull([2005],0))

Select *
FROM #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))
	AND ( isnull([2008],isnull([2007],0)) >=isnull([2007],0))
	AND ( isnull([2007],isnull([2006],0)) >=isnull([2006],0))

DELETE
from  #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))
	AND ( isnull([2008],isnull([2007],0)) >=isnull([2007],0))
	AND ( isnull([2007],isnull([2006],0)) >=isnull([2006],0))

Select *
FROM #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))
	AND ( isnull([2008],isnull([2007],0)) >=isnull([2007],0))

DELETE
from  #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))
	AND ( isnull([2008],isnull([2007],0)) >=isnull([2007],0))

Select *
FROM #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))


DELETE
from  #tmp
WHERE ( isnull([2009],isnull([2008],0)) >=isnull([2008],0))


Select *
FROM #tmp

-- the second is totals by type by year


END
GO

