SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	BRian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[CreateSchoolSurvey]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) ,
	@SurveyYear int ,
	@SchoolType nvarchar(10) = null		-- allow pass null to use the current type 20141112
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


begin try

-- 12 10 2009 we should always take this from Schools, becuase Schools should always be the current setting
-- it should come from SchoolSurvey and go to schools when the survey is created
-- this means we should update directly from Schools

/*		Select
			@ElectL = schElectL,
			@ElectN = schElectN,
			@Auth = schAuth,
			@Lang = schLang
		from
			Schools
		WHERE
			schNo = @schNo
*/

		-- insert these new values
		INSERT INTO SchoolSurvey(schNo, svyYear,ssSchType,
				ssElectL, ssElectN, ssAuth, ssLang)
		Select @schNo, @SurveyYear, isnull( @SchoolType, schType)
			, schElectL
			, schElectN
			, schAuth
			, schLang

		from
			Schools
		WHERE
			schNo = @schNo

		select scope_identity() ssID
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

