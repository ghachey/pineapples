SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for toilets pivot
-- =============================================
CREATE PROCEDURE [dbo].[PIVResources]
	@DimensionColumns nvarchar(20) = 'CORE',
	@DataColumns nvarchar(20) = null,
-- for filtering by school and year
	@Group nvarchar(30) = 'WaterSupply',
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null,
	-- for interactive enquiries - 'quick pivots'
	@RowField nvarchar(50) = null,
	@ColField nvarchar(50) = null,
	@DataField nvarchar(50) = 'Number'


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select @DataColumns = dbo.pivDefaultDataColumns(@DataColumns)

-- no branching yet on datacolumns

	if (@Group = 'Water Supply') begin
		if (@DataColumns is null)
			EXEC dbo.PIVResources_WaterSupply
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_WaterSupply_SO
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

	if (@Group = 'Power Supply') begin
		if (@DataColumns is null)
			EXEC dbo.PIVResources_Default
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_Default_SO
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return
	end

	if (@Group in ('Text Books','Teacher Guides', 'Readers')) begin
		if (@DataColumns is null)
			EXEC dbo.PIVResources_TextBooks
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_TextBooks
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return
	end
	if (@Group in ('Library Resources','Special Needs Teaching')) begin
			--somalia uses the default for these formats

			EXEC dbo.PIVResources_Default
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end
	-- all other categries
		if (@DataColumns is null)
			EXEC dbo.PIVResources_Default
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_default_SO
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return


END
GO

