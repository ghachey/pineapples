SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 11 2009
-- Description:	find all database users, optinally excluse members of a single role
-- =============================================
CREATE PROCEDURE [dbo].[getPineapplesUsers]
	-- Add the parameters for the stored procedure here
	@ExcludeRole sysname = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ExcludedRoleID int
	select @ExcludedRoleID = database_principal_id(@ExcludeRole)
	select DP.Name
		, DP.Type
		, SP.Name LoginName
		, DM.member_principal_ID
from sys.database_principals DP
	Inner join sys.server_principals SP
	on DP.sID = SP.sID
	LEFT JOIN
	(Select member_principal_ID
		from sys.database_role_members
		WHERE role_principal_ID = @ExcludedRoleID) DM
	on DM.member_principal_ID = DP.principal_ID

WHERE
	dp.Type in ('G','U')			-- not interested in SQL logins


END
GO
GRANT EXECUTE ON [dbo].[getPineapplesUsers] TO [public] AS [dbo]
GO

