SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		John Louis
-- Create date: 14/11/07
-- Description:	InputDataIntoPIVCPTable
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVCPData]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT *
INTO #ebse

FROM dbo.tfnESTIMATE_BestSurveyenrolments()

    -- Insert statements for procedure here
	SELECT
	E.schNo,
	E.Lifeyear as [Survey Year],
	E.ActualssID,
	E.bestssID,
	E.surveyDimensionssID,
	E.Estimate,
	E.Offset as [Age of Data],
	E.bestYear as [Year of Data],
	C.ptCode as [Issue Category Code],
	DEFS.tdefName AS [Issue Category],
	C.cpItem as [CP Issue Code],
	TRCP.cpDesc AS [CP Issue],
	C.ptLevel,
	C.ptM,
	C.ptF

From
	vtblChildProtection C
		INNER JOIN #EBSE E
			ON C.ssID = E.bestssID
		INNER JOIN TRmetaPupilTableDefs DEFS
			ON C.ptCode = DEFS.tdefCode
		INNER JOIN TRChildProtection TRCP
			ON C.cpItem = TRCP.cpCode

Drop TABLE #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVCPData] TO [pEnrolmentReadX] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[sp_PIVCPData] TO [pSchoolReadX] AS [dbo]
GO

