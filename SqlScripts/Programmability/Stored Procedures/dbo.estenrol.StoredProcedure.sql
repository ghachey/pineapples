SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		estenrol
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[estenrol]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @forward int
	declare @backward int

	select @forward = dbo.sysParamInt(N'EST_ENROL_FILL_FORWARD')
	select @backward = dbo.sysParamInt(N'EST_ENROL_FILL_BACKWARD')
    -- Insert statements for procedure here
	drop table ##estenrol
	SELECT     schNo, LifeYear, ActualssID,
			bestssID,
			bestYear, LifeYear - bestYear AS Offset,
			bestssqLevel,
			QI.ssqLevel ActualssqLevel,
			isnull(bestssID, ActualssID) SurveydimensionssID,
		CASE
			WHEN (BestYear IS NULL) THEN NULL
            WHEN LifeYear = BestYear THEN 0
			WHEN QI.ssqLevel = 2 then 2
			ELSE 1
		END AS Estimate
into ##estenrol
FROM
	(SELECT     schNo, LifeYear, ActualssID,
		MIN(xBestData) AS BestData,
		dbo.fnextractBestssID(MIN(xBestData)) AS bestssID,
		dbo.fnextractBestYear(MIN(xBestData)) AS bestYear,
		dbo.fnExtractBestSSQLevel(MIN(xBestData)) AS bestSSQLevel

     FROM  (SELECT     L.schNo, L.svyYear AS LifeYear, L.ActualssID,
			D.svyYear AS EnrolYear,
			dbo.fnMakeBestDataStr(L.svyYear, D.svyYear, D.EnrolssID, D.ssqLevel) AS xBestData
			FROM dbo.SchoolLifeYears AS L LEFT OUTER JOIN
				dbo.schoolYearHasDataEnrolments AS D
				ON L.schNo = D.schNo AND D.svyYear BETWEEN
                L.svyYear - @forward AND L.svyYear + @backward
			) AS subQ
	 GROUP BY schNo, LifeYear, ActualssID
	) AS subQ2
	LEFT OUTER JOIN dbo.tfnQualityIssues('Pupils','Enrol') QI
	ON subQ2.ActualssID = QI.ssID

END
GO

