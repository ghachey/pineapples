SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 09 2011
-- Description:	SimpleXML of school counts to feed into PAF
-- =============================================
CREATE PROCEDURE [dbo].[xmlSchoolCounts]
	-- Add the parameters for the stored procedure here
	@SendAsXML int
	, @xmlOut xml OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    exec dbo.BuildRank
    DECLARE @xml xml

    SELECT @xml =
    (
    Select svyYear [@year]
	, rankSchType	[@schoolType]
	, count(schNo)  [count]
	, sum(rankEnrol) [enrol]
	, sum(rankEnrolM) [enrolM]
	, sum(rankEnrolF) [enrolF]
	, convert(decimal(7,4),convert(float,sum(case when rankEstimate = 1 then isnull(rankEnrol,0) else 0 end)) / sum(rankEnrol)) [est]

	FROM SurveyYearRank
	GROUP BY svyYear
	, rankSchType
	ORDER by svyYear, rankSchType
	FOR XML PATH('SchoolCount')
	)

	SELECT @xmlOut=
	(
	SELECT @xml
	FOR XML PATH('SchoolCounts')
    )
END
GO

