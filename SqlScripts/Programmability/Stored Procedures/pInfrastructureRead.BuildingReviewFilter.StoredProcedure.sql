SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 2 2010
-- Description:	Return the set of inspections for a given building
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[BuildingReviewFilter]
	-- Add the parameters for the stored procedure here
	@BuildingID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select
		inspStart
		, schNo
		, inspNote
		, inspBy
		, inspsetName
		, BR.*
	from SchoolInspection SI
	INNER JOIN InspectionSet ISET
		ON SI.inspsetID = ISET.inspsetID
	INNER JOIN BuildingREview BR
		on SI.inspID = BR.inspID
			AND BR.bldID = @buildingID
END
GO

