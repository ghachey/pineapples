SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 2 2010
-- Description:	School Planning Data
-- =============================================
CREATE PROCEDURE [dbo].[schoolPlanningSheet]
	-- Add the parameters for the stored procedure here
	@schoolNo nvarchar(50) = null,
	@ScenarioID int = null,
	@StartYear int = null,
	@SchoolType nvarchar(10) = null,
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try
	-- the school planning sheet contains
	-- the historic enrolments by level
	-- a nominated projection scenario, by level

	-- teacher requirements from the establishment quota, by year
	-- current teacher numbers

	-- classroom numbers current
	-- classrooms to complete by year of work order

	-- <Planner><School schoolNo="" schoolName = "">
	--		<Enrolments>
	--			<Enrolment year="" estimate="1" enrol=""><Level code="" enrol=""/>
	--		<TeacherCounts>
	--			<TeacherCount year="" survey="" appointments="">
	--      <Projections scenario="" name="">
	--			<Projection year="" enrol=""><Level
	--      <RoomCompletions>
	--			<RoomCompletion year="" total=""><RoomClass code="" add="" replace=""/>
	--		<TeacherQuotas>
	--			<TeacherQuota year="" quota=""><RoleGrade code="" quota=""/>
	--		<BuildingCounts>
	--			<BuildingCount year="">
	--				<BuildingType count="">
	--		<BuildingRoomCounts>
	--			<BuildingRoomCount year=""><RoomClass code="" count=""/>
	--		<EstablishmentPlans>
	--			<EstablishmentPlan year="" total="">
	--				<RoleGrade code="" total="">

	--- first make a list of the schools we are interested in

	--- clean up the parameters


--**************************************************************************************
-- build all temp tables up front
--**************************************************************************************


-- Main Planning sheet table
-- this generates the XML Explicit

CREATE TABLE #ps
(
	Tag	int
	, Parent int
	,schNo nvarchar(50)
	, schName nvarchar(50)
	, LastSurveyYear int
	, LastBuildingReview datetime
	, Sorter nvarchar(2)
	, plYear int
	, Estimate int
	, total int
	, levelCode nvarchar(10)
	, enrol int
	, ScenarioID int
	, ScenarioName nvarchar(50)
	, RoleGrade nvarchar(10)
	, TeacherRole nvarchar(10)
	-- for room completions
	, roomFunction nvarchar(10)
	, added int
	, replaced int
	-- for room totals
	, BuildingType nvarchar(10)
	, roomTotal int
	-- for establishment
	, filled int


)

-- for teacher quota

create table #tempQ
(
schNo nvarchar(50)
, epYear int
, rgCode nvarchar(10)
, Quota int
, CountUser int
)

-- room completions
CREATE TABLE #rcomp
( schNo nvarchar(50)
	, woYEar int
	, rfcnCode nvarchar(10)
	, Added int
	, Replaced int
)

-- this is for building completions
CREATE TABLE #bldcomp
( schNo nvarchar(50)
	, woYEar int
	, bldgCode nvarchar(10)
	, Added int
	, Lost int
)

-- building data
CREATE TABLE #tmpBuild
(
	svyYear int
	, schNo nvarchar(50)
	, bldID int				-- the building identifier
	, bldgCode nvarchar(10)
	, LastReviewed Datetime
	, RoomsClass int NULL
	, RoomsOHT int NULL
	, RoomsStaff int NULL
	, RoomsAdmin int NULL
	, RoomsStorage int NULL
	, RoomsDorm int NULL
	, RoomsDining int NULL
	, RoomsKitchen int NULL
	, RoomsLibrary int NULL
	, RoomsHall int NULL
	, RoomsSpecialTuition int NULL
	, RoomsOther int NULL
	, RoomsAll int NULL
)

CREATE TABLE #sch
(
	schNo	nvarchar(50)
	, schName nvarchar(50)
	, LastBuildingReview datetime NULL
	, LastSurvey int NULL
)
---------------------------------------------------------------------------
-- preliminaries -- populate any defaults, and set the school set

	if @ScenarioID is null begin
		Select TOP 1 @ScenarioID = escnID
		from EstablishmentControl
		WHERE EscnId is not null
		ORDER BY estcYear DESC
	end

	if @ScenarioID is null begin
		raiserror('No scenario ID was supplied and no scenarioID could be found',16,1);
	end

	Select @StartYear = isnull(@StartYear,0)

	INSERT INTO #sch
	(	schNo
		, schName
		, LastBuildingReview
		, LastSurvey
	)
	Select Schools.schNo
	, schName
	, LastBuildingReview
	, LastSurvey
	FROM Schools
		LEFT JOIN Islands
			ON Schools.iCode = Islands.iCode
		LEFT JOIN (Select schNo
						, max(inspStart) LastBuildingReview
					from SchoolInspection SI
						INNER JOIN InSpectionSet ISET
						ON SI.inspSetID = ISET.inspSetID
					WHERE ISET.inspSetType = 'BLDG'			-- hard coded here... hmmmm
					GROUP BY schNo
					) LastBR
			ON Schools.schNo = LastBR.schNo
		LEFT JOIN (Select schNo
					, max(svyYear) LastSurvey
					FROM SchoolSurvey
					WHERE SchoolSurvey.ssId = any (Select ssID FROM Enrollments)
					GROUP BY schNo
					) LastS
			ON Schools.schNo = LastS.schNo
	WHERE (Schools.schNo = @SchoolNo or @schoolNo is null)
	AND (schType = @schoolType or @schoolType is null)
	AND (iGRoup = @District or @District is null)
	AND (SchAuth = @Authority or @Authority is null)


	Select EE.*
	into #ee
	from dbo.tfnESTIMATE_BestSurveyEnrolments() EE
	where ee.schNo in (Select schNo from #sch)
		AND EE.LifeYear >= @StartYear

-- if no scenario ID is supplied,then we'll use the one associated to the
-- latest Establishment Plan

declare @thisYear int
declare @thisDate datetime

select @thisDate = getDate()
select @thisYear = year(@thisDate)

--**********************************************************************************
-- Build Data
--**********************************************************************************

-- School Header record: Tag = 1
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, LastSurveyYear
	, LastBuildingReview
)
Select 1
	, null
	, schNo
	, schName
	, LastSurvey
	, LastBuildingReview

from #sch

-- 2 is the header for the enrolments tree
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, Sorter
)
Select 2
	, 1
	, schNo
	, schName
	, 'A'
from #sch

-- 3 is the Enrolment year=
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, Sorter
	, plYear
	, Estimate
	, Total
)
Select 3
	, 2
	, #sch.schNo
	, #sch.schName
	, 'A'
	, LifeYear
	, Estimate
	, BestEnrol
from #sch
INNER JOIN #ee
	ON #ee.schNo = #sch.schNo

-- by level
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, Sorter
	, plYear
	, Estimate
	, Total
	, LevelCode
	, Enrol
)
Select 4
	, 3
	, #sch.schNo
	, #sch.schName
	, 'A'
	, LifeYear
	, Estimate
	, BestEnrol
	, enLevel
	, levelEnrol
from #sch
INNER JOIN #ee
	ON #ee.schNo = #sch.schNo
INNER JOIN (Select ssID, enLEvel, sum(isnull(enM,0) + isnull(enF,0)) levelEnrol
			FROM  Enrollments E
			group by ssId, enLevel
			) E
	On #ee.bestssID = E.ssID


--*****************************************************************************
-- Enrolment Projection
--*****************************************************************************

-- node type 5 is the PRojections tree - this details the specific projection used
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, Sorter
)
Select DISTINCT 5
	, 1
	, #sch.schNo
	, schName
	, 'B'
from #sch
WHERE #sch.schNo = any
	(
	SELECT schNo
		FROM EnrolmentProjection EP
	WHERE (EP.escnID = @ScenarioID )
	)

-- node type 6 is the PRojectionannual record

INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, Sorter
	, plYear
	, Estimate
	, Total
	, ScenarioID
)
Select 6
	, 5
	, #sch.schNo
	, null
	, 'B'
	, epYear
	, null
	, sum(epdSum)
	, escnID
from #sch
INNER JOIN EnrolmentProjection EP
	ON EP.schNo = #sch.schNo
INNER JOIN EnrolmentProjectionData EPD
	ON ep.epID = EPD.epID
WHERE (escnID = @ScenarioID )
GROUP BY #sch.schNo, epYear , escnID


-- Tag 7 is the projection level totals
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, schName
	, Sorter
	, plYear
	, Estimate
	, Total
	, LevelCode
	, Enrol
	, ScenarioID
)
Select 7
	, 6
	, #sch.schNo
	, #sch.schName
	, 'B'
	, epYear
	, null
	, null
	, epdLevel
	, epdSum
	, escnID
from #sch
INNER JOIN EnrolmentProjection EP
	ON EP.schNo = #sch.schNo
INNER JOIN EnrolmentProjectionData EPD
	ON ep.epID = EPD.epID
WHERE (escnID = @ScenarioID )

--*****************************************************************************
---- Teacher Quotas
--*****************************************************************************
-- Teacher Quotas depend on a specific projection , or else from the actual enrolment
-- <TeacherQuotas scenarioID=>
	-- <TeacherQuota  year="" >
		--<RoleGrade code="" quota=""/>
-- need to be a member of pEstablishmentRead to see this

-- get the results from QuotaForScenario


INSERT INTO #tempQ
EXEC pEstablishmentRead.quotaForScenario @scenarioID, null, @SchoolNo, 0


-- 8 is the teacher quotas tree

INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, ScenarioID
)
Select 8
	, 1
	, #sch.schNo
	, 'C'
	, @SCenarioID
from #sch
	WHERE #sch.schNo in (Select schNo from  #tempQ)

-- 9 is the TeacherQuota record for the year


INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, ScenarioID
	, plYear
	, total
)
Select 9 Tag
	, 8 as Parent
	, #sch.schNo
	, 'C'	as Sorter
	, @ScenarioID scenarioID
	, epYear
	, sum(quota) total
from #sch
	INNER JOIN #tempQ
		ON #sch.schNo = #tempQ.schNo
GROUP BY #sch.schNo,  epYear

-- 10 is the rolegrade node
INSERT INTO #ps

(
	Tag
	, Parent
	, schNo
	, Sorter
	, ScenarioID
	, plYear
	, total
	, RoleGrade
	, TeacherRole
)
Select  10
	, 9
	, #sch.schNo
	, 'C'
	, @SCenarioID
	, epYear
	, Quota
	, #tempQ.rgCode
	, roleCode
from #sch
	INNER JOIN #tempQ
		ON #sch.schNo = #tempQ.schNo
	INNER JOIN RoleGrades
		ON #tempQ.rgCode = RoleGRades.rgCode

DROP TABLE #tempQ


--***************************************************************************************
-- Room and Building Completions
--***************************************************************************************

-- have to get the number of rooms that are going to be built in a year from the work orders

-- double check we hae all room function codes defined
EXECUTE dbo.setup_lkpRoomFunction

-- build the temp table of room completions
INSERT INTO #rcomp
(SchNo
, woYear
, rfcnCode
, Added
)
Select schNo
	, woYEar
	, rfcnCode
	, value
from (
Select
	#sch.schNo
	, year(isnull(woPlannedCompletion, woPlanned)) woYear
	, rfcnCode
	, case rfcnCode
			when 'Class' then witmRoomsClass
			when 'OHT' then witmRoomsOHT
			when 'STAFF' then witmRoomsStaff
			when 'Admin' then witmRoomsAdmin
			when 'Storage' then witmRoomsStorage
			when 'Dorm' then witmRoomsDorm
			when 'Kitchen' then witmRoomsKitchen
			when 'Dining' then witmRoomsDining
			when 'Library' then witmRoomsLibrary
			when 'Spec' then witmRoomsSpecialTuition
			when 'Hall' then witmRoomsHall
			when 'Other' then witmRoomsOther
	end value

from WorkITems
	INNER JOIN WorkOrders
		ON workItems.woID = workOrders.woID
	INNER JOIN #sch
		ON #sch.schNo = WorkItems.schNo

CROSS JOIN lkpRoomFunction
) sub
WHERE value is not null


INSERT INTO #bldcomp
(SchNo
, woYear
, bldgCode
, Added
)
Select #sch.schNo
	, year(isnull(woPlannedCompletion, woPlanned)) woYear
	, witmBldCreateType
	, witmBldCreateNum			-- this is a clculated field, if UNIE = EACH then qty = num blgs

from WorkITems
	INNER JOIN WorkOrders
		ON workItems.woID = workOrders.woID
	INNER JOIN #sch
		ON #sch.schNo = WorkItems.schNo

WHERE witmBldCreateType is not null


-- the second inserts in #bldcomp are for rooms that are going away
-- a room is replaced if it is referenced on a wotkitem ine that also has a new room created
-- presumably it will be the same tye of room, but can;t guarantee that

INSERT INTO #bldcomp
(SchNo
, woYear
, bldgCode
, lost
)
Select #sch.schNo
	, year(isnull(woPlannedCompletion, woPlanned)) woYear
	, bldgCode
	, 1

from WorkITems
	INNER JOIN WorkOrders
		ON workItems.woID = workOrders.woID
	INNER JOIN #sch
		ON #sch.schNo = WorkItems.schNo
	INNER JOIN Buildings
		ON Buildings.bldID = WorkItems.bldID

WHERE witmBldCreateType is not null		-- that's how we know its getting replaced

----------Room completions tree
-------------------------------------
-- 11 it the RoomCompletions tree
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
)
Select DISTINCT 11
	, 1
	, schNo
	, 'D'
from #rcomp

-- 12 is room completion year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, Added
	, Replaced
)
Select 12
	, 11
	, schNo
	, 'D'
	, woYear
	, sum(added)
	, sum(replaced)

from #rcomp
GROUP BY #rcomp.schNo, #rcomp.woYear

-- 13 is the room function details
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, RoomFunction
	, Added
	, Replaced
)
Select 13
	, 12
	, schNo
	, 'D'
	, woYear
	, rfcnCode
	, sum(added)
	, sum(replaced)

from #rcomp
GROUP BY SchNo, woYear, rfcnCode

--**************************************************************************
--************ Building Completions ****************************************
--**************************************************************************
-- insert in here building completions for particular building types
-- 24 is building Completion header
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
)
Select DISTINCT 24
	, 1
	, schNo
	, 'DD'
from #bldcomp

-- total buildings by year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, Added
	, Replaced
)
Select 25
	, 24
	, schNo
	, 'DD'
	, woYear
	, sum(added)
	, sum(lost)

from #bldcomp
GROUP BY #bldcomp.schNo, #bldcomp.woYear

-- totoal buildings by building type byyear
-- total buildings by year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, BuildingType
	, Added
	, Replaced
)
Select 26
	, 25
	, schNo
	, 'DD'
	, woYear
	, bldgCode
	, sum(added)
	, sum(lost)

from #bldcomp
GROUP BY #bldcomp.schNo, #bldcomp.woYear, #bldcomp.bldgCode

--**********************************************************************
--******************* Building Count ***********************************
--**********************************************************************

-- next the count of building types
-- this depends on exisitng data - ie not a forecast or projection

-- Node 14 is the BuidlingTypeCounts node
-- only include schools in the selection set
-- that are in the #sch list, and have a building type at all


-- node 15 is the header for the year
-- we put a total on here , but its apples and oranges really
-- 12 is room completion year

-- this logic is to find the stae of buildings at a point in time
-- for each survey census date, get the latest building review for eachbuilding,
-- on or before that date
-- Also, for the current year, which must be the highest year that a building survey can be conducted
-- we hae to make sure we don;t miss a survey, if there is no Census yet set up, or the Building REview
-- takes place after the census date.
-- Choices are to use, in place of the census date in the current year
------ the current date
------ or, 31/12
-- let's assume we cannot future date a building review, so we'll use the current date

INSERT INTO #tmpBuild
(
	svyYear
	, schNo
	, bldID
	, bldgCode
	, LastReviewed
	, RoomsClass
	, RoomsOHT
	, RoomsStaff
	, RoomsAdmin
	, RoomsStorage
	, RoomsDorm
	, RoomsDining
	, RoomsKitchen
	, RoomsLibrary
	, RoomsHall
	, RoomsSpecialTuition
	, RoomsOther
	, RoomsAll
)

Select svyYear
	, schNo
	, U.bldID
	, brevCode
	, inspStart LastReviewed
	,brevRoomsClass RoomsClass
	,brevRoomsOHT RoomsOHT
	,brevRoomsStaff RoomsStaff
	,brevRoomsAdmin RoomsAdmin
	,brevRoomsStorage RoomsStorage
	,brevRoomsDorm RoomsDorm
	,brevRoomsDining RoomsDining
	,brevRoomsKitchen RoomsKitchen
	,brevRoomsLibrary RoomsLibrary
	,brevRoomsHall RoomsHall
	,brevRoomsSpecialTuition RoomsSpecialTuition
	,brevRoomsOther RoomsOther
	,brevRoomsAll RoomsAll
from
(
Select brevID, B.bldID, B.schNo, brevClosed
	, inspStart, BR.inspID, svyYear, svyCensusDate censusDate
	, row_number() OVER (PARTITION BY BR.bldID, svyCensusDate ORDER by inspStart DESC) POS
		from
			BuildingREview BR
				INNER JOIN Buildings B
					ON BR.bldID = B.bldID
				INNER JOIN #sch
					ON B.SchNo = #sch.schNo
				INNER JOIN SchoolInspection
					ON SchoolInspection.inspID = BR.inspID
				INNER JOIN
					(Select svyYear, svyCensusDate
					FROM Survey
					WHERE svyYEar < @thisYear
					UNION ALL
					Select @thisYear
					, @thisDate
					) surveyEx
					ON SchoolInspection.inspStart <=surveyEx.svyCensusDate
				WHERE (@StartYear <= surveyEx.svyYear)
) U
INNER JOIN BuildingReview BR
	on BR.brevID = U.brevID
WHERE U.Pos = 1
	AND U.brevClosed = 0	-- don;t include any building if it is closed on the last review prior to the annual survey date


-- However, not every building will have a review
-- For those that don't, we write their building data
INSERT INTO #tmpBuild
(
	svyYear
	, schNo
	, bldID
	, bldgCode
	, LastReviewed
	, RoomsClass
	, RoomsOHT
	, RoomsStaff
	, RoomsAdmin
	, RoomsStorage
	, RoomsDorm
	, RoomsDining
	, RoomsKitchen
	, RoomsLibrary
	, RoomsHall
	, RoomsSpecialTuition
	, RoomsOther
	, RoomsAll
)
SELECT
	SurveyEx.svyYEar
	, #sch.schNo
	, Buildings.bldID
	, bldgCode
	, null
	, [bldgRoomsClass]
      ,[bldgRoomsOHT]
      ,[bldgRoomsStaff]
      ,[bldgRoomsAdmin]
      ,[bldgRoomsStorage]
      ,[bldgRoomsDorm]
      ,[bldgRoomsKitchen]
      ,[bldgRoomsDining]
      ,[bldgRoomsLibrary]
      ,[bldgRoomsHall]
      ,[bldgRoomsSpecialTuition]
      ,[bldgRoomsOther]
      ,[bldgRoomsAll]
from Buildings
INNER JOIN #sch
	ON Buildings.SchNo = #sch.schNo
INNER JOIN
	(Select svyYear, svyCensusDate
					FROM Survey
					WHERE svyYEar < @thisYear
					UNION ALL
					Select @thisYear
					, @thisDate
					) surveyEx
	ON isnull(Buildings.bldgYear,0) <= SurveyEx.svyYear
LEFT JOIN
-- this join finds all the reviews before the Census Date
	(Select DISTINCT bldID, inspStart
		from BuildingReview BR
					INNER JOIN SchoolInspection
						ON SchoolInspection.inspID = BR.inspID
	) u
		ON Buildings.bldID = u.bldID
		AND SurveyEx.svyCensusDate >= U.inspStart
WHERE U.bldID is null		-- so this means there is no review before the census date
							-- which means we get the building from its header
AND @startYear <=SurveyEx.svyYear
AND (Buildings.bldgCloseDate is null or Buildings.bldgCloseDate < surveyEx.svyCensusDate)

-- node 14 is the BuildingCounts tree
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
)
Select DISTINCT 14
	, 1
	, schNo
	, 'E'
from #sch
WHERE #sch.schNo = any (Select schNo from #tmpBuild)

-- node 15 is building count by year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, total
	, RoomTotal
)
Select 15
	, 14
	, schNo
	, 'E'
	, svyYear
	, count(bldID)
	, sum(RoomsAll)

from #tmpBuild

GROUP BY #tmpBuild.schNo, #tmpBuild.svyYear

-- node 16 is buildingcount by building type
-- node 15 is building count by year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, BuildingType
	, total
)
Select 16
	, 15
	, schNo
	, 'E'
	, svyYear
	, bldgCode
	, count(bldID)

from #tmpBuild
GROUP BY #tmpBuild.schNo, #tmpBuild.svyYear, #tmpBuild.bldgCode

-- node 17 is the room count by room function - this is a bit different to the others
-- hangs off BuildingCount year=

INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, RoomFunction			-- reuse for the building type
	, total
)
Select 17
	, 15
	, schNo
	, 'E'
	, svyYear
	, rfcnCode
	, value
from
(
	Select schNo
		, svyYear
		, rfcnCode
		, sum(case rfcnCode
				when 'Class' then RoomsClass
				when 'OHT' then RoomsOHT
				when 'STAFF' then RoomsStaff
				when 'Admin' then RoomsAdmin
				when 'Storage' then RoomsStorage
				when 'Dorm' then RoomsDorm
				when 'Kitchen' then RoomsKitchen
				when 'Dining' then RoomsDining
				when 'Library' then RoomsLibrary
				when 'Spec' then RoomsSpecialTuition
				when 'Hall' then RoomsHall
				when 'Other' then RoomsOther
		end) value
	from #tmpBuild
	CROSS JOIN lkpRoomFunction
	GROUP BY #tmpBuild.schNo, #tmpBuild.svyYear, lkpRoomFunction.rfcnCode
) u
WHERE value is not null

----***************************************************************************************
-- Establishment Plan
----***************************************************************************************

---- 18 is the EstablishmentPlan headers
-- node 14 is the BuildingCounts tree
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
)
Select DISTINCT 18
	, 1
	, schNo
	, 'F'
from #sch
WHERE #sch.schNo = any (Select schNo from SchoolEstablishment)

---- 19 is the EstablishmentPlan per year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, total
)
Select 19
	, 18
	, #sch.schNo
	, 'F'
	, estYear
	, sum(estrCount)
FROM #sch
	INNER JOIN SchoolEstablishment SE
		ON SE.schNo = #sch.schNo
	INNER JOIN SchoolEstablishmentRoles SER
		ON SE.estID = SER.estID
GROUP BY #sch.schNo, estYEar

-- 20 is the total by role grade
---- 19 is the EstablishmentPlan per year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	,RoleGrade
	, TeacherRole
	, total
)
Select 20
	, 19
	, #sch.schNo
	, 'F'
	, estYear
	, estrRoleGrade
	, roleCode
	, sum(estrCount)
FROM #sch
	INNER JOIN SchoolEstablishment SE
		ON SE.schNo = #sch.schNo
	INNER JOIN SchoolEstablishmentRoles SER
		ON SE.estID = SER.estID
	INNER JOIN RoleGrades RG
		ON Rg.rgCode = SER.estrRoleGrade
GROUP BY #sch.schNo, estrRoleGrade, roleCode, estYEar


---------Establishment -----------------------------
-- this is calcuated by looking at the positions that are in effect on Census Day
---- 21 is the Establishment headers
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
)
Select DISTINCT 21
	, 1
	, schNo
	, 'G'
from #sch
WHERE #sch.schNo = any (Select schNo from Establishment)

---- 22 is the Establishment per year
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	, total
	, filled
)
Select 22
	, 21
	, #sch.schNo
	, 'G'
	, svyYear
	, count(E.estpNo)
	, count(TA.taID)
FROM #sch
	INNER JOIN Establishment E
		ON #sch.schNo = E.schNo
	INNER JOIN Survey
		ON Survey.svyCensusDate >= E.estpActiveDate
		AND (Survey.svyCensusDate <= E.estpClosedDate or E.estpClosedDate is null)
	LEFT JOIN TeacherAppointment TA
		ON TA.estpNo = E.estpNo
		AND Survey.svyCensusDate >= TA.taDate
		AND (Survey.svyCensusDate <= TA.taEndDate or TA.taEndDate is null)
	GROUP BY #sch.schNo, svyYEar

-- 23 is the total by role grade
INSERT INTO #ps
(
	Tag
	, Parent
	, schNo
	, Sorter
	, plYear
	,RoleGrade
	, total
	, filled
)
Select 23
	, 22
	, #sch.schNo
	, 'G'
	, svyYear
	, estpRoleGrade
	, count(E.estpNo)
	, count(TA.taID)
FROM #sch
	INNER JOIN Establishment E
		ON #sch.schNo = E.schNo
	INNER JOIN Survey
		ON Survey.svyCensusDate >= E.estpActiveDate
		AND (Survey.svyCensusDate <= E.estpClosedDate or E.estpClosedDate is null)
	LEFT JOIN TeacherAppointment TA
		ON TA.estpNo = E.estpNo
		AND Survey.svyCensusDate >= TA.taDate
		AND (Survey.svyCensusDate <= TA.taEndDate or TA.taEndDate is null)
	GROUP BY #sch.schNo, estpRoleGrade, svyYEar


declare @XML xml

Select @XML =
(
Select
	Tag
	, Parent
	, schNo as [PlanningSheet!1!schoolNo]
	, schName as [PlanningSheet!1!schoolName]
	, LastSurveyYear as [PlanningSheet!1!lastSurvey]
	, LastBuildingReview as [PlanningSheet!1!lastBuildingReview]
	, SchNo as [Enrolments!2!!Hide]
	, plYear as [Enrolment!3!year]
	, total as [Enrolment!3!enrol]
	, case Estimate when 1 then 1 else null end as [Enrolment!3!estimate]
	, LevelCode as [Level!4!code]
	, Enrol as [Level!4!enrol]
	, ScenarioID as [Projections!5!scenarioID]
	, ScenarioName as [Projections!5!scenarioName]
	, plYear as [Projection!6!year]
	, Total as [Projection!6!enrol]
	, LevelCode as [Level!7!code]
	, Enrol as [Level!7!enrol]
-- teacher quotas
	, ScenarioID as [TeacherQuotas!8!scenarioID]
	, plYear as [TeacherQuota!9!year]
	, total as [TeacherQuota!9!quota]
	, RoleGrade as [RoleGrade!10!code]
	, TeacherRole as [RoleGrade!10!role]
	, total as [RoleGrade!10!quota]
-- room completions
	, schNo as [RoomCompletions!11!!Hide]
	, plYear as [RoomCompletion!12!year]
	, RoomFunction as [RoomFunction!13!code]
	, added as [RoomFunction!13!added]
	,replaced as [RoomFunction!13!replaced]
	, schNo as [BuildingCounts!14!!Hide]
	, plYear as [BuildingCount!15!year]
	, total  as [BuildingCount!15!count]
	, RoomTotal as [BuildingCount!15!roomCount]
	, BuildingType as [BuildingType!16!code]
	, total as [BuildingType!16!count]
	, RoomFunction as [RoomFunction!17!code]
	, total as [RoomFunction!17!count]
-- establishment plans
	, schNo	as [EstablishmentPlans!18!!Hide]
	, plYear as [EstablishmentPlan!19!year]
	, total as [EstablishmentPlan!19!total]
	, RoleGrade as [RoleGrade!20!code]
	, TeacherRole as [RoleGrade!20!role]
	, total as [RoleGrade!20!total]
--establishments - from position counts
	, schNo	as [Establishments!21!!Hide]
	, plYear as [Establishment!22!year]
	, total as [Establishment!22!total]
	, filled as  [Establishment!22!filled]
	, RoleGrade as [RoleGrade!23!code]

	, total as [RoleGrade!23!total]
	, filled as [RoleGrade!23!filled]
-- building completions
	, schNo as [BuildingCompletions!24!!Hide]
	, plYear as [BuildingCompletion!25!year]
	, added as [BuildingCompletion!25!added]
	, replaced as [BuildingCompletion!25!lost]
	, BuildingType as [BuildingType!26!code]
	, added as [BuildingType!26!added]
	, replaced as [BuildingType!26!lost]


from #ps
order BY schNo,  Sorter, ScenarioID, plYear,levelCode, RoleGrade, RoomFunction, BuildingType, Tag
FOR XML ExPLICIT
)
-- put the wrapper around the whole lot - ensure there is only one top level element
--
declare @pl XML
Select @pl =
(
Select
	@scenarioID [@ScenarioID]
	, @XML
FOR XML Path('PlanningSheets')
)

Select @pl PlanningSheets

	DROP TABLE #ee
	DROP TABLE #sch
	DROP TABLE #ps
	DROP TABLE #rcomp
	DROP TABLE #bldcomp
	DROP TABLE #tmpBuild

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

