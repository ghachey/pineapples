SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 1 2009
-- Description:	delete a teacher survey record
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[deleteTeacherSurvey]
	-- Add the parameters for the stored procedure here
	@tchsID int = 0,
	@DeleteIdentity int = 0
WITH EXECUTE AS 'pineapples'	-- to allow teacher delete
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	begin transaction

	declare @tID int

	-- find the tID
	Select @tID = tID from TeacherSurvey
	WHERE tchsID = @tchsID
	-- get rid of any ClassTeacher links to this tchsID

	DELETE FROM ClassTeacher
		WHERE tchsID = @tchsID

	-- delete the teachersurvey
	DELETE from TeacherSurvey
	WHERE tchsID = @tchsID

	if (@DeleteIdentity = 1)
		Delete from TeacherIdentity
				WHERE tID = @tID
				and @TID not in (Select tID from TeacherSurvey)

	commit transaction
end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

