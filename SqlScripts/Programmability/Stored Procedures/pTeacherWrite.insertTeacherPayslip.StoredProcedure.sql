SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 7 2010	(revised)
-- Description:	insert a payslip record
-- note that if the payslip arguent is not null, it will be parsed
-- to get the XML by the trigger
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[insertTeacherPayslip]
	-- Add the parameters for the stored procedure here
	@PayrollNo nvarchar(20)
	, @PeriodStart datetime
	, @PeriodEnd datetime
	, @Title nvarchar(50)
	, @Level nvarchar(20)
	, @PositionNo nvarchar(20)
	, @PayPointCode nvarchar(10)
	, @PayPointDesc nvarchar(50)
	, @gross money
	, @Payslip nvarchar(max)
	, @NamePrefix nvarchar(20) = null
	, @NameGiven nvarchar(50) = null
	, @NameMiddle nvarchar(50) = null
	, @NameLast nvarchar(50) = null
	, @NameSuffix nvarchar(20) = null
	, @payslipXML xml = null
AS
BEGIN

begin try
	SET NOCOUNT ON;
	declare @tID int

	INSERT INTO PayPointCodes
		(payPtCode, payptDesc)
	SELECT @PayPointCode, @PayPointDesc
	WHERE @PayPointCode not in (Select payptCode from PayPointcodes)


	INSERT INTO TeacherPayslips
	( tpsPeriodStart
		, tpsPeriodEnd
		, tpsTitle
		, tpsSalaryPoint
		, tpsPosition
		, tpsGross
		, tpsPayslip
		, tpsPayPoint
		, tpsPayroll
		, tpsNamePRefix
		, tpsGiven
		, tpsMiddleNames
		, tpsSurname
		, tpsNameSuffix
		, tpsXML
	)
	VALUES(
	@PeriodStart
	, @PeriodEnd
	, @Title
	, @Level
	, @PositionNo
	, @gross
	, @Payslip
	, @PayPointCode
	, @PAyrollNo
	, @NamePrefix
	, @NameGiven
	, @NameMiddle
	, @NameLast
	, @NameSuffix
	, @payslipXML
	)
--	from ( Select @PayrollNo PayrollNo) PP LEFT JOIN TeacherIdentity
--	ON TeacherIDentity.tPayroll = PP.PayrollNo -- this used to get the tID
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

