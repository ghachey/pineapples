SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	Update 'water rating' question on SchoolSurvey
-- note that the main 'Water' table is stored as resources, this is a one-off question
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfWater]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Water block - not to be confused with Resource.Water
  <field name="Water">
      <field name="Rating">
        <value>4</value>
      </field>
    </field>
  this is updated directly onto the SchoolSurvey Table
*/
UPDATE schoolSurvey
	SET ssWaterRating = WaterRating
FROM OPENXML(@idoc, '/x:field',2)		-- base is the Housing Node
WITH
(
	WaterRating				int			'x:field[@name="Rating"]/x:value'
) X
WHERE SchoolSurvey.ssID = @SurveyID

END
GO

