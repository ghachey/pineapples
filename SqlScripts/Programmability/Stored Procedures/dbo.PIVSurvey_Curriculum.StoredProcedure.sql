SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Report on curriculum ,ed system, medium of instruction
-- from school survey -
-- principally used by somalia ( CS), curriculum CS SL PL
-- =============================================
CREATE PROCEDURE [dbo].[PIVSurvey_Curriculum]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) ,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	print 'Curriculum'
   Select *
   INTO #tmppivcols
   FROM PIVColsCurriculum

	exec dbo.PIVSurvey_EXEC

   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

