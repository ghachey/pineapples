SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 02 2009
-- Description:	Return the XML grid representing the resources
-- =============================================
CREATE PROCEDURE [dbo].[xmlResources]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = '',
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select R.*
into #R
from Resources R
		inner join SchoolSurvey S
			on R.ssId = S.ssID
	WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar
	order by resName, resSplit, resLevel


declare @xmlResult xml

set @xmlResult =
(
Select Tag
, Parent
, Category as [Resources!1!category]
, Location as [Resources!1!location]
, Item as [Resource!2!item]
, ClassLevel as [data!3!classLevel]
, available as [data!3!available]
, adequate as [data!3!adequate]
, condition as [data!3!condition]
, functioning as [data!3!functioning]
, material as [data!3!material]
, number as [data!3!number]
, qty as [data!3!quantity]
, note as [data!3!note]

from

(Select DISTINCT
	1 as Tag
	, null as Parent
	, resName as Category
	, resLocation as Location
	, null as Item
	, null as classLevel
	, null as available
	, null as adequate
	, null as condition
	, null as functioning
	, null as material
	, null as number
	, null as qty
	, null as note
	, null as roomID

	from #r

UNION ALL
Select DISTINCT
	2 as Tag
	, 1 as Parent
	, resName as Category
	, resLocation as Location
	, ressplit as Item
	, null as classLevel
	, null as available
	, null as adequate
	, null as condition
	, null as functioning
	, null as material
	, null as number
	, null as qty
	, null as note
	, null as roomID

	from #r

UNION ALL
Select
	3 as Tag
	, 2 as Parent
	, resName as Category
	, resLocation as Location
	, ressplit as Item
	, resLevel as classLevel
	, resAvail as available
	, resAdequate as adequate
	, resCondition as condition
	, resFunctioning as functioning
	, resMaterial as material
	, resNumber as number
	, resQty as qty
	, resNote as note
	, rmID as roomID

	from #r

) u
ORDER BY

  [Resources!1!Category]
, [Resources!1!Location]
, [Resource!2!Item]
, [data!3!classLevel]
, Tag

for xml explicit
)


select @xmlResult
drop table #R
END
GO
GRANT EXECUTE ON [dbo].[xmlResources] TO [pSchoolRead] AS [dbo]
GO

