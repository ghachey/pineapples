SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2017
-- Description:	accepts a set of school number and returns those that are not valid
-- the Xml is a collection of all the schools represented on any page of the data survey workbook
-- also reports on duplicates found on sheet named Schools or WASH
-- called from Controllers_Api/NdoeController/upload
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeValidateSchools]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	SET NOCOUNT ON;

	-- this is the structure of the xml argument:
	-- Note that Sheet_Name is taken from the @Sheet attribute on School_No, rather than the parent node @name
	-- this was found to be far more efficient in execution
/*
<?xml version="1.0"?>
<Schools>
	<Sheet name="Students">
		<School_No Sheet="Students" Name="My School Name" Index="0">KSA201</School_No>
		<School_No Sheet="Students" Name="My School Name" Index="1">KSA201</School_No>
		<School_No Sheet="Students" Name="My School Name" Index="0">KSA201</School_No>
		<School_No Sheet="Students" Name="My School Name" Index="1">KSA201</School_No>
		<School_No Sheet="Students" Name="My School Name" Index="2">KSA201</School_No>
	</Sheet>
	<Sheet name="WASH">
		<School_No Sheet="WASH" Name="My School Name" Index="0">KSA201</School_No>
		<School_No Sheet="WASH" Name="My School Name 2" Index="1">KSA203</School_No>
	</Sheet>
</Schools>
*/

	-- collect all school nos that appear on any sheet
	declare @all table
	(
		School_No nvarchar(50)
		, Sheet_Name nvarchar(50)
		, rowID int
		,  School_Name nvarchar(200)
	)
	declare @validations TABLE
	(
		School_No nvarchar(50)
		, Sheet_Name nvarchar(50)
		, School_Name nvarchar(200)
		, NumRows int
		, FirstRow int
		, LastRow int
		, valmsg nvarchar(200)
	)

	INSERT INTO @all
	Select
		nullif(v.value('.', 'nvarchar(50)'),'') School_No
		--, null
		--, null
		, v.value('@Sheet', 'nvarchar(50)') Sheet_Name
		, v.value('@Index', 'int') rowID
		, v.value('@Name', 'nvarchar(200)') School_Name
	FROM @xml.nodes('/Schools/Sheet/School_No') as V(v)

	-- bit kludgey - we are allowed to have the PNIDOE on the staff page
	-- FSM specific rule
	DELETE FROM @all
	WHERE Sheet_Name = 'SchoolStaff'
	AND School_No like '%%%DOE'


-- not supplied  or not valid

	INSERT INTO @validations
	Select
	isnull(School_No,'(blank)') School_No
	, Sheet_Name
	, School_Name
	, count(rowID) NumRows
	, min(rowID) FirstRow
	, max(rowID)LastRow
	, case when School_Name is Null then 'No school selected' else 'Unknown school' end valmsg

	from @all A
		LEFT JOIN Schools S
			ON A.School_No= S.schNo
	WHERE S.schNo is null
	GROUP BY School_No, Sheet_Name, School_Name


-- duplicates
	INSERT INTO @validations
	Select
	isnull(School_No,'(blank)') School_No
	, Sheet_Name
	, School_Name
	, count(rowID) NumRows
	, min(rowID) FirstRow
	, max(rowID) LastRow
	, 'Duplicate records for this school'
	from @all A
	WHERE Sheet_Name not in ('SchoolStaff','Students')
	GROUP BY School_No, Sheet_Name, School_Name
	HAVING count(rowID) > 1


	Select * from @validations
	ORDER BY Sheet_Name, FirstRow

END
GO

