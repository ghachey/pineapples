SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of student data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeStudents]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50)
, @ValidationMode int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

------	select 1/0

DECLARE @ndoe TABLE
(
RowIndex int
-- Start Generated Code
	          , SchoolYear                   nvarchar(100) NULL
              , [State]                      nvarchar(100) NULL
              , School_Name                nvarchar(100) NULL
              , School_No                  nvarchar(100) NULL
              , School_Type                nvarchar(100) NULL
              , National_Student_ID        nvarchar(100) NULL
              , First_Name                 nvarchar(100) NULL
              , Middle_Name                nvarchar(100) NULL
              , Last_Name                  nvarchar(100) NULL
              , Full_Name                  nvarchar(100) NULL
              , Gender                     nvarchar(100) NULL
              , inDate_of_Birth            nvarchar(100) NULL
              , inDoB_Estimate             nvarchar(50) NULL
              , Age                        int NULL
              , Citizenship                nvarchar(100) NULL
              , Ethnicity                  nvarchar(100) NULL
              , Grade_Level                nvarchar(100) NULL
              , [From]                       nvarchar(100) NULL
              , Transferred_From_which_school   nvarchar(100) NULL
              , inTransfer_In_Date         nvarchar(100) NULL
              , SpEd_Student               nvarchar(100) NULL
			  , [IDEA_SA_ECE]				nvarchar(100) NULL
			  , [IDEA_SA_School_Age]		nvarchar(100) NULL
			  , Disability					nvarchar(100) NULL
			  , English_Learner				nvarchar(100) NULL
              , Full_Name_2                nvarchar(100) NULL
              , Days_Absent                int NULL
              , Completed                 nvarchar(100) NULL
              , Outcome                    nvarchar(100) NULL
              , Dropout_Reason             nvarchar(100) NULL
              , Expulsion_Reason           nvarchar(100) NULL
              , Post_secondary_study       nvarchar(100) NULL
-- END generated Code
, fileReference		uniqueidentifier
, studentID uniqueidentifier		-- search in the Student_ table

-- put some normalised values in here
, clsLevel nvarchar(10)			-- Grade Level is the grade name, translate back to code
, yearofEd	int					-- the realted year of ed to the class - to get the right value for SpEdEnv
, tfrSchNo nvarchar(50)			-- school number transferred from Transferred_From_which_school is the school name

, spDisability	 nvarchar(20)	-- special ed - disability code
, spEnv			 nvarchar(20)	-- special ed environment
, spEnglish		nvarchar(10)	-- special ed english learner

, ssID		int


-- cleanups
, Date_of_Birth          date NULL
, DoB_Estimate			 int			-- convert this from No Yes to 1 or null
, Transfer_In_Date		 date NULL
)

--- validations table
declare @val TABLE
(

	errorValue nvarchar(100)
	, valMsg nvarchar(200)
	, NumRows int
	, FirstRow int
	, LastRow int

)

--- warnings thable - these are mismatches on NationalID numbers
---- between current and exisitng data
--- they may be of two kinds :
--- the IDs match, but the data does not
--- the data matches (first name,last name,  DoB gender) but the IDs are different
declare @warnings TABLE
(
	rowID int,
	Id	nvarchar(50),
	givenName nvarchar(100),
	lastName  nvarchar(100),
	dob		  date,
	dobEstimate int,
	registerId	nvarchar(50),
	registerGivenName nvarchar(100),
	registerLastName  nvarchar(100),
	registerDob		  date,
	registerDobEstimate int
)


declare @SurveyYear int
declare @districtName nvarchar(50)
declare @firstRow int

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8
-- startpos = 8

Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
, @firstRow = v.value('@FirstRow', 'int')
From @xml.nodes('ListObject') as V(v)


Select @districtID = dID
from Districts WHERE dName = @districtName

print @SurveyYear
print @DistrictID

--INSERT INTO NdoeStudents_
INSERT INTO @ndoe
Select
v.value('@Index', 'int') [ RowIndex]
-- Start Gendereted Code -- a macro in the workbook can generate this from column names
, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
, nullif(ltrim(v.value('@State', 'nvarchar(100)')),'')											[State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
, nullif(ltrim(v.value('@School_Type', 'nvarchar(100)')),'')                                      [School_Type]
, nullif(ltrim(v.value('@National_Student_ID', 'nvarchar(100)')),'')                              [National_Student_ID]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                                       [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                                      [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                                        [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                                        [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')											[Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')								[Date_of_Birth]
, nullif(ltrim(v.value('@DoB_Estimate', 'nvarchar(50)')),'')												[DoB_Estimate]
, nullif(ltrim(v.value('@Age', 'int')),'')														  [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                                      [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                                        [Ethnicity]
, nullif(ltrim(v.value('@Grade_Level', 'nvarchar(100)')),'')                                      [Grade_Level]
, nullif(ltrim(v.value('@From', 'nvarchar(100)')),'')                               [From]
, nullif(ltrim(v.value('@Transferred_From_which_school', 'nvarchar(100)')),'')                                  [Transferred_From_which_school]
, nullif(ltrim(v.value('@Transfer_In_Date', 'nvarchar(100)')),'')                                 [Transfer_In_Date]
, nullif(ltrim(v.value('@SpEd_Student', 'nvarchar(100)')),'')                                     [SpEd_Student]

, nullif(ltrim(v.value('@IDEA_SA_ECE', 'nvarchar(100)')),'')                                    [IDEA_SA_ECE]
, nullif(ltrim(v.value('@IDEA_SA_School_Age', 'nvarchar(100)')),'')                             [IDEA_SA_School_Age]
, nullif(ltrim(v.value('@Disability', 'nvarchar(100)')),'')										 [Disability]
, nullif(ltrim(v.value('@English_Learner', 'nvarchar(100)')),'')								  [English_Learner]

, nullif(ltrim(v.value('@Full_Name_2', 'nvarchar(100)')),'')                                      [Full_Name_2]
, nullif(ltrim(v.value('@Days_Absent', 'nvarchar(100)')),'')                                      [Days_Absent]
, nullif(ltrim(v.value('@Completed', 'nvarchar(100)')),'')                                        [Completed]
, nullif(ltrim(v.value('@Outcome', 'nvarchar(100)')),'')                                          [Outcome]
, nullif(ltrim(v.value('@Dropout_Reason', 'nvarchar(100)')),'')                                   [Dropout_Reason]
, nullif(ltrim(v.value('@Expulsion_Reason', 'nvarchar(100)')),'')                                 [Expulsion_Reason]
, nullif(ltrim(v.value('@Post_secondary_study', 'nvarchar(100)')),'')                             [Post_secondary_study]
-- End Generated Code
, @fileReference
, null	-- the student Id - set to null first, it will get filled by matches where possible, then any remaining unmatched get a NEWID()
-- placeholders for the normalised fields
, null			-- class level; from lkp levels based on class name
, null			-- year of ed
, null			-- transfer school; from schools based on name

, null			-- sp disability
, null			-- sp environment
, null			-- sp english learner
, null			-- ssID from SchoolSurvey based on school no and year
-- cleanups
, null				-- Date_Of_Birth
, null				---Dob_Estimate
, null				-- Transfer_In_Date
from @xml.nodes('ListObject/row') as V(v)


-- poulate the normalised values
-- normalise school names for transfer schools
UPDATE @ndoe
SET tfrSchNo = S.schNo
FROM @ndoe
LEFT JOIN Schools S
	ON [@ndoe].Transferred_From_which_school in (S.schNo, S.schName)


-- date values need to be cleaned up depending  on whether they are presented as a string or number
UPDATE @ndoe
SET Date_Of_Birth =
	case ISNUMERIC(inDate_Of_Birth)
		when 0 then	case isdate(inDate_Of_Birth) when 1 then convert(date, inDate_Of_Birth) else null end
		when 1 then cast( convert(float, inDate_Of_Birth) - 2 as datetime)
	end
, DoB_Estimate =
	case inDoB_Estimate
		when 'Y' then 1
		when 'Yes' then 1
		when '1' then 1
		when 'unknown' then 1			-- allow for some confirmation we really don;t know the DoB
		else null end
, Transfer_In_Date =
	case ISNUMERIC(inTransfer_In_Date)
		when 0 then	case  isdate(inTransfer_In_Date) when 1 then convert(date, inTransfer_In_Date) else null end
		when 1 then cast( convert(float, inTransfer_In_Date) - 2 as datetime)
	end

-- normalise grade levels
UPDATE @ndoe
SET clsLevel = codeCode
, yearofEd = lvlYear
FROM @ndoe
LEFT JOIN lkpLEvels L
	ON [@ndoe].Grade_Level = L.codeDescription

print 'before gender'

-- normalise Gender
UPDATE @ndoe
SET Gender = case when codeCode is null then  Gender else codeCode end		-- avoid isnull becuase it will case as nvarchar(1)
FROM @ndoe
LEFT JOIN lkpGender G
	ON [@ndoe].Gender = G.codeDescription

-- normalise ethnicity
UPDATE @ndoe
SET ethnicity = codeCode
FROM @ndoe
LEFT JOIN lkpEthnicity E
	ON [@ndoe].Ethnicity = E.codeDescription

--- nrmalisations for special ed codes
update @ndoe
SET spDisability = codeCode
FROM @ndoe
	INNER JOIN lkpDisabilities D
		ON [@ndoe].Disability = D.codeDescription

-- special ed environment -- chose between ECE and school age fields, based on the pupil's grade
update @ndoe
SET spEnv = codeCode
FROM @ndoe
	INNER JOIN lkpEdEnvironment ENV
		ON
	case when yearofed = 0 then coalesce(IDEA_SA_ECE, IDEA_SA_School_Age)  else coalesce(IDEA_SA_School_Age, IDEA_SA_ECE) end
	 = ENV.codeDescription

-- special ed environment -- chose between ECE and school age fields, based on the pupil's grade
update @ndoe
SET spEnglish = codeCode
FROM @ndoe
	INNER JOIN lkpEnglishLearner E
		ON
		[@ndoe].English_Learner	 = E.codeDescription

-------------------------------------------------------------------------------------------
-- Validations on the input data - the user can abort and correct it in the source document

declare @missingSchool int
declare @missingGrade int
declare @missingDoB int
declare @missingNatID int

declare @UnknownSchool int
declare @badStateSchool int

declare @unknownGrade int
declare @missingName int
declare @missingTransferSchool int


-- Validation 1) school must be supplied on every record
print 'start of validations'

SELECT @missingSchool = count(*)
FROM @ndoe
WHERE School_No is null

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
null
, 'No school'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE School_Name is null
HAVING count(*) > 0

-- it must be a valid school code
SELECT @unknownSchool = count(DISTINCT School_No)
from @ndoe
WHERE School_No not in (Select schNo from Schools)


INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
School_No
, 'unknown school no'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE School_No not in (Select schNo from Schools)
and School_No is not null
Group BY School_No
HAVING count(*) > 0


-- it must be in the nominated state
SELECT @badStateSchool = count(DISTINCT School_No)
from @ndoe
WHERE School_No not in
(Select schNo from Schools
 INNER JOIN Islands I
 ON Schools.iCode = I.iCode
 	WHERE I.iGroup <> @districtID
	)

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
School_No
, 'School not in state'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE
School_No not in
	(Select schNo from Schools
	 INNER JOIN Islands I
	 ON Schools.iCode = I.iCode
 		WHERE I.iGroup = @districtID
		)
-- allow for a full national upload by setting the workbook district to 'ALL'
and @districtID <> 'ALL'
and School_No in (Select schNo from Schools)
GROUP BY School_No
HAVING count(*) > 0

-------

-- Validation 2) grade must be supplied on every record

SELECT @missingGrade = count(*)
FROM @ndoe
WHERE Grade_Level is null


INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
null
, 'no grade level'
, count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoe
WHERE Grade_Level is null
HAVING count(*) > 0


-- it must be a defined  grade level

Select @unknownGrade = COUNT(DISTINCT Grade_Level)
FROM @ndoe
WHERE Grade_Level not in (Select codeDescription from lkpLevels)

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
Grade_Level
, 'unknown grade level'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE  Grade_Level not in (Select codeDescription from lkpLevels)
GROUP BY Grade_Level
HAVING count(*) > 0

-- Validation 3 date of birth must be on every record (even if there is a estimate)
print 'check DoB'

SELECT @missingDoB = count(*)
FROM @ndoe
WHERE Date_of_Birth is null and
( DoB_Estimate is null)							-- in other words, we can allow a null DoB if it is marked as an estimate

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
null
, 'no date of birth'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Date_of_Birth is null and
( DoB_Estimate is null)
HAVING count(*) > 0

-- now any specific date of birth values that are not null, but not valid dates
-- check for malformed dates
INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
inDate_Of_Birth
, 'invalid date of birth'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inDate_Of_Birth is not null
AND isnumeric(inDate_Of_Birth) = 0
AND isdate(inDate_Of_Birth) = 0

-- check for time part
INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
 cast(convert(float, inDate_Of_Birth) - 2 as datetime)
, 'date of birth must not include time'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inDate_Of_Birth is not null
AND isnumeric(inDate_Of_Birth) = 1
AND (inDate_Of_Birth - floor(inDate_Of_Birth)) <> 0


INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
inTransfer_In_Date
, 'invalid transfer date'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inTransfer_In_Date is not null
AND isnumeric(inTransfer_In_Date) = 0
AND isdate(inTransfer_In_Date) = 0

-- check for time part
INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
 cast(convert(float, inTransfer_In_Date) - 2 as datetime)
, 'transfer date must not include time'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inTransfer_In_Date is not null
AND isnumeric(inTransfer_In_Date) = 1
AND (inTransfer_In_Date - floor(inTransfer_In_Date)) <> 0


-- Validation 4 missing national student ID
print 'check national id'

SELECT @missingNatID = count(*)
FROM @ndoe
WHERE National_Student_ID is null

-- Validation 5 some part of name
print 'check name'
SELECT @missingName = count(*)
FROM @ndoe
WHERE First_Name is null or Last_Name is null

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
NULL
, 'incomplete name'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE First_Name is null or Last_Name is null
HAVING count(*) > 0

print 'check gender'
-- Validation 6 -- gender must be supplied and valid
INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
null
, 'missing gender'
, count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoe
WHERE Gender is null
HAVING count(*) > 0

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
Gender
, 'unknown gender value'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Gender is not null and Gender not in (select codeCode from lkpGender)
GROUP BY Gender
HAVING count(*) > 0

print 'check missing ID'

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
null
, 'no Student ID number'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE National_Student_ID is null
HAVING count(*) > 0

-- Validation 6 Student Id must be unique in the input file
print 'check duplicate ID'
INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
National_Student_ID
, 'duplicate Student ID'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE National_Student_ID is not null
GROUP BY National_Student_ID
having count(*) > 1

-- Validation 7 transfer school must be valid if it is supplied, but if its not there, we can;t do much
print 'check transfer school'

SELECT @missingTransferSchool = count(DISTINCT Transferred_From_which_school)
FROM @ndoe
WHERE [From] = 'Transfer In' and Transferred_From_which_school is not null
and tfrSchNo is null

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
Transferred_From_which_school
, 'Transfer origin school not known'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE [From] = 'Transfer In' and Transferred_From_which_school is not null
and tfrSchNo is null
GROUP BY Transferred_From_which_school


-- check lengths of a few fields, note this is temporary, these should not be freely editable
INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
Dropout_reason
, 'dropout reason > 50 chars: ' + convert(nvarchar(2), len(dropout_reason))
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE len(Dropout_reason) > 50
GROUP BY Dropout_reason

INSERT INTO @val
(
	errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
Expulsion_reason
, 'expulsion reason > 50 chars: ' + convert(nvarchar(2), len(expulsion_reason))
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE len(expulsion_reason) > 50
GROUP BY expulsion_reason

--- Return the collection of errors
declare @NumValidationErrors int
print 'Report Validation errors'
select
	errorValue
	, valMsg
	, NumRows
	, FirstRow + @firstRow FirstRow
	, LastRow + @FirstRow LastRow
from @val V
ORDER BY V.FirstRow, V.valMsg

Select @NumValidationErrors = @@ROWCOUNT
print @NumValidationErrors
-- Return any warnings about ID numbers

print 'collect warnings'
-------------------------------------------------------
INSERT INTO @warnings
(
	rowID
	, Id
	, givenName
	, lastName
	, dob
	, dobEstimate
	, registerId
	, registerGivenName
	, registerLastName
	, registerDob
	, registerDobEstimate
)
SELECT NDOE.rowIndex
, stuCardID
, stuGiven
, stuFamilyName
, stuDoB
, stuDoBEst
, NDOE.National_Student_ID
, NDOE.First_Name
, NDOE.Last_Name
, NDOE.Date_of_Birth
, NDOE.DoB_Estimate

FROM Student_ STU
INNER JOIN @ndoe NDOE
ON NDOE.National_Student_ID = STU.stuCardID
WHERE (
	NDOE.Last_Name <> STU.stuFamilyName
	OR NDOE.Gender <> STU.stuGender
	OR
		( NDOE.Date_of_Birth <> STU.stuDoB
			AND isnull(NDOE.DoB_Estimate,0) = 0
			AND STU.stuDoBEst = 0
		)
)

print 'report warnings'

Select *
from @warnings

----- IF THERE ARE VALIDATION ERRORS LOGGED RETURN HERE
----- the data will not be processed
if (@NumValidationErrors > 0 or @ValidationMode = 1) return

------ Validation 6 - report any matches on family name, given name, DoB that have variant National ID
----SELECT stuGiven
----, stuFamilyName
----, stuDoB
----, stuGender
----FROM Student STU
----INNER JOIN @ndoe NDOE
----ON NDOE.First_Name = STU.stuGiven
----AND NDOE.Last_Name = STU.stuFamilyName
----AND NDOE.Gender = STU.stuGender
----AND NDOE.Date_of_Birth = STU.stuDoB

------ Validation 7 : Matcing National IDs but some difference on name or DoB


---------------------------------------------------------
----- PROCESSING --------
---------------------------------------------------------
-- first clean up:
-- remove any enrolments for @SurveyYear for any of the schools represented in this file
 -- clear any existing entries
 print 'Deleting student enrolments'
 DELETE FROM StudentEnrolment_
 WHERE schNo in (Select school_No from @ndoe)  -- issue #648
 AND stueYear = @SurveyYear

 print @@Rowcount

 print 'Deleting students'
 -- now remove any Student_ records that have no enrolment - this allows us to recreate those records
 DELETE from Student_
 WHERE stuID not in (select stuId from StudentEnrolment_)
 print @@Rowcount


-- Student Level records
-- How to identify an exisitng student record:
-- Must match on National ID, PLUS 2 of Last_Name DoB School of Enrolment ( this year same as last)

-- Matchng on dob and Gender - otherwise we get incorrect enrolment totals if previous versions are used

-- StudentId family name

-- family name given name school (update student ID)

-- family name given name school transfer

-- Not matching on DOB - if everything else matches, and the Student_ record says DOB is EStimate,
--  use the Student and update the Dob

--- TEST 1 - perfect match: on student ID, DoB and gender, (family name OR reversed name )
----
UPDATE @ndoe
SET studentID = STU.stuID
FROM @ndoe
INNER JOIN Student_ STU
	ON [@ndoe].National_Student_ID = STU.stuCardID
	AND (
		[@ndoe].Last_Name = STU.stuFamilyName
		OR (
			[@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName
		)
	)
	AND	[@ndoe].Date_of_Birth = STU.stuDoB
	AND [@ndoe].Gender = STU.stuGender
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)

-- not matching on student ID - but full name or reverse full name and the current school
UPDATE @ndoe
SET studentID = STU.stuID
FROM @ndoe
INNER JOIN Student_ STU
	ON (
		( [@ndoe].First_Name = STU.stuGiven AND [@ndoe].Last_Name = STU.stuFamilyName)
		OR
		 ([@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName)
	)
	AND	[@ndoe].Date_of_Birth = STU.stuDoB
	AND [@ndoe].Gender = STU.stuGender

INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID

WHERE
	[@ndoe].studentID is null
	AND STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].School_No
	AND ( [@ndoe].[From] is null or [@ndoe].[From] in ('ECE', 'REP'))
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)

-- not matching student id - matching dob gender name and transfer school
UPDATE @ndoe
SET studentID = STU.stuID
FROM @ndoe
INNER JOIN Student_ STU
	ON
	(
		( [@ndoe].First_Name = STU.stuGiven AND [@ndoe].Last_Name = STU.stuFamilyName)
		OR
		 ([@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName)
	)
	AND	[@ndoe].Date_of_Birth = STU.stuDoB
	AND [@ndoe].Gender = STU.stuGender

INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID
WHERE
	[@ndoe].studentID is null
	AND STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].tfrSchNo
	AND (  [@ndoe].[From] in ('Transfer In'))
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)

--- LAST CASE not matching DoB - but DoB is marked as Estimated orginally, but not estimated in input
--- and everything else matches (first name, last name, student ID gender, school)
--- In this event, we can update the DoB to the new DoB, and set DoBEstimate = 0
UPDATE @ndoe
SET studentID = STU.stuID
FROM @ndoe
INNER JOIN Student_ STU
	ON [@ndoe].National_Student_ID = STU.stuCardID
	AND
	(
		( [@ndoe].First_Name = STU.stuGiven AND [@ndoe].Last_Name = STU.stuFamilyName)
		OR
		 ([@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName)
	)
	AND	STU.stuDoBest = 1 and [@ndoe].DoB_Estimate is null
	AND [@ndoe].Gender = STU.stuGender

INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID

WHERE
	[@ndoe].studentID is null
	AND STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].School_No
	AND ( [@ndoe].[From] is null or [@ndoe].[From] in ('ECE', 'REP'))
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)


 -- now any retireved identities are on @ndoe
 -- other records not matched
 print 'update student'
 UPDATE Student_
 SET stuEthnicity = ndoe.Ethnicity
 , stuCardID = ndoe.National_Student_ID			-- we may update the student ID if we had a string enough match on everything else
 , stuDoB = ndoe.Date_of_Birth					-- we may be updating an estimate
 , stuDobEst =
	case DoB_Estimate when null then null
		when 1 then stuDobEst				-- ie the estimate flag is unchanged if the incoming data shouws estimate
		else null end
 , stuEditFileRef = @filereference
 FROM Student_
 INNER JOIN  @ndoe ndoe
 ON ndoe.studentID = Student_.stuID
 print @@rowcount
 ---populate the reamining studentIDs now with new guids

 print 'Assign guids for unknown students'
 UPDATE @ndoe
 SET studentID = newID()
 WHERE studentID is null
 print @@rowcount
 -- Now insert any unidientified records
 print 'insert student'
 INSERT INTO Student_
 (
 stuID
 , stuGiven
 , stuFamilyName
 , stuDoB
 , stuDobEst
 , stuGender
 , stuCardID
 , stuCreateFileRef
 , stuEditFileRef
 )
 Select
 StudentID
 , First_Name
 , Last_Name
 , Date_of_Birth
 , case DoB_Estimate when null then null when 1 then 1 else null end
 , left(Gender,1)
 , National_Student_ID
 , @fileReference
 , @filereference
 FROM @ndoe ndoe
 WHERE ndoe.studentID not in (Select stuID from Student_)
 print @@rowcount
 -- Populate the StudentEnrolment_ records

 print 'insert student enrolment'
 INSERT INTO StudentEnrolment_
(
    stuID
    ,schNo
    ,stueYear
    ,stueClass
    ,stueFrom
    ,stueFromSchool
    ,stueFromDate

    ,stueSpEd
	, stueSpEdEnv
	, stueSpEdDisability
	, stueSpEdEnglish

    ,stueDaysAbsent
    ,stueCompleted
    ,stueOutcome
    ,stueOutcomeReason
	,stueCreateFileRef
	,stueCreateFileRow
)
SELECT
StudentID
, School_No
, @SurveyYear
, clsLevel
, case [From] when 'Repeater' then 'REP'
				when 'Transferred In' then 'TRIN'
				when 'ECE' then 'ECE' end
, tfrSchNo
, Transfer_In_Date
, case SpEd_Student when 'No' then 0 when 'Yes' then 1 else null end
-- get the correct value for spEd Env from the ECE and school age versions - only one should be not null
, spEnv
, spDisability
, spEnglish

, Days_Absent
, case Completed when 'No' then 'N' when 'Yes' then 'Y' else null end
, Outcome
, case outcome
	when 'Expelled' then coalesce(expulsion_reason, dropout_reason)
	else coalesce(dropout_reason, expulsion_reason)
  end
, @filereference
, RowIndex
FROM @ndoe
print @@rowcount
-- create any records required in SchoolSurvey
-- but they should not be needed if we have already processed Schools
INSERT INTO SchoolSurvey
(
svyYear
, schNo
, ssSchType
, ssAuth
, ssElectN
, ssElectL
, ssSource
)
Select DISTINCT @SurveyYear
, NDOE.School_No
, schType
, schAuth
, schElectN
, schElectL
, @filereference

FROM @ndoe NDOE
INNER JOIN Schools S
	ON NDOE.School_No = S.schNo
LEFT JOIN SchoolSurvey SS
	ON NDOE.School_No = SS.schNo
	AND @SurveyYear = SS.svyYear
WHERE SS.ssID is null

-- for convenience, put the ssID back on the @ndoe table

UPDATE @ndoe
SET ssID = SS.ssID
FROM @ndoe
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoe].School_No
		AND SS.svyYear = @SurveyYear

------------------------------------------------------
--- roll up the totals in Enrollments and PupilTables
--- from the StudentEnrolment_ records

-- refactored into a separate sp - 20 4 2019

exec dbo.ndoeStudentEnrolmentRollup @SurveyYear

---------- end of processing --------------------------

-- finally return a pupil table of the data we have
-- build a pupil table structure

-- RETURN ENROLMENTS

-- recordset 1: identifying data
Select 'enrolments' tableName
, @SurveyYear surveyYear

-- 2 the row code set -- in this case the school nos

SElect DISTINCT isnull(schNo,'<>') codeCode
, isnull(schNo,'<>') codeDescription
FROM StudentEnrolment STUE
WHERE ssID in (Select ssId from @ndoe)
ORDER BY isnull(schNo,'<>')

---- 3 is the column definitions
--Select codeCode
--, codeDescription
--from lkpLevels
--ORDER BY lvlYear

Select codeCode
, codeDescription
FROM lkpLevels
ORDER BY lvlYear

-- 4 is the data

Select enLevel col
, isnull(schNo,'<>') row
, sum(enM) M
, sum(enF) F
FROM Enrollments E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
GROUP BY enLevel
, SchNo
ORDER BY SchNo
, enLevel


-- RETURN REPEATERS

-- recordset 1: identifying data
Select 'repeaters' tableName
, @SurveyYear surveyYear

-- recordset 2 , 3 the client will reuse the ones above

-- 4 repeater data

Select ptLevel col
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
AND ptCode = 'REP'
GROUP BY ptLevel
, SchNo
ORDER BY SchNo
, ptLevel


-- RETURN PSA

-- recordset 1: identifying data
Select 'psa' tableName
, @SurveyYear surveyYear

-- recordset 2 -- reuse from above schools
--
-- 3 - age range
SELECT num codeCode
, num codeDescription
FRom metaNumbers
WHERE num between 4 and 12
ORDER BY Num

-- 4 psa data

Select ptAge col
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
AND ptCode = 'PSA'
GROUP BY ptAge
, SchNo
ORDER BY SchNo
, ptAge


--- RETURN DIS

-- recordset 1: identifying data
Select 'dis' tableName
, @SurveyYear surveyYear

-- recordset 2 -- reuse from above

--
-- 3 - columns are dis codes
SELECT codeCode
, codeDescription
From lkpDisabilities
ORDER BY codeSeq

-- 4 disdata

Select ptRow col		-- disability code is in ptRow, even though we wnt a column orientation in this case
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
AND ptCode = 'DIS'
GROUP BY ptRow
, SchNo
ORDER BY SchNo
, ptRow


END
GO

