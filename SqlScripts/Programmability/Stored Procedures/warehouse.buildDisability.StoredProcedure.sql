SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 12 2017
-- Description:	Refresh the warehouse disability tables
-- tableDisability:
-- Disability
-- =============================================
CREATE PROCEDURE [warehouse].[buildDisability]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
---- school level flow model

	begin transaction


			-- disability table
			IF OBJECT_ID('warehouse.disability', 'U') IS NULL begin
				Select  G.*
					, D.codeDescription Disability
					INTO warehouse.Disability
					FROM
						measureDisabilityG G
					LEFT JOIN TRDisabilities D
						ON G.disCode = D.codeCode
					WHERE isnull(G.disab,0) <> 0

				print 'warehouse.Disability created - rows:' + convert(nvarchar(10), @@rowcount)


			end else begin
				DELETE
				from warehouse.disability
				WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
				print 'warehouse.Disability deletes - rows:' + convert(nvarchar(10), @@rowcount)

				INSERT INTO warehouse.Disability
				Select  G.*
					, D.codeDescription Disability
					FROM
						measureDisabilityG G
					LEFT JOIN TRDisabilities D
						ON G.disCode = D.codeCode
					WHERE isnull(G.disab,0) <> 0
					AND (SurveyYear >= @StartFromYear or @StartFromYEar is null)
					print 'warehouse.Disability inserts - rows:' + convert(nvarchar(10), @@rowcount)

			end

		IF OBJECT_ID('warehouse.tableDisability', 'U') IS NULL begin

			Select D.SurveyYear
				, D.Estimate
				, ClassLevel
				, GenderCode
				, DSS.[District Code] DistrictCode
				, [AuthorityCode]
				, [SchoolTypeCode]
				, DisCode
				, Disability
				, sum(Disab) Disab
				INTO warehouse.tableDisability
				from warehouse.disability D
				INNER JOIN warehouse.dimensionSchoolSurvey DSS
				ON D.surveyDimensionID = DSS.[Survey ID]

				GROUP BY
				D.SurveyYear
				, D.Estimate
				, ClassLevel
				, GenderCode
				, DSS.[District Code]
				, [AuthorityCode]
				, [SchoolTypeCode]
				, DisCode
				, Disability
					print 'warehouse.tableDisability created - rows:' + convert(nvarchar(10), @@rowcount)
			end else begin
				DELETE
					FROM warehouse.tableDisability
					WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
					print 'warehouse.tableDisability deletes - rows:' + convert(nvarchar(10), @@rowcount)

				INSERT INTO warehouse.tableDisability
					Select D.SurveyYear
					, D.Estimate
					, ClassLevel
					, GenderCode
					, DSS.[District Code] DistrictCode
					, [AuthorityCode]
					, [SchoolTypeCode]
					, DisCode
					, Disability
					, sum(Disab) Disab

					from warehouse.disability D
					INNER JOIN warehouse.dimensionSchoolSurvey DSS
					ON D.surveyDimensionID = DSS.[Survey ID]

					WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
					GROUP BY
					D.SurveyYear
					, D.Estimate
					, ClassLevel
					, GenderCode
					, DSS.[District Code]
					, [AuthorityCode]
					, [SchoolTypeCode]
					, DisCode
					, Disability
					print 'warehouse.tableDisability inserts - rows:' + convert(nvarchar(10), @@rowcount)

			end

	commit transaction

END
GO

