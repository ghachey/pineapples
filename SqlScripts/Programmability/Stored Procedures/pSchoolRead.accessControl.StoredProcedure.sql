SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 9 2018
-- Description:	check access control
-- =============================================
CREATE PROCEDURE [pSchoolRead].[accessControl]
	-- Add the parameters for the stored procedure here
	@schoolNo nvarchar(50)
	, @district nvarchar(10) = null
	, @authority nvarchar(20) = null
	, @userSchool nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @userSchool is not null begin
		if @userSchool <> @SchoolNo begin
			Raiserror('Forbidden', 16,1);
		end
		return
	end

	declare @s nvarchar(50)
	SELECT @s = schNo
	FROM Schools S
		INNER JOIN lkpIslands I
			ON S.iCode = I.iCode
	WHERE (schAuth = @authority or @authority is null)
		AND (I.iGroup = @district or @district is null)
		AND (schNo = @schoolNo)

	if @@ROWCOUNT = 0 begin
		Raiserror('Forbidden', 16,1);
	end

END
GO

