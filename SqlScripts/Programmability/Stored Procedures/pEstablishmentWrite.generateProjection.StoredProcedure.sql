SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-20
-- Description:	Generate school level enrolment projection, using rep and promotion rates
-- =============================================
CREATE PROCEDURE [pEstablishmentWrite].[generateProjection]
	-- Add the parameters for the stored procedure here
	@BaseYear int
	,@TargetYear int
	,@ScenarioID int
	,@SizeFactor float =1
	,@SchoolNo nvarchar(50) = null


AS
BEGIN

	SET NOCOUNT ON


	declare @efa table
	(
	schNo nvarchar(50),
	[Survey Year] int,
	surveyDimensionssID int,
	LevelCode nvarchar(10),
	YearOfEd int,
	EnrolM int,
	enrolF int,
	EnrolNYM int,
	EnrolNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepM int,
	RepF int,
	RepNYM int,
	RepNYF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate int,
	[Year of Data] int,
	[Age of Data] int,
	[Estimate NY] int,
	[Year of Data NY] int,
	 [Age of Data NY] int
	)


	INSERT INTO @efa
	exec sp_EFACore 0, @BaseYear


	-- next stpe is to create the Enrolment Projection records.
	-- there is one of these for each school

	-- remove any that are not required, unless they are frozen

	DELETE 	from EnrolmentProjection
	from EnrolmentProjection
		LEFT JOIN @efa EFA
		ON EnrolmentProjection.schNo = EFA.schNo
	WHERE
		EFA.schNo is null
	AND
		escnID = @ScenarioID
	AND epLocked = 0


	INSERT INTO EnrolmentProjection
	(escnID, epYear, schNo)
	Select DISTINCT
		@scenarioID
		, @TargetYear
		, efa.schNo
	from @efa EFA
	LEFT JOIN
	 (Select schNo from EnrolmentProjection
		WHERE epYear = @TargetYear AND escnID = @ScenarioID ) Locks
	ON EFA.schNo = Locks.schNo
	WHERE locks.schNo is null


	-- now we get to write the projectionData

	-- remove anything that is already there, expcet if locked

	DELETE from EnrolmentProjectionData
	from EnrolmentPRojectionData
		INNER JOIN EnrolmentProjection ON EnrolmentProjection.epID = EnrolmentProjectionData.epID
	WHERE
		escnID = @ScenarioID
		AND epYear = @TargetYear
		AND (schNo = @SchoolNo or @SchoolNo is null)
		AND (epLocked = 0)

	-- add them in, ignore locked
	INSERT INTO EnrolmentProjectionData
	(epID, epdLevel, epdU)
	Select epID
		, efa.levelCode
		, round((isnull(efa.enrolF,0)+isnull(efa.enrolM,0)) * @sizeFactor,0)
	from EnrolmentProjection EP
	INNER JOIN @efa EFA
		ON efa.schNo = EP.schNo
	WHERE EP.escnID = @ScenarioID
		AND epYear = @TargetYear
		AND epLocked = 0
		AND efa.[survey Year] = @BaseYear


END
GO

