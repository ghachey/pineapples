SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 10 2009
-- Description:	search for payslips
-- =============================================
-- date from and to :
-- if both supplied, the earliest payslip contains date from, and the last date to

CREATE PROCEDURE [pTeacherReadX].[PayslipsFilter]
	-- Add the parameters for the stored procedure here
	@PeriodFrom datetime  = null
	, @PeriodTo datetime = null
	, @Paypoint nvarchar(20) = null
	, @PositionNo nvarchar(20) = null
	, @PayrollNo nvarchar(20) = null
	, @SalaryPoint nvarchar(50) = null
	, @SalaryLevel nvarchar(5) = null
	, @GrossMin money = null
	, @GrossMax money = null
	, @GivenName nvarchar(50) = null
	, @Surname nvarchar(50) = null
	, @Unidentified bit = 0
	, @TeacherID int = null
	, @ITemType nvarchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- first offf, if there is a teacher id, convert it to a payroll no
--

declare @Switch bit
Select @Switch = 1

	if (@TeacherID is not null)
		begin
			Select @PayrollNo = null
			Select @PayrollNo = tPayroll
			from TeacherIdentity
			WHERE tID = @TeacherID

			-- if there is no payroll for this tID, return nothing

			if (@PayrollNo is null)
				Select @Switch = 0
		end

	Select P.tpsID
	, P.tpsGross
	, tpsPayroll
	, case when tPayroll is null then 1 else 0 end Unidentified
	INTO #finds
	from TeacherPayslips P
		LEFT JOIN lkpSalaryPoints SalP on SalP.spCode = P.tpsSalaryPoint
		LEFT JOIN (Select DISTINCT tPayroll from TeacherIdentity) TIP
			ON P.tpsPayroll = TIP.tPayroll
	WHERE
		(@Switch = 1)		-- just return nothing if teacherid supplied and no payroll no
		AND
		(@PeriodFrom is null
			or tpsPeriodStart <= @PeriodFrom
		)

		AND

		(@PeriodTo is null
			or tpsPeriodEnd >= @PeriodTo
		)

		AND (tpsPaypoint = @Paypoint or @Paypoint is null)
		AND (tpsPosition like @PositionNo or @PositionNo is null)
		AND (tpsPayroll = @PayrollNo or @PayrollNo is null)


		and (
				@Unidentified = 0  or
				tPayroll is null
			)

		AND (@GrossMin is null or  tpsGross >= @grossMin)
		AND (@grossMax is null or tpsGross <= @grossMax)

		AND (@SalaryPoint is null or tpsSalaryPoint = @SalaryPoint)

		AND (@SalaryLevel is null or SalP.salLevel = @SalaryLevel)

		AND (tpsGiven like @GivenName + '%' or @GivenName is null)

		AND (tpsSurname like @SurName + '%' or @SurName is null)

		AND (tpsXML.exist('/Payslip/ThisPay/Item[@pc=sql:variable("@ItemType")]') = 1
				OR @ItemType is null)

	ORDER BY tpsPeriodEnd DESC, tpsSurname, tpsGiven, tpsPayroll

	Select P.*
	, I.Unidentified
	from #finds I
		INNER JOIN TeacherPayslips P
			ON I.tpsID = P.tpsID


declare @TotUnidentified int


	Select count(tpsID) RecordsFound
	, sum(tpsGross) TotalGross
	, sum(Unidentified) Unidentified
	from
		#finds

	drop table #finds
END
GO

