SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 2 2010
-- Description:	Generate School Grants for a year/payment no
-- =============================================
CREATE PROCEDURE [pFinanceWriteX].[GenerateGrants]
	-- Add the parameters for the stored procedure here
	@GrantYear int,
	@PaymentNo int,
	@SchoolType nvarchar(10) = null,
	@Authority nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


begin try
	-- clear the recalc flag for the recalcs we are about to remove
/*
	PARAMETERS paramYear Short, paramPaymentNo Short, paramRecalced Bit;
*/

/*
Parent and extension options

For payment to parent school -
Parent - payable once if the parent school qualifies
Every - may be applied to every child of the parent and added
any - payable once if any child of the parent qualifies - (used only on fixed amounts)

direct - payable directly to the extension school

If actuals are required for the financial year
- every school involved in the calculation must have supplied a survey


- payment is made if EVERY extension has supplied a survey
- if any extension has not supplied a survey, payment is not made
*/
DECLARE @E TABLE
(
	schNo nvarchar(50)
	, Estimate int
	, secCode nvarchar(10)
	, edLevel nvarchar(10)
	, enrol int
)


DECLARE @B TABLE
(
	schNo nvarchar(50)
	, Estimate int
	, secCode nvarchar(10)
	, edLevel nvarchar(10)
	, boarders int
)


DECLARE @schList TABLE
(
	schNo nvarchar(50)
	, schType nvarchar(10)
	, schAuth nvarchar(10)
	, EstabPoint nvarchar(50)		-- = self if not an extension, parent for extensions
	, dID nvarchar(10)
	, Estimate int
)

-- this is the MAx of the Estimate across the Estab point
-- in other words, a school is trated as an estimate if any of its extensions are
DECLARE @EstimateStatus TABLE
(
	schNo nvarchar(50)
	, Estimate int
)
DECLARE @schoolRules TABLE
(
	SchNo nvarchar(50)
	, EstabPoint nvarchar(50)
	, grCode nvarchar(20)
	, grBoarder nvarchar(1)
	, grExtensionAction nchar(1)
)

DECLARE @tmpLines TABLE
(
	schNo nvarchar(50),
	estabPoint nvarchar(50),
	[grCode] nvarchar(20) NULL,
	grExtensionAction nchar(1),
	[secCode] nvarchar(3) NULL,
	edLevel nvarchar(10) NULL,
	[gvValue] [money] NULL,
	[Qty] [int] NULL,
	[Value] [money] NULL,
	[PaymentNo] [int] NULL,
	[Reverse] nvarchar(1) NULL,
	AuditClass nvarchar(20)
)


DECLARE @tmpLinesOut TABLE
(
	PaySchool nvarchar(50),				-- the school to pay
	SourceSchool nvarchar(50),			-- the source school
	[grCode] nvarchar(20) NULL,
	grExtensionAction nchar(1),
	[secCode] nvarchar(3) NULL,
	edLevel nvarchar(10) NULL,
	[gvValue] [money] NULL,
	[Qty] [int] NULL,
	[Value] [money] NULL,
	[PaymentNo] [int] NULL,
	[Reverse] nvarchar(1) NULL,
	AuditClass nvarchar(20)
)


--- Build Temp Tables
--- some business rules:
--- the school cannot be flagged as Dormant in the grant year
--- the school must be active for the grant year

-- first collect the enrolment data
SELECT schNo
	, Estimate
	, bestssID
	, surveyDimensionssID
INTO #ee
FROM
	 dbo.tfnESTIMATE_BEstSurveyEnrolments() BE
WHERE
	LifeYear = @GrantYear

-- the school type and authority


-- 7 7 2011 get the authority from schoolYearHistory in the grant year
--, or else from the school record

print 'build school list'

INSERT INTO @schList
(
	schNo
	, schType
	, schAuth
	, EstabPoint
	, dID
	, Estimate
)
SELECT S.schNo
-- we shouldn;t really have to rely on the school survey to get this
-- best practice should be - go from SchoolYearHistory and Schools only
	, isnull(SEst.systCode,isnull(ssSchType, S.schType))
	, isnull(Sest.syAuth, isnull(ssAuth, S.schAuth))
	-- the parent
	, isnull(syEstablishmentPoint, schEstablishmentPoint)
	, iGRoup
	, #ee.Estimate
FROM
	Schools S
	LEFT JOIN Islands
		ON S.iCode = Islands.iCode
	LEFT JOIN #ee
		ON S.schNo = #ee.schNo
	LEFT JOIN SchoolSurvey SS
		ON SS.ssID = #ee.SurveyDimensionssID
	LEFT JOIN SchoolYearHistory SEst
		ON S.schNo = SEst.schNo
		AND Sest.syYear = @GrantYear
WHERE schEst <= @GrantYear
	and (schClosed = 0 or @GrantYear < schClosed)
	and isnull(syDormant,0) = 0				-- not flagged dormant

	-- put this line in here to exclude any school that is already in School Grants
	-- ie is paid or on hold
	-- we do this so as not to double count an extension school that may have been paid as a				standalone and avoid bad things happening on changing extension status

	AND (S.schNo not in
			(Select sgiSource
				from schoolGrantItems SGI
					INNER JOIN SchoolGrant SG
						on SGI.sgID = SG.sgID
				WHERE
						sgYear = @GrantYear
						AND sgPaymentNo = @PaymentNo
						AND sgStatus is not null
			)
		)
		AND (isnull(SEST.systcode, S.schType) = @SchoolType or @SchoolType is null)
		AND (isnull(SESt.syAuth, S.schauth) = @Authority or @Authority is null)


-- next get the Estimate status across all children of a parent

INSERT INTO @Estimatestatus
(
	Schno
	, Estimate
)
SELECT
	estabPoint
	, max(Estimate)
FROM @SchList S
GROUP BY estabPoint


-- now the applicable rules
INSERT INTO @SchoolRules
Select SL.schNo
, SL.EstabPoint
, grCode
, grBoarder
, grExtensionAction
FROM  GrantRules
CROSS JOIN @SchList SL
LEFT JOIN ListSchools
	ON GrantRules.grLstName = Listschools.lstName
	AND SL.SchNo = listSchools.schNo

WHERE
		GrantRules.grRuleClass = 'S'			-- only school rules
		AND (SL.schType = GRantrules.grSchtype or GrantRules.grSchType is null)
		AND (SL.dID = GrantRules.grdID or GrantRules.grdID is null)
		AND
			(Grantrules.grLstName is null
			OR
				(
				SL.SchNo = listSchools.schNo
				AND (GrantRules.grLstValue = ListSchools.lstValue or GrantRules.grLstValue is null)
				)
			)


print 'Enrolments'

-- enrolment by sector
--- table is schNo Estimate SectorCode enrolment

INSERT INTO @E
Select BE.schNo
	, BE.Estimate
	, secCode
	, EL.codecode
	, sum(isnull(enM,0) + isnull(enF,0)) secEnrol
from #ee BE
	INNER JOIN Enrollments E
		ON BE.bestssID = E.ssID
	INNER JOIN lkpLevels
		on E.enLevel = lkpLEvels.codeCode
	INNER JOIN lkpEducationLevels EL
		ON lkpLevels.lvlYear between edlMinYear and edlMaxYear
GROUP BY schNo, BE.Estimate, secCode, El.codeCode


-- enrolment by education Level
-- SchNo Estimate EdLevelCode Enrolment


print 'boarders by sector'

-- boarders by sector
--SchNo Estimate SectorCode Boarders
INSERT INTO @B
Select BE.schNo
	, BE.Estimate
	, secCode
	, EL.codecode
	, sum(isnull(ptM,0) + isnull(ptF,0)) secBRD
from #EE BE
	INNER JOIN PupilTables BRD
		ON BE.bestssID = BRD.ssID
		AND ptCode = 'BRD'
	INNER JOIN lkpLevels
		on BRD.ptLevel = lkpLEvels.codeCode
	INNER JOIN lkpEducationLevels EL
		ON lkpLevels.lvlYear between edlMinYear and edlMaxYear
GROUP BY schNo, BE.Estimate, secCode, El.codeCode

--Select * from @B

print 'borders by ed level'


-- now insert the temp lines from the matching rules

INSERT INTO @tmpLines
(
	schNo
	, EstabPoint
	, grCode
	, grExtensionAction
	, secCode
	, edlevel
	, gvValue
	, Qty
	, Value
	, PaymentNo
	, Auditclass
)
Select
	SR.schNo
	, SR.EstabPoint
	, SR.grcode
	, SR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue
	, sum(E.enrol)
	, gvValue * sum(isnull(E.enrol,0))
	, @PaymentNo
	, 'enrol'
FROM  @Schoolrules SR
INNER JOIN GrantValues
	ON GrantValues.grCode = SR.grCode
INNER JOIN @E E
	ON SR.schNo = E.schNo
	AND (E.secCode = GrantValues.gvSector OR GrantValues.gvSector is null)
	AND (E.edLevel = GrantValues.gvEdLevel OR GrantValues.gvEdLevel is null)
WHERE GrantValues.gvYear = @GrantYear
	AND SR.grBoarder = 'E'
GROUP BY
	SR.schNo
	, SR.EstabPoint
	, SR.grcode
	, SR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue


INSERT INTO @tmpLines
(
	schNo
	, EstabPoint
	, grCode
	, grExtensionAction
	, secCode
	, edlevel
	, gvValue
	, Qty
	, Value
	, PaymentNo
	, Auditclass
)
Select
	SR.schNo
	, SR.EstabPoint
	, SR.grcode
	, SR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue
	, sum(B.boarders)
	, gvValue * sum(isnull(B.boarders,0))
	, @PaymentNo
	, 'boarders'
FROM  @Schoolrules SR
INNER JOIN GrantValues
	ON GrantValues.grCode = SR.grCode
	AND GrantValues.gvYear = @GrantYear
INNER JOIN @B B
	ON SR.schNo = B.schNo
	AND (B.secCode = GrantValues.gvSector OR GrantValues.gvSector is null)
	AND (B.edLevel = GrantValues.gvEdLevel OR GrantValues.gvEdLevel is null)
WHERE GrantValues.gvYear = @GrantYear
	AND SR.grBoarder = 'B'
GROUP BY
	SR.schNo
	, SR.EstabPoint
	, SR.grcode
	, SR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue

-- fixed amount

INSERT INTO @tmpLines
(
	schNo
	, EstabPoint
	, grCode
	, grExtensionAction
	, secCode
	, edlevel
	, gvValue
	, Qty
	, Value
	, PaymentNo
	, Auditclass
)
Select
	SR.schNo
	, SR.EstabPoint
	, SR.grcode
	, SR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue
	, 1
	, gvValue
	, @PaymentNo
	, 'Fixed'
FROM  @Schoolrules SR
INNER JOIN GrantValues
	ON GrantValues.grCode = SR.grCode
	AND GrantValues.gvYear = @GrantYear
-- the sector paremeter should be ignored on a fixed item
----INNER JOIN @SchList SL
----	ON (SL.secCode = GrantValues.gvSector OR GrantValues.gvSector is null)
----	AND (SL.edLevel = GrantValues.gvEdLevel OR GrantValues.gvEdLevel is null)
WHERE GrantValues.gvYear = @GrantYear
	AND SR.grBoarder is null


----- RECALC LOGIC
--- Reverse anything Estimated from Period 1
DECLARE @LinesToRecalc TABLE
(
	sgiIDRecalc int
)

INSERT INTO @LinesToRecalc
SELECT sgiID
FROM SchoolGrant SG
	INNER JOIN SchoolGrantItems SGI
		ON SG.sgID = SGI.sgID
	INNER JOIN @SchList S
		ON S.schNo = SGI.sgiSource
		-- the source school , it will get group later according to the parent or not
	INNER JOIN GrantRules GR
		ON GR.grCode = SGI.grCode
WHERE SG.sgYear = @GrantYear
	AND @PaymentNo = 2		-- after payment 1
	AND sgPaymentNo = 1		-- reverse payment 1 only
	AND SG.sgEstimate = 1	-- was estimated in payment 1
	AND GR.grBoarder is not null	-- rule based on enrol or boarders
	AND SG.sgStatus = 'Raised'		-- don;t bother to recalc if the payment has not yet been raised

INSERT INTO @TmpLines
(
	schNo
	, EstabPoint
	, grCode
	, grExtensionAction
	, secCode
	, edlevel
	, gvValue
	, Qty
	, Value
	, PaymentNo
	, [Reverse]
	, Auditclass
)
SELECT
	sgiSource
	, SG.schNo
	, SGI.grCode
	, 'P'			-- always force this to reverse from where it was before
	, secCode
	, EdLevelCode
	, SGI.grValue
	, SGI.sgiQty * -1
	, SGI.sgiValue * -1
	, 1			-- payment number getting reverses
	, 'R'
	,'R'
FROM SchoolGrant SG
	INNER JOIN SchoolGrantItems SGI
		ON SG.sgID = SGI.sgID
	INNER JOIN @SchList S
		ON S.schNo = SGI.sgiSource
		-- the source school , it will get group later according to the parent or not
	INNER JOIN GrantRules GR
		ON GR.grCode = SGI.grCode
WHERE sgiID in (Select sgiIDRecalc from @LinesToRecalc LR)


--- now the recalc of these lines
INSERT INTO @TmpLines
(
	schNo
	, EstabPoint
	, grCode
	, grExtensionAction
	, secCode
	, edlevel
	, gvValue
	, Qty
	, Value
	, PaymentNo
	, [Reverse]
	, Auditclass
)
SELECT
	sgiSource
	, SG.schNo
	, SGI.grCode
	, GR.grExtensionAction			-- post where it ought to go
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue
	, sum(E.enrol)
	, GrantValues.gvValue * sum(E.enrol)
	, 1			-- payment number getting reverses
	, 'A'
	,'R enrol'
FROM SchoolGrant SG
	INNER JOIN SchoolGrantItems SGI
		ON SG.sgID = SGI.sgID
	INNER JOIN @SchList S
		ON S.schNo = SGI.sgiSource
		-- the source school , it will get group later according to the parent or not
	INNER JOIN GrantRules GR
		ON GR.grCode = SGI.grCode
	INNER JOIN GrantValues
		ON GrantValues.grCode = GR.grCode
		AND (isnull(SGI.secCode,'') = isnull(GrantValues.gvSector,''))
		AND (isnull(SGI.edLevelCode,'') = isnull(GrantValues.gvEdLevel,''))

	INNER JOIN @E E
		ON S.schNo = E.schNo
	AND (E.secCode = SGI.secCode OR SGI.secCode is null)
	AND (E.edLevel = SGI.EdLevelCode OR SGI.edLevelCode is null)

WHERE sgiID in (Select sgiIDRecalc from @LinesToRecalc LR)
	AND GR.grBoarder =  'E'	-- rule based on enrol or boarders
	AND GrantValues.gvYear = @GrantYear
GROUP BY
	sgiSource
	, SG.schNo

	, SGI.grcode
	, GR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue


INSERT INTO @TmpLines
(
	schNo
	, EstabPoint
	, grCode
	, grExtensionAction
	, secCode
	, edlevel
	, gvValue
	, Qty
	, Value
	, PaymentNo
	, [Reverse]
	, Auditclass
)
SELECT
	sgiSource
	, SG.schNo
	, SGI.grCode
	, GR.grExtensionAction			-- post where it ought to go
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue
	, sum(B.boarders)
	, GrantValues.gvValue * sum(B.boarders)
	, 1			-- payment number getting reverses
	, 'A'
	,'R Boarders'
FROM SchoolGrant SG
	INNER JOIN SchoolGrantItems SGI
		ON SG.sgID = SGI.sgID
	INNER JOIN @SchList S
		ON S.schNo = SGI.sgiSource
		-- the source school , it will get group later according to the parent or not
	INNER JOIN GrantRules GR
		ON GR.grCode = SGI.grCode
	INNER JOIN GrantValues
		ON GrantValues.grCode = GR.grCode
		AND (isnull(SGI.secCode,'') = isnull(GrantValues.gvSector,''))
		AND (isnull(SGI.edLevelCode,'') = isnull(GrantValues.gvEdLevel,''))

	INNER JOIN @B B
		ON S.schNo = B.schNo
	AND (B.secCode = SGI.secCode OR SGI.secCode is null)
	AND (B.edLevel = SGI.EdLevelCode OR SGI.edLevelCode is null)

WHERE sgiID in (Select sgiIDRecalc from @LinesToRecalc LR)
	AND GR.grBoarder =  'B'	-- rule based on enrol or boarders
	AND GrantValues.gvYear = @GrantYear
GROUP BY
	sgiSource
	, SG.schNo

	, SGI.grcode
	, GR.grExtensionAction
	, GrantValues.gvSector
	, GrantValues.gvEdLevel
	, GrantValues.gvValue

Select * from @tmpLines T
--select * from #schList
--select * from #schEdLevelEnrol
--Select * from #schSectorEnrol

-- now apply the posting rules
-- 1) if payable DIRECT - push that through to the output

INSERT INTO @tmpLinesOut
(
	PaySchool
	, sourceSchool
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, Auditclass
)
SELECT schNo
	, schNo
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, Auditclass
FROM @tmpLines
WHERE grExtensionAction = 'D'		-- the school to pay is the school that was calculated

-- second case is sum is payable to the parent
INSERT INTO @tmpLinesOut
(
	PaySchool
	, sourceSchool
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, [Reverse]
	, Auditclass
)
SELECT EstabPoint
	, schNo
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, [Reverse]
	, Auditclass
FROM @tmpLines
WHERE grExtensionAction = 'E'		-- the school to pay is the parent school

INSERT INTO @tmpLinesOut
(
	PaySchool
	, sourceSchool
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, [Reverse]
	, Auditclass
)
SELECT EstabPoint
	, schNo
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, [Reverse]
	, Auditclass
FROM @tmpLines
WHERE grExtensionAction = 'P'		-- only payable to parent schools
	AND EstabPoint = SchNo

INSERT INTO @tmpLinesOut
(
	PaySchool
	, sourceSchool
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, [Reverse]
	, Auditclass
)

SELECT EstabPoint
	, schNo
	, grCode
	, secCode
	, edLevel
	, gvValue
	, Qty
	, value
	, PaymentNo
	, [Reverse]
	, Auditclass
FROM
	(
		SELECT EstabPoint
		, schNo
		, grCode
		, secCode
		, edLevel
		, gvValue
		, Qty
		, value
		, PaymentNo
		, [Reverse]
		, Auditclass
		, row_number() OVER (PARTITION BY EstabPoint ORDER BY case when schNo = EstabPoint then 0 else 1 end, SchNo) RR
		FROM @tmpLines
		WHERE grExtensionAction = 'A'		-- pay once if any school is OK
	) S
WHERE S.RR = 1


SELECT * FROM @tmpLinesOut

-----------------------------------------------------------
-- process the grants

-- now deal with the
-- from the temp lines, we can get all schools that have a grant
-- when grExtensionAction
-- clear out anything existing that is not PAID or on HOLD


begin transaction

-- from Grants and GrantItems
	DELETE FROM SchoolGrantItems
	FROM SchoolGrantItems
		INNER JOIN SchoolGRant
		ON SchoolGRantITems.sgId = SchoolGRant.sgID
	WHERE
		sgYear = @GrantYear
		AND sgPaymentNo = @PaymentNo
		AND (sgStatus is null or sgStatus = 'Waiting')
		AND schNo in
			(Select S.schNo
				FROM Schools S
					LEFT JOIN SchoolYearHistory SYH
					ON S.schNo = SYH.Schno
					AND SYH.syYear = @GrantYear
			 WHERE
			 	(isnull(SYH.systcode, S.schType) = @SchoolType or @SchoolType is null)
				AND (isnull(SYH.syAuth, S.schauth) = @Authority or @Authority is null)
			)


	DELETE FROM  SchoolGrant
	WHERE
		sgYear = @GrantYear
		AND sgPaymentNo = @PaymentNo
		AND (sgStatus is null or sgStatus = 'Waiting')
		AND schNo in
			(Select S.schNo
				FROM Schools S
					LEFT JOIN SchoolYearHistory SYH
					ON S.schNo = SYH.Schno
					AND SYH.syYear = @GrantYear
			 WHERE
			 	(isnull(SYH.systcode, S.schType) = @SchoolType or @SchoolType is null)
				AND (isnull(SYH.syAuth, S.schauth) = @Authority or @Authority is null)
			)


print 'Before Insert Grants'
---  Insert the new Grant record for the school

INSERT INTO SchoolGrant
( schNo
	, sgYear
	, sgPaymentNo
	, sgEstimate
	, sgRecalculated
	, sgStatus
)
Select DISTINCT
	PaySchool
	--EstabPoint		-- only for the parent school
	, @GrantYear
	, @PaymentNo
	, Estimate
	,0
	, case when (@PaymentNo > 1 and SL.Estimate = 1) then 'Waiting' else null end

FROM
	@tmpLinesOut O
	INNER JOIN @SchList SL
		ON O.PaySchool = SL.schNo

Select * from SchoolGRant WHERE sgYear = @GrantYear


INSERT INTO SchoolGRantItems
(
	sgID
	, grCode
	, secCode
	, edLevelCode
	, grValue
	, sgiQty
	, sgiValue
	, sgiPaymentNo
	, sgiReverse
	, sgiSource
)
Select
	sgID
	,O.grCode
	, O.secCode
	, O.edLevel
	, O.gvValue
	, O.qty
	, O.value
	, O.PaymentNo
	, O.[Reverse]
	, O.sourceSchool
from @tmpLinesOut O
	INNER JOIN SchoolGrant
		ON O.PaySchool = SchoolGRant.schNo
			AND SchoolGrant.sgYear = @GrantYear
			AND SchoolGrant.sgPaymentNo = @PaymentNo
	WHERE SchoolGrant.sgStatus is null			-- if you're waitintg you get nothing


Select * from SchoolGRantItems
commit transaction

return

-------------------------------------------------------------
-- Recosting --
-- Any earlier transaction for the year that differs from the current value ought to get recosted

--
-- So we find:
-- existing transactions in the year matching School, source and grCode in the current outputs

-- now we have any recosted lines
-- anything that is E or B driven should be recosted
-- if the current value calced is different from what it is on earlier items, adjust for that difference
-- write 2 transactions - one to net out everything already there if different


-- Recosting - only items in period 1 can be generated from estimates
-- we only want to recost anything that is an estimate, and that uses enrolments and boarders data


select *
INTO #prevPaymentTotals
FROM
(
select schNo, sgYear , sgiPaymentNo, secCode, grCode, sum(sgiQty)  TotQty, sum(sgiValue) TotAmt
from schoolGrant
INNER JOIN SchoolGrantITems
	ON SchoolGrant.sgID = SchoolGrantITems.sgID
GROUP BY schNo,sgYear, sgiPaymentNo, grCode, secCode
) PaymentTotals

Select * from #prevPaymentTotals

Select
	schNo, sgYear, sgiPaymentNo PaymentNo
	, secCode
	, grCode
	, sum(TotQty) PaymentQty
	, sum(totAmt) PaymentAmt
	, sum(Qty) ThisPaymentQty
	, sum(amt) ThisPaymentAmt
INTO #paymCompare
From
	(
	Select
		schNo, sgYear, sgiPaymentNo
		, secCode
		, grCode
		, TotQty
		, TotAmt
		, null Qty
		, null Amt

	 FROM #prevPaymentTotals
		WHERE sgiPaymentNo < @PaymentNo

	 UNION
		Select
			schNo, sgYear, num
			, secCode
			, grCode
			, null
			, null
			, TotQty Qty
			, TotAmt Amt


		 FROM #prevPaymentTotals
			INNER JOIN metaNumbers
			ON Num between 1 and @PaymentNo - 1
		WHERE sgiPaymentNo = @PaymentNo
)	U
GROUP BY schNo, sgYear, sgiPaymentNo
	,SecCode, grCode


	Select * from #paymCompare

INSERT INTO SchoolGrantItems
(
	sgID
	, grCode
	, secCode
	, grValue
	, sgiQty
	, sgiValue
	, sgiPaymentNo
	, sgiReverse
)
Select SchoolGrant.sgID
, PPT.grCode
, PPT.secCode
, case num when 0 then PaymentAmt/PaymentQty else ThisPaymentAmt /ThisPaymentQty end
, case num when 0 then PaymentQty * -1 else ThisPaymentQty  end
, case num when 0 then PaymentAmt * -1 else ThisPaymentAmt end

, PPT.PaymentNo
, case num when 0 then 'R' else 'A' end

FROM SchoolGrant
	INNER JOIN #paymCompare PPT
		ON SchoolGRant.SchNo = PPT.SchNo
		AND SchoolGRant.sgYear = PPT.sgYear

INNER JOIN MetaNumbers
	ON MetaNumbers.Num in (0,1)
WHERE SchoolGrant.sgYear = @GrantYear
		AND SchoolGrant.sgPaymentNo = @PaymentNo
	AND (
		isnull(PaymentQty,0) <> isnull(ThisPaymentQty,0)
		OR isnull(PaymentAmt,0) <> isnull(ThisPaymentAmt,0)
		)
	AND
		( isnull(case num when 0 then PaymentQty * -1 else ThisPaymentQty  end ,0) <> 0
		OR
			isnull(case num when 0 then PaymentAmt * -1 else ThisPaymentAmt end,0) <> 0
		)

-- Fixed Items

	DROP TABLE #prevPaymentTotals
	commit transaction

end try


	begin catch
	    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
END
--GO
GO

