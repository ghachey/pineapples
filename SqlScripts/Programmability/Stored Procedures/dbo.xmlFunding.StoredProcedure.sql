SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 02 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[xmlFunding]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = '',
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select SF.*
, F.fundType
, SOF.sofCode
, FG.sfgCode
into #sf
from SurveyFunds SF
		inner join FundTypes F
			on sf.svfType = F.fundType
		inner join SourceOfFunds SOF
			on F.sofCode = SOF.sofCode
		left join SourceOfFundsGroup FG
			on SOF.sfgCode = FG.sfgCode
		inner join SchoolSurvey S
			on SF.ssId = S.ssID
	WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar
    -- Insert statements for procedure here

declare @xmlResult xml

set @xmlResult =
(
Select Tag
, Parent
, Category as [Funding!1!ID!Hide]
, FundingCategory as [Funding!1!Category]
, fundType as [Fund!2!type]
, source as [Fund!2!source]
, amount as [Fund!2!amount]
, details as [Fund!2!details]
, svfDate as [Fund!2!date]
, fee as [Fund!2!fee]
, feePer as [Fund!2!feePer]
, feePercent as [Fund!2!feePercent]


from

(
Select DISTINCT
	1 as Tag
	, null as Parent
	, 'Funding' as Category
	, isnull(sfgCode,'NCC') as FundingCategory
	, null as fundType
	, null as Fee
	, null as FeePer
	, null as FeePercent
	, null as amount
	, null as details
	, null as svfDate
	, null as source

	from #sf
UNION ALL

Select DISTINCT
	2 as Tag
	, 1 as Parent
	, 'Funding' as Category
	, isnull(sfgCode,'NCC') as FundingCategory
	, svfType as fundType
	, svfFee as Fee
	, svfFeePer as FeePer
	, svfFeePercent as FeePercent
	, svfAmount as amount
	, svfDetails as details
	, svfDate
	, sofCode as source


	from #sf


) u
ORDER BY

	[Funding!1!Category]
	, [Fund!2!type]


for xml explicit
)


select @xmlResult
drop table #sf

END
GO

