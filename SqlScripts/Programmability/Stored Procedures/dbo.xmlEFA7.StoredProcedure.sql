SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 8 2009
-- Description:	EFA7 calculation
-- =============================================
CREATE PROCEDURE [dbo].[xmlEFA7]
	@sendAsXML int = 0
	, @xmlOut xml = null OUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- get estimated enrolments into a table - we need these later
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()


-- this is a temp table to hold the sector expenditure
-- after the prorata calculation are taken into account
-- it is built from a union query over 3 cases:
-- direct sector expenditure, prorated sector expenditure, ....

DECLARE @sectorInfo TABLE
(
	[survey Year] int
	, CostCentreCode nvarchar(20)
	, [Cost Centre] nvarchar(100)
	, sectorCode nvarchar(10)
	, ProRated int
	, [Actual Recurrent] float
	, [Budget Recurrent] float
	, [Actual Total] float
	, [Budget Total] float
	, SectorEnrolment int
	, sectorEnrolmentPerc float
)

-- this will hold the rolled up totals from sectorInfo
DECLARE @sectorTotals TABLE
(
	[survey Year] int
	, sectorCode nvarchar(10)
	, [Actual Recurrent] float
	, [Budget Recurrent] float
	, [Actual Total] float
	, [Budget Total] float
	, SectorEnrolment int
	, sectorEnrolmentPerc float
)

DECLARE @sectorEnrolments TABLE
(
	[survey Year] int
	, sectorcode nvarchar(10)
	, enrolM int
	, enrolF int
	, Enrol int
)

INSERT INTO @sectorEnrolments
SELECT      EE.LifeYear AS [Survey Year]
			, L.secCode
			, sum(enM) as EnrolM
			, sum(enF) as EnrolF
			, isnull(sum(enM) ,0) + isnull(sum(enF),0) as Enrol

			from
            #ebse EE INNER JOIN
                      dbo.Enrollments E ON EE.bestssID = E.ssID INNER JOIN
                      dbo.lkpLevels L ON E.enLevel = L.codeCode

			group by
			EE.LifeYEar
			, secCode


select [Survey Year]
	, sum(S.EnrolM) EnrolM
	, sum(S.EnrolF) EnrolF
	, sum(S.Enrol) Enrol

into #yearTotals

from @sectorEnrolments S
	group by [Survey Year]

DECLARE @ExpYear TABLE

(
	ExpYear int
	, currentA money
	, currentB money
	, TotA money
	, TotB money
	, GovExpA money
	, GovExpB money
	, GNP money
	, GNPLocal money
	, GNPCapita money
	, GNPCapitaLocal money
	, GNPCurrency nvarchar(3)
)


/*
-- begin by collecting the cost centre amounts
-- th union query handles 3 cases:
-- 1) specific postings to a sector
-- 2) pro-rated across all sectors
-- 3) not posted to any sector
*/

-- case 1 - all goes to one sector
INSERT INTO @sectorInfo
	SELECT
		EdExpenditure.xedYear as [Survey Year]
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, ccSector as SectorCode
		, ccProRate as ProRated
		, EdExpenditure.xedCurrentA as [Actual Recurrent]
		, EdExpenditure.xedCurrentB as [Budget Recurrent]
		, xedTotA as [Actual Total]
		, xedTotB as [Budget Total]
		, null as SectorEnrolment
		, null as SectorEnrolmentPerc
	FROM EdExpenditure
		INNER JOIN CostCentres ON EdExpenditure.xedEdLevel = CostCentres.ccCode
	WHERE ccSector is not null


-- case 2 - prorated (prorated = 1)
INSERT INTO @sectorInfo
	SELECT EdExpenditure.xedYear AS [Year]
		, ccCode
		, ccDescription
		, sectorEnrolments.SectorCode AS Sector
		, ccProRate
		, [xedCurrentA]*[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] AS CurrentA
		, [xedCurrentB]*[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] AS CurrentB
		, xedTotA*[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] as [Actual Total]
		, xedTotB *[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] as [Budget Total]
		, sectorEnrolments.Enrol as SectorEnrolment
		, cast([sectorEnrolments].[enrol] as float)/cast([yearEnrolments].[enrol] as float) as SectorEnrolmentPerc
	FROM EdExpenditure
		INNER JOIN CostCentres ON EdExpenditure.xedEdLevel = CostCentres.ccCode
		INNER JOIN @sectorEnrolments sectorEnrolments ON EdExpenditure.xedYear = sectorEnrolments.[Survey Year]
		INNER JOIN #YearTotals yearEnrolments ON EdExpenditure.xedYear = yearEnrolments.[Survey Year]
	WHERE
		ccSector Is Null AND ccProRate=1


-- case 3 ignored becuase ccPRoRAte is 2 and the sector code is null, so sector is left blank in the output
INSERT INTO @sectorInfo
	Select EdExpenditure.xedYear as [Survey Year]
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, null as Sector
		, ccProRate as ProRated
		, EdExpenditure.xedCurrentA as [Actual Recurrent]
		, EdExpenditure.xedCurrentB as [Budget Recurrent]
		, xedTotA as [Actual Total]
		, xedTotB as [Budget Total]
		, null as SectorEnrolment
		, null as SectorEnrolmentPerc
	FROM EdExpenditure
		LEFT JOIN CostCentres ON EdExpenditure.xedEdLevel = CostCentres.ccCode
	WHERE
	ccSector Is Null AND (ccProRate=2 or ccProRate is null)	-- catch any bad data set up

-- finally we add the sector enrolment values
INSERT INTO @sectorInfo
	Select S.[Survey Year]
		, null as CostCentre
		, null as CostCentreDescription
		, sectorCode
		, null as ProRated
		, null as AR
		, null as BR
		, null as AT
		, null as BT
		, S.Enrol
		, cast(S.Enrol as float)/ cast(Y.Enrol as float)

	from @sectorEnrolments S
		INNER JOIN #yearTotals Y on S.[Survey Year] = Y.[Survey Year]


-- now flatten these records so we have just one per sector
-- this level of the query groups and total the union query
INSERT INTO @sectorTotals

Select U.[Survey Year]
	, U.sectorCode
	, sum(U.[Actual Recurrent]) [Actual Recurrent]
	, sum(U.[Budget Recurrent]) [Budget Recurrent]
	, sum(U.[Actual Total]) [Actual Total]
	, sum(U.[Budget Total]) [Budget Total]
	, avg(U.SectorEnrolment) [Sector Enrolment]				-- its repeated on each row
	, avg(U.SectorEnrolmentPerc) [Sector Alloc]
FROM @sectorInfo U
Group BY
	[Survey Year]
	, sectorCode


-- now the annual totals
INSERT INTO @ExpYear
SELECT fnmYear

	, sum(U.[Actual Recurrent]) [Actual Recurrent]
	, sum(U.[Budget Recurrent]) [Budget Recurrent]
	, sum(U.[Actual Total]) [Actual Total]
	, sum(U.[Budget Total]) [Budget Total]
	, fnmExpTotA
	, fnmExpTotB
	, fnmGNP
	, fnmGNP * (case when isnull(fnmGNPXchange,0) = 0 then 1 else fnmGNPXChange end)
	, fnmGNPCapita
	, fnmGNPCapita * (case when isnull(fnmGNPXchange,0) = 0 then 1 else fnmGNPXChange end)
	, fnmGNPCurrency

FROM GovtExpenditure GV
	LEFT JOIN @sectorInfo U
		ON GV.fnmYear = U.[Survey Year]
GROUP BY fnmYear
, fnmExpTotA
, fnmExpTotB
, fnmGNP
, fnmGNPCapita
, fnmGNPXchange
, fnmGNPCurrency

if @SendAsXML = 0 begin

	-- the outer level of the query ormalises Actual and Budget
	Select
		[Survey Year]
		, [CostCentreCode]
		, [Cost Centre]
		, SectorCode
		, ProRated
		, GNP
		, GNPPerCapita
		, case num when 1 then 'Actual' else 'Budget' end [View]
		, case num when 1 then GovtExpActual else GovtExpBudget end [Total Govt Exp]
		, case num when 1 then [Actual Total] else [Budget Total] end [Sector Total]
		, case num when 1 then [Actual Recurrent] else [Budget Recurrent] end [Sector Recurrent]
	from
		(Select num from MetaNumbers WHERE num in (1,2)) M
	CROSS JOIN
	(
		Select U.*

		, G.fnmGNP GNP
		, case when fnmGNPCurrency is null then 1 else fnmGNPXChange end * G.fnmGNPCapita GNPPerCapita
		, G.fnmExpTotA GovtExpActual
		, G.fnmExpTotB GovtExpBudget
		, G.fnmGNPCurrency
		, G.fnmGNPXChange

	from
	@sectorInfo U
	INNER JOIN GovtExpenditure G on U.[Survey Year] = G.fnmYear

	) main
end


if @SendAsXML = 1 begin

	declare @xml xml
	declare @xmlYears xml

	Select @xmlYEars =
	(
	Select
	ExpYear [@year]
	, currentA  EdExpRecurrentA
	, currentB  EdExpRecurrentB
	, TotA		EdExpTotalA
	, TotB		EdExpTotalB
	, GovExpA	GovExpTotalA
	, GovExpB	GovExpTotalB
	, GNP
	, GNPLocal
	, GNPCapita		GNPperCapita
	, GNPCapitaLocal GNPperCapitaLocal
	, GNPCurrency
	FROM @ExpYear
	FOR XML PATH('Exp')
	)

	Select @xml =
	(
	SELECT [Survey Year] [@year]


	, sectorCode		[@sector]
	, cast([Actual Recurrent] as decimal(15,2)) [RecurrentA]
	, cast([Budget Recurrent] as decimal(15,2))  [RecurrentB]
	, cast([Actual Total] as decimal(15,2)) 	 [TotalA]
	, cast([Budget Total] as decimal(15,2)) 	 [TotalB]
	, [sectorEnrolment]  [enrol]
	, case isnull(GV.totA,0) when 0 then 0 else cast([Actual Total] / GV.totA as decimal (8,5)) end PercA
	, case isnull(GV.totB,0) when 0 then 0 else cast([Budget Total] / GV.totB as decimal (8,5)) end PercB
	, case  isnull(sectorEnrolment,0) when 0 then 0 else cast([Actual Total]/sectorEnrolment as decimal(15,2)) end ExpPupilA
	, case  isnull(sectorEnrolment,0) when 0 then 0 else cast([Budget Total]/sectorEnrolment as decimal(15,2)) end ExpPupilB
	, case when isnull(sectorEnrolment,0) = 0 or isnull(GNPCapitaLocal,0) = 0 then 0
		else cast(([Actual Total]/sectorEnrolment) / GNPCapitaLocal as decimal(15,5)) end ExpPupilGNPCapitaA
	, case when isnull(sectorEnrolment,0) = 0 or isnull(GNPCapitaLocal,0) = 0 then 0
		else cast(([Budget Total]/sectorEnrolment) / GNPCapitaLocal as decimal(15,5)) end ExpPupilGNPCapitaB
	FROM @SectorTotals SS
	LEFT JOIN @ExpYear GV
		ON SS.[Survey Year] = GV.expYear

	FOR XML PATH('SectorExp')

	)

	SELECT @xmlOut =
	(
		Select
		@xmlYears
		, @xml
		FOR XML PATH('Expenditure')
	)


end
drop table #yearTotals
drop table #ebse

END
GO

