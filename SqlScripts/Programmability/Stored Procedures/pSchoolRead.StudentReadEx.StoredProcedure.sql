SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 04 2018
-- Description:	Extended read of student for web site
-- =============================================
CREATE PROCEDURE [pSchoolRead].[StudentReadEx]
	@studentID uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	from Student_ S
	WHERE stuID = @studentID

	Select *
	from StudentEnrolment_
	WHERE stuID = @studentID

END
GO

