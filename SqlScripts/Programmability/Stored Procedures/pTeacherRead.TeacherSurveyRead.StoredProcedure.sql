SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Read a single teacher survey record
-- dec 2014: Uses the view for selection
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherSurveyRead]
	-- Add the parameters for the stored procedure here
	@TeacherSurveyID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT *
	FROM
		pTeacherRead.TeacherSurveyV
	WHERE tchsID = @TeacherSurveyID


END
GO

