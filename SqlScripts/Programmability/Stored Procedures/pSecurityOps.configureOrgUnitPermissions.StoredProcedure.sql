SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BRian Lewis
-- Create date: 10 3 2010
-- Description:	Configure org unit permissions
-- fo a single data area

-- =============================================
CREATE PROCEDURE [pSecurityOps].[configureOrgUnitPermissions]
	-- Add the parameters for the stored procedure here
	@OrgUnit sysname = 0
	, @dataArea nvarchar(20)
	, @basic nvarchar(2) = null
	, @extended nvarchar(2) = null
	, @high nvarchar(2) = null
	, @admin nvarchar(1) = null
	, @ops nvarchar(1) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @OrgUnitID int

	declare @readR sysname
	declare @writeR sysname
	declare @readID int
	declare @writeID int

	select @OrgUnitID = database_principal_id(@OrgUnit)

/*************************************************************************************
Basic
*************************************************************************************/
		select @readR = 'p' + @dataArea + 'Read'
		select @writeR = 'p' + @dataArea + 'Write'
		select @readID = database_principal_id(@readR)
		select @writeID = database_principal_id(@writeR)

		declare @addTo sysname
		declare @dropFrom sysname
		declare @addToID int
		declare @dropFromID int

		select @addTo = null

		if (@Basic = 'RW') begin
			select @AddTo = @writeR
			select @dropFrom = @readR			-- tidier becuase of the nesting
		end

		if (@Basic = 'R') begin

			select @AddTo = @readR
			select @dropFrom = @writeR			-- tidier becuase of the nesting
		end

		select @AddToID =  database_principal_id(@AddTo)
		select @DropFromID =  database_principal_id(@DropFrom)

		if (@addTo is null)	begin
			if exists (Select role_principal_id from sys.database_role_members
				WHERE role_principal_ID = @readID and member_principal_id = @orgUnitID)
						exec sp_droprolemember @readR , @OrgUnit

			if exists (Select role_principal_id from sys.database_role_members
				WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
						exec sp_droprolemember @writeR , @OrgUnit
			end
		else	begin
			if not exists (Select role_principal_id from sys.database_role_members
				WHERE role_principal_ID = @addToID and member_principal_id = @orgUnitID)
						exec sp_addrolemember @AddTo , @OrgUnit
				-- drop from read becuase its nest throuh Write - always

			if exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @dropFromID and member_principal_id = @orgUnitID)
						exec sp_droprolemember  @dropFrom , @OrgUnit

		end


/*************************************************************************************
Extended
*************************************************************************************/
		select @readR = 'p' + @dataArea + 'ReadX'
		select @writeR = 'p' + @dataArea + 'WriteX'
		select @readID = database_principal_id(@readR)
		select @writeID = database_principal_id(@writeR)


		select @addTo = null

		if (@extended = 'RW') begin
			select @AddTo = @writeR
			select @dropFrom = @readR			-- tidier becuase of the nesting
		end

		if (@Extended = 'R') begin

			select @AddTo = @readR
			select @dropFrom = @writeR			-- tidier becuase of the nesting
		end

		select @AddToID =  database_principal_id(@AddTo)
		select @DropFromID =  database_principal_id(@DropFrom)

		if (@addTo is null)	begin
			if exists (Select role_principal_id from sys.database_role_members
				WHERE role_principal_ID = @readID and member_principal_id = @orgUnitID)
						exec sp_droprolemember @readR , @OrgUnit

			if exists (Select role_principal_id from sys.database_role_members
				WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
						exec sp_droprolemember @writeR , @OrgUnit
			end
		else	begin
			if not exists (Select role_principal_id from sys.database_role_members
				WHERE role_principal_ID = @addToID and member_principal_id = @orgUnitID)
						exec sp_addrolemember @AddTo , @OrgUnit
				-- drop from read becuase its nest throuh Write - always

			if exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @dropFromID and member_principal_id = @orgUnitID)
						exec sp_droprolemember  @dropFrom , @OrgUnit

		end


/*************************************************************************************
High
*************************************************************************************/
		select @readR = 'p' + @dataArea + 'ReadXX'
		select @writeR = 'p' + @dataArea + 'WriteXX'
		select @readID = database_principal_id(@readR)
		select @writeID = database_principal_id(@writeR)

		if not (@readID is null) begin

			select @addTo = null

			if (@High = 'RW') begin
				select @AddTo = @writeR
				select @dropFrom = @readR			-- tidier becuase of the nesting
			end

			if (@high = 'R') begin

				select @AddTo = @readR
				select @dropFrom = @writeR			-- tidier becuase of the nesting
			end

			select @AddToID =  database_principal_id(@AddTo)
			select @DropFromID =  database_principal_id(@DropFrom)

			if (@addTo is null)	begin
				if exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @readID and member_principal_id = @orgUnitID)
							exec sp_droprolemember @readR , @OrgUnit

				if exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
							exec sp_droprolemember @writeR , @OrgUnit
				end
			else	begin
				if not exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @addToID and member_principal_id = @orgUnitID)
							exec sp_addrolemember @AddTo , @OrgUnit
					-- drop from read becuase its nest throuh Write - always

				if exists (Select role_principal_id from sys.database_role_members
						WHERE role_principal_ID = @dropFromID and member_principal_id = @orgUnitID)
							exec sp_droprolemember  @dropFrom , @OrgUnit

			end
		end			--@readID is null


/*************************************************************************************
Admin
*************************************************************************************/
		select @writeR = 'p' + @dataArea + 'Admin'
		select @writeID = database_principal_id(@writeR)

		if not (@writeID is null) begin


			if (@Admin = 'Y') begin
				if not exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
							exec sp_addrolemember @writeR , @OrgUnit

				end
			else	begin

				if exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
							exec sp_droprolemember @writeR , @OrgUnit


			end
		end


/*************************************************************************************
Ops
*************************************************************************************/
		select @writeR = 'p' + @dataArea + 'Ops'
		select @writeID = database_principal_id(@writeR)

		if not (@writeID is null) begin


			if (@Ops = 'Y') begin
				if not exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
							exec sp_addrolemember @writeR , @OrgUnit

				end
			else	begin

				if exists (Select role_principal_id from sys.database_role_members
					WHERE role_principal_ID = @writeID and member_principal_id = @orgUnitID)
							exec sp_droprolemember @writeR , @OrgUnit


			end
		end
END
GO

