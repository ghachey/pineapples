SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 03 2010
-- Description:	the NIS value calculation
-- =============================================
CREATE FUNCTION [common].[NisSizeComplies]
(
	-- Add the parameters for the function here
	@StdValue decimal(10,2)
	, @StdMaxValue decimal(10,2)
	, @PerPupil decimal(10,4)
	,@PerTeacher decimal(10,4)
	, @StdProRataCeiling decimal(10,2)
	, @StdTextValue nvarchar(50)
	-- and the values to work out
	, @NumPupils int
	, @NumTeachers int
	, @ValueToMatch decimal(10,2)

)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	declare @ProRataCalc decimal(10,4)
	declare @EffectiveMax decimal(10,4)
	declare @EffectiveMin decimal(10,4)

	-- calculate the minimum requirement according to the proratas
	Select @ProRataCalc =
		IsNull(@PerPupil,0) * isnull(@NumPupils,0)
		+ Isnull(@PerTeacher,0) * isnull(@NumTeachers,0)

	-- never require more than the ceiling, however large the prorata calc gets
	if @StdProRataCeiling < @ProRataCalc 	-- expecting ANSI null behaviour
		Select @ProRataCalc = @StdProRataCeiling

	-- the effective minimum to compare to the value is the maximum of the proratacalc and the Std Value
	select @EffectiveMin =  case when @ProRataCalc < isnull(@StdValue,0) then @StdValue
									else @ProRataCalc end

	If (@ValueToMatch is null)
		select @Result = null

	else
		Select @Result = case when @ValueToMatch between  @EffectiveMin and isnull(@StdMaxValue, @ValueToMatch) then 1
								else 0
							end
	return @result

END
GO

