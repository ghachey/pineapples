SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 1 2017
-- Description:	Return the svyYear formatted according to sysParam
-- Uses the sysParam value SURVEY_YEAR_FORMAT as a template
-- =============================================
CREATE FUNCTION [common].[surveyYearFormat]
(
	-- Add the parameters for the function here
	@value int
)
returns nvarchar(20)
AS
BEGIN
	declare @template nvarchar(20)
	Select @template = common.sysParam('SURVEY_YEAR_FORMAT');


	declare @yyyy nvarchar(4) = convert(nvarchar(4), @value);
	declare @yy nvarchar(2) = right(@yyyy,2);

	declare @nnnn nvarchar(4) = convert(nvarchar(4), @value + 1);
	declare @nn nvarchar(2) = right(@nnnn,2);

	if @template is null
		return @yyyy

	Select @template = replace(@template, 'yyyy', @yyyy)
	Select @template = replace(@template, 'yy', @yy)
	Select @template = replace(@template, 'nnnn', @nnnn)
	Select @template = replace(@template, 'nn', @nn)

	-- Return the result of the function
	RETURN @template

END
GO

