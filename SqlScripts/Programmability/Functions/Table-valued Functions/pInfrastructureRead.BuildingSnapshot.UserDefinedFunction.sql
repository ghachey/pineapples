SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 3 2010
-- Description:	Return a snapshot of buildings as at a supplied date
-- =============================================
CREATE FUNCTION [pInfrastructureRead].[BuildingSnapshot]
(
	-- Add the parameters for the function here
	@AsAtDate datetime
)
RETURNS
@table TABLE
(
	ID int
	, reviewID int
	, reviewDate datetime
	, reviewInspID int
	, schNo nvarchar(50)
	, buildingType nvarchar(10)
	, subType nvarchar(10)
	, closed int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set

	INSERT INTO @table
	(
		ID
		, reviewID
		, reviewDate
		, reviewInspID
		, schNo
		, buildingType
		, subType
		,closed
	)
	Select
		ID
		, reviewID
		, reviewDate
		, reviewInspID
		, schNo
		, BuildingType
		, subType
		, closed
	FROM
	(
		Select ID
		, ReviewID
		, reviewDate
		, reviewInspID
		, schNo
		, BuildingType
		, SubType
		, Closed
		, Row_Number() OVER (PARTITION BY ID ORDER BY reviewDate DESC) pos
		FROM
		(
			SELECT bldID  ID
				, null ReviewID
				, 'B' as rType
				, schNo
				, case when isnull(bldgYear,0) <= 1900 then '1900-01-01'
						else cast(cast(bldgYear as nchar(4))+'-01-01' as datetime) end reviewDate
				, null as reviewInspID
				, bldgCode BuildingType
				, bldgSubType SubType
				, bldgClosed Closed

			FROM Buildings

		UNION ALL
			SELECT BuildingReview.bldID
				, brevID
				, 'A'
				, Buildings.schNo
				, inspStart
				, BuildingReview.inspID
				, brevCode
				, brevSubType
				, brevClosed
			FROM BuildingReview
				INNER JOIN Buildings
					ON BuildingReview.bldID = Buildings.bldID
				INNER JOIN SchoolInspection SI
					ON SI.inspID = BuildingReview.inspID
		) U
		WHERE ( U.reviewDate <= @AsAtDate)
	) RANKED
	WHERE RANKED.Pos = 1

	RETURN
END
GO

