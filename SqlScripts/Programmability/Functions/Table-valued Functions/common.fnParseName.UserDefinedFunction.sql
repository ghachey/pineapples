SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--http://www.sqlusa.com/bestpractices2005/parsename/
CREATE FUNCTION [common].[fnParseName]

               (@FullName nvarchar(128))

RETURNS @NameComposition TABLE(Honorific nvarchar(128),
								FirstName  nvarchar(128),

                               MiddleName nvarchar(128),

                               LastName   nvarchar(128),
                               Suffix nvarchar(128),
								FirstNameSoundex nchar(4),
								LastNameSoundex nchar(4)
						)

AS

  BEGIN

    DECLARE  @Honorific nvarchar(128),
			@FirstName  nVARCHAR(128),

             @MiddleName nVARCHAR(128),

             @LastName   nVARCHAR(128),

             @Suffix   nvarchar(128),

             @WORK       nVARCHAR(128)

    SET @FullName = RTRIM(LTRIM(REPLACE(@FullName,'.','. ')))
	SET @FullNAme =  RTRIM(LTRIM(REPLACE(@FullName,'(',' ')))
	SET @FullNAme =  RTRIM(LTRIM(REPLACE(@FullName,')',' ')))

    -- REPLACE DOUBLE SPACE WITH A SINGLE SPACE - ELIMINATE EXTRA SPACES

    -- make sure the following line copies the single and double spaces correcly

    WHILE CHARINDEX('  ',@FullName) > 0
      SET @FullName = REPLACE(@FullName,'  ',' ')

    WHILE CHARINDEX('.',@FullName) > 0
      SET @FullName = REPLACE(@FullName,'.','')

	SET @FullName = UPPER(Left(@Fullname,1)) + substring(lower(@FullName),2,len(@FullName))
	declare @k int = 0
	WHILE CHARINDEX(' ', @FullName, @k) > 0 begin

		Select @k = CHARINDEX(' ', @FullName, @k) + 1;
		SET @FullName = left(@FullName, @k - 1) + upper(SUBSTRING(@FullName, @k,1)) + substring(@FullName, @k + 1, len(@FullName))
	end

    SET @WORK = RTRIM(LEFT(@FullName,CHARINDEX(' ',@FullName)))

    IF RIGHT(@WORK,1) = ',' begin
		-- is the name reversed with a comma? last name is the first part
		SET @LastName = LEFT(@WORK,LEN(@WORK) - 1)

		IF LEN(@LastName) > 0
			SET @FullName = LTRIM(RIGHT(@FullName,LEN(@FullName) - LEN(@WORK)))
	end

	-- deal with the suffix here

	select @WORK = LTRIM(RIGHT(@FullName,CHARINDEX(' ',REVERSE(@FullName)+' ')))

	if @WORK in ('Snr','Jr','II','III','IV') begin
		SET @Suffix = @WORK
		SET @FullName = RTRIM(LEFT(@FullName,LEN(@FullName) - LEN(@WORK)))
	end


	-- deal with compound names

    WHILE CHARINDEX(',',@FullName) > 0
      SET @FullName = REPLACE(@FullName,',','')

	WHILE CHARINDEX(' VAN ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' VAN ',' Van_')

	WHILE CHARINDEX(' VON ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' VON ',' Von_')

	WHILE CHARINDEX(' ST ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' ST ',' St_')

	WHILE CHARINDEX(' DE LA',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' DE LA ',' de_la_')

	WHILE CHARINDEX(' DE ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' DE ',' de_')

	WHILE CHARINDEX(' DU ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' DU ',' du_')

	WHILE CHARINDEX(' LE ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' LE ',' Le_')

	WHILE CHARINDEX(' LA ',@FullName) > 0
		SET @FullName = REPLACE(@FullName,' LA ',' La_')


    IF @LastName IS NULL

      BEGIN

        SET @LastName=LTRIM(RIGHT(@FullName,CHARINDEX(' ',

                                REVERSE(@FullName)+' ')))

        SET @FullName=RTRIM(LEFT(@FullName,LEN(@FullName) - LEN(@LastName)))

      END
    SET @FirstName = RTRIM(LEFT(@FullName,CHARINDEX(' ',@FullName + ' ')))

	if @FirstName in ( 'BR','FR','REV','DR','SR','MR','MISS','MRS','MS')
		SET @Honorific = @FirstName

	if len(@Honorific) > 0 begin
		SET @FullName = LTRIM(RIGHT(@FullName,LEN(@FullName) - LEN(@Honorific)))
		SET @FirstName = RTRIM(LEFT(@FullName,CHARINDEX(' ',@FullName + ' ')))
	end

	SET @FullName = LTRIM(RIGHT(@FullName,LEN(@FullName) - LEN(@FirstName)))

    SET @MiddleName = @FullName

	set @LastName = replace(@LastName,'_',' ')

	set @Honorific = rtrim(@Honorific)
	set @FirstName = rtrim(@FirstName)
	set @MiddleName = rtrim(@MiddleName)
	set @LastName = rtrim(@LastName)
	set @Suffix = rtrim(@Suffix)


    INSERT @NameComposition


    SELECT case when @Honorific = '' then null else @Honorific end
			, case when @FirstName = '' then null else @FirstName end
			, case when @MiddleName = '' then null else @MiddleName end
			, case when @LastName = '' then null else @LastName end
			, case when @Suffix = '' then null else @Suffix end
			,soundex(@firstName)
			,soundex(@LastName)

    RETURN

  END
GO

