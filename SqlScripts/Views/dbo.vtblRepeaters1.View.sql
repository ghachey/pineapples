SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblRepeaters1]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptAge,
PupilTables.ptLevel,
PupilTables.ptM,
PupilTables.ptF
FROM
dbo.lkpLevels INNER JOIN dbo.PupilTables
ON dbo.lkpLevels.codeCode = dbo.PupilTables.ptLevel
WHERE
(((dbo.PupilTables.ptCode)='REP' Or (dbo.PupilTables.ptCode)='REP1')
AND ((dbo.PupilTables.ptAge)<>0)) OR (((dbo.PupilTables.ptCode)='REP'
Or (dbo.PupilTables.ptCode)='REP1')AND ((dbo.lkpLevels.lvlYear)<>1))
GO

