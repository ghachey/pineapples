SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qrySurveyStatus]
AS
SELECT     dbo.SchoolSurvey.ssID, dbo.Survey.svyYear, dbo.SurveyControl.saYear, dbo.Survey.svyCensusDate, dbo.Survey.svyCollectionDate, dbo.SurveyControl.schNo,
                      dbo.Schools.schName, dbo.SurveyControl.saSent, dbo.SurveyControl.saCollected, dbo.SurveyControl.saReceived, dbo.SurveyControl.saReceivedTarget,
                      dbo.SurveyControl.saCompletedby, dbo.SurveyControl.saEntered, dbo.SurveyControl.saAudit, dbo.SurveyControl.saAuditDate, dbo.SurveyControl.saAuditBy,
                      dbo.SurveyControl.saAuditResult, dbo.SurveyControl.saAuditNote, dbo.SurveyControl.saComment, dbo.SurveyControl.saFileLocn, dbo.SchoolSurvey.ssReview,
                      dbo.Schools.iCode, dbo.Islands.iGroup, dbo.Schools.schType, dbo.Schools.schAuth, dbo.SchoolSurvey.ssMissingData, dbo.SchoolSurvey.ssMissingDataNotSupplied,
                       dbo.ssIDQuality.NumIssues, dbo.ssIDQuality.NumAlerts, dbo.ssIDQuality.NumBlocks, dbo.ssIDQuality.MaxLevel, dbo.Schools.schClosed, dbo.Schools.schEst,
                      dbo.SurveyControl.saDormant, dbo.SurveyControl.saReceivedD, dbo.SurveyControl.saReceivedDTarget
FROM         dbo.Islands INNER JOIN
                      dbo.Schools ON dbo.Islands.iCode = dbo.Schools.iCode INNER JOIN
                      dbo.Survey INNER JOIN
                      dbo.SurveyControl ON dbo.Survey.svyYear = dbo.SurveyControl.saYear LEFT OUTER JOIN
                      dbo.SchoolSurvey ON dbo.SurveyControl.saYear = dbo.SchoolSurvey.svyYear AND dbo.SurveyControl.schNo = dbo.SchoolSurvey.schNo LEFT OUTER JOIN
                      dbo.ssIDQuality ON dbo.SchoolSurvey.ssID = dbo.ssIDQuality.ssID ON dbo.Schools.schNo = dbo.SurveyControl.schNo
GO
GRANT SELECT ON [dbo].[qrySurveyStatus] TO [pSurveyRead] AS [dbo]
GO

