SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Somalia view of CEC / CSC based on OC abd SC fields
-- see PIVSurvey_Rubbish
-- =============================================
CREATE VIEW [dbo].[PIVColsCECSO]
AS
Select ssID
, case when ssParentCommittee = 1 then 'Y'
		when ssParentcommittee = 0 then 'N'
		when ssParentCommittee is null then null
end HasCEC
, ssPCMeet CECMeetings
, ssPCMembersF	CECMembersF
, ssPCMembersM	CECMembersM
, case when ssPCMembersM is null and ssPCMembersF is null then NULL
		else isnull(ssPCMembersM, 0) + isnull(ssPCMembersF,0) end CECMembers

, ssSCMembersF	CTCMembersF
, ssSCMembersM	CTCMembersM
, case when ssSCMembersM is null and ssSCMembersF is null then NULL
		else isnull(ssSCMembersM, 0) + isnull(ssSCMembersF,0) end CTCMembers


FROM SchoolSurvey
GO

