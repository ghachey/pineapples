SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRInspectionTypes] AS
SELECT intyCode,
isnull(CASE dbo.userLanguage()
    WHEN 0 THEN intyDesc
    WHEN 1 THEN intyDescL1
    WHEN 2 THEN intyDescL2
 END,intyDesc) AS intyDesc
 FROM dbo.lkpInspectionTypes
GO
GRANT SELECT ON [dbo].[TRInspectionTypes] TO [public] AS [dbo]
GO

