SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paCompetencies]
WITH VIEW_METADATA
AS
Select C.*
, pafrmCode + '.' + paComCode comFullID
from paCompetencies_ C
GO

