SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2007
-- Description:	dormitory summary by survey
-- =============================================
CREATE VIEW [dbo].[ssIDClassrooms]
AS
SELECT
CLASS.ssID,
Count(CLASS.rmType) AS NumRooms,
Sum(CLASS.rmSize) AS TotSize,
Min(CLASS.rmSize) AS minSize,
Max(CLASS.rmSize) AS MaxSize,
Sum(case when [rmSize]>=[stdValue] then 1 else 0 end) AS sizeOK,
Sum(case IsNull([rmSize],0) when 0 then 0 else 1 end ) AS SizeSupplied,
STD.stdValue
FROM vtblClassrooms AS CLASS
	INNER JOIN SchoolSurvey SS
		ON CLASS.ssID = SS.ssID
	INNER JOIN schoolyearApplicableStandards AS STD
		ON SS.schNo = STD.schNo and SS.svyYear = STD.LifeYear
-- note that this view means that the following record MUST be defined in
-- SurveyYearStandards
WHERE (((STD.stdName)='ROOM_SIZE') AND ((STD.stdItem)='CLASS'))
GROUP BY CLASS.ssID, STD.stdValue
GO

