SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
--
-- Teacher counts aggregated by district and sector.
-- Derived from warehouse.schoolTeacherCount table.
-- =============================================
CREATE VIEW [warehouse].[DistrictTeacherCount]
AS
Select SurveyYear
, DistrictCode
, dName District
, GenderCode
, AgeGroup
, Sector
-- this logic assumes that the column 'Support' is a text field in SchoolStaffCount
-- and that records are disaggregated between 'Support' and (null) values - ie teaching staff
-- warehouse.schoolTeacherCount is a VIEW that 'denormalises' support numbers in the same way as this view
-- Certified, Qualified Certqual do NOT include non-teaching staff
-- see Issue #518

, sum(case when Support is not null then NumStaff else 0 end) NumSupportStaff
, sum(case when Support is null then NumStaff else null end) NumTeachers
, sum(case when Support is null then Certified else null end) Certified
, sum(case when Support is null then Qualified else null end) Qualified
, sum(case when Support is null then CertQual else null end) CertQual
from warehouse.schoolStaffCount S
	LEFT JOIN Districts D
		ON S.DistrictCode = D.dID
GROUP BY SurveyYear,  DistrictCode, dName, GenderCode, Sector, AgeGroup
GO

