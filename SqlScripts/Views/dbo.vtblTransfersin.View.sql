SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTransfersin]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptLevel,
PupilTables.ptRow AS OriginCode,
PupilTables.ptM,
PupilTables.ptF
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='TRIN'))
GO

