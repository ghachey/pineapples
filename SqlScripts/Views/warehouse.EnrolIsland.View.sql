SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by District
--
-- This is a simple consoilidation of warehouse.enrol to group by District
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW
[warehouse].[EnrolIsland]
AS
Select SurveyYear
, [Island Code] IslandCode
, Island
, [Region Code] RegionCode
, Region
, [District Code] DistrictCode
, District

, ClassLevel
, Age
, GenderCode

, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA

from warehouse.Enrol E
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON E.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, [Island Code]
, Island
, [Region Code]
, Region
, [District Code]
, District
, ClassLevel
, Age
, GenderCode
GO

