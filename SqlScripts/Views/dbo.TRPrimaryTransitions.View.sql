SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRPrimaryTransitions]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codedescription
	WHEN 1 THEN codedescriptionL1
	WHEN 2 THEN codedescriptionL2

END,codedescription) AS codedescription,codeSeq

FROM         dbo.lkpPrimaryTransitions
GO
GRANT SELECT ON [dbo].[TRPrimaryTransitions] TO [public] AS [dbo]
GO

