SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblStaki]
AS
SELECT
ExamStandardTest.stID,
ExamStandardTest.exID,
ExamStandardTest.schNo,
ExamStandardTest.stGender,
ExamStandardTest.stSubject,
ExamStandardTest.stComponent,
ExamStandardTest.st0,
ExamStandardTest.st1,
ExamStandardTest.st2,
ExamStandardTest.st3,
ExamStandardTest.st4,
ExamStandardTest.st5
FROM dbo.ExamStandardTest
GO

