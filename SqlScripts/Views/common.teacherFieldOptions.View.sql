SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [common].[teacherFieldOptions]
AS
SELECT        f, g, seq
FROM            (SELECT        'Authority' AS f, NULL AS g, 0 AS seq
                          UNION
                          SELECT        'School Type' AS f, NULL AS g, 0 AS seq
                          UNION
                          SELECT        vocabTerm AS f, NULL AS g, 0 AS seq
                          FROM            dbo.sysVocab
                          WHERE        (vocabName = 'District')
                          UNION
                          SELECT        vocabTerm AS f, NULL AS g, 0 AS seq
                          FROM            dbo.sysVocab AS sysVocab_2
                          WHERE        (vocabName = 'National Electorate')
                          UNION
                          SELECT        vocabTerm AS f, NULL AS g, 0 AS seq
                          FROM            dbo.sysVocab AS sysVocab_1
                          WHERE        (vocabName = 'Local Electorate')
                          UNION
                          SELECT        vocabTerm AS f, NULL AS g, 0 AS seq
                          FROM            dbo.sysVocab AS sysVocab_4
                          WHERE        (vocabName = 'SalaryLevel')) AS S
GO

