SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [common].[schoolDataOptions]
AS
Select C, N, seq
from
(
Select 'total' c
, 'Total' N
, 0 seq
UNION
Select 'enrol' C
, 'Enrol' N
, 1 seq
UNION
Select 'percF' C
, 'Female %' N
, 2 seq

) S
GO

