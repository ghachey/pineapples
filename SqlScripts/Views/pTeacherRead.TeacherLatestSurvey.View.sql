SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherRead].[TeacherLatestSurvey]
AS
Select TI.tID
, tchsID
, TI.tSurname
, TI.tGiven
, TI.tDOB
, TI.tSex
, TI.tPayroll
, svyYear
, schNo
, schName
, tchStatus
, tchRole
, tchTAM
, tchHouse
FROM TeacherIdentity TI
		LEFT JOIN pTeacherRead.TeacherSurveyV V
			ON TI.tID = V.tID
			AND V.LatestSurvey = 1
GO

