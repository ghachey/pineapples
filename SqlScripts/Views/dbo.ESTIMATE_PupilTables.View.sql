SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ESTIMATE_PupilTables]
AS
SELECT
ESTIMATE_BestSurveyPupilTables.LifeYear AS [Survey Year],
ESTIMATE_BestSurveyPupilTables.schNo,
ESTIMATE_BestSurveyPupilTables.bestYear AS [Year of Data],
ESTIMATE_BestSurveyPupilTables.Estimate,
ESTIMATE_BestSurveyPupilTables.Offset AS [Age of Data],
ESTIMATE_BestSurveyPupilTables.bestssID,
ESTIMATE_BestSurveyPupilTables.bestssqLevel,
ESTIMATE_BestSurveyPupilTables.ActualssID,
ESTIMATE_BestSurveyPupilTables.ActualssqLevel,
ESTIMATE_BestSurveyPupilTables.SurveyDimensionssID,
ESTIMATE_BestSurveyPupilTables.tdefCode, PupilTables.*
FROM
dbo.ESTIMATE_BestSurveyPupilTable
AS ESTIMATE_BestSurveyPupilTables
LEFT JOIN PupilTables
ON (ESTIMATE_BestSurveyPupilTables.tdefCode =
PupilTables.ptCode) AND
(ESTIMATE_BestSurveyPupilTables.bestssID =
PupilTables.ssID);
GO

