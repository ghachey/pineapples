SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolYearHasDataSize]
AS
SELECT     S.schNo, S.svyYear,
			S.ssID EnrolssID,S.ssSizeSite,
			Q.ssqLevel
FROM         dbo.SchoolSurvey AS S LEFT OUTER JOIN
                      dbo.tfnQualityIssues('GROUNDS', null) AS Q ON S.ssID = Q.ssID
WHERE     (S.ssID IN
                          (SELECT     ssID
                            FROM          dbo.TeacherSurvey AS D))
			and (q.ssqLevel is null or q.ssqLevel <2)
			and  (S.ssSizeSite >0)
GO

