SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************
This view shows the total of enrolments in a survey, and the total of those enrolments that are at the official age for their year of ed.
This is not the same as the of official age enrolments for the school level, which is bigger.

*************************************************************/
CREATE VIEW [pEnrolmentRead].[ssIDEnrolmentN]
as
SELECT     Enrollments.ssID,

SUM(enM) AS EnrolM,
SUM(enF) AS enrolF,
SUM(isnull(enM,0) + isnull(enF,0)) AS Enrol,
sum(case when enAge = svyPSAge + L.lvlYear - 1 then enM else null end) nEnrolM,
sum(case when enAge = svyPSAge + L.lvlYear - 1 then enF else null end) nEnrolF,
sum(case when enAge = svyPSAge + L.lvlYear - 1
		then isnull(enM,0) + isnull(enF,0) else null end
	) nEnrol


FROM   dbo.Enrollments
INNER JOIN lkpLevels L
ON Enrollments.enLevel = L.codecode
INNER JOIN SchoolSurvey SS
ON SS.ssID = Enrollments.ssID
LEFT JOIN Survey S
ON S.svyYear = SS.svyYEar
GROUP BY Enrollments.ssID
GO

