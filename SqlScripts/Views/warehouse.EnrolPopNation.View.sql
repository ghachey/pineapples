SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - Enrolment and population data National totals
--
-- This is a consolidation of warehouse.enrol to group by year, age, gender
-- including national Population for each Age
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW [warehouse].[EnrolPopNation]
AS
Select SurveyYear
, Age
, GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA
, sum(Pop) Pop

from warehouse.EnrolAndPop E
	INNER JOIN
	(
		select SurveyYear SY, max(age) A from warehouse.Enrol WHERE Enrol is not null GROUP BY SurveyYear
	) MaxAge
		ON E.SurveyYear = MaxAge.SY
		AND E.Age <= MaxAge.A
GROUP BY SurveyYear
, Age
, GenderCode
GO

