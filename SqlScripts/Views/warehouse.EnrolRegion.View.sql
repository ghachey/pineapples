SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by region
--
-- This is a simple consoilidation of warehouse.enrol to group by Region
-- which is an attribute of Island.
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation
--		warehouse.EnrolIsland

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolIslandR
--		warehouse.EnrolRegionR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW
[warehouse].[EnrolRegion]
AS
Select SurveyYear
, [Region Code] RegionCode
, Region

, ClassLevel
, Age
, GenderCode

, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA

from warehouse.Enrol E
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON E.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, [Region Code]
, Region
, ClassLevel
, Age
, GenderCode
GO

